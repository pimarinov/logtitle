<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('library::partials.head')
</head>
<body>
    <section id="header">
        <nav class="navbar navbar-expand-sm align-items-start">
            <div class="navbar-nav mr-auto">
                <div class="navbar-brand">
                    <a href="{{ route('site.home') }}">{{ $website_name }}</a>
                    <small>&rsaquo;</small>
                    <a class="cp~" href="{{ route('library::read.summary') }}"><strong>lib</strong></a>
                </div>
            </div>
            <div class="navbar-nav ml-auto align-items-end">
                <button class="navbar-toggler d-md-none" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse">
                    @include('library::partials.main-nav') 
                </div>
            </div>
        </nav>
    </section>
    <section id="header-ext">
        @include('library::partials.sub-nav')
    </section>
    <section id="header-breadcrumbs" class="breadcrumbs"></section>
    <section id="page-content">
		@yield('page-content')
    </section>
    <section id="footer-breadcrumbs" class="breadcrumbs"></section>
    <div id="footer">
        @include('library::partials.footer') 
    </div>
</body>
</html>
