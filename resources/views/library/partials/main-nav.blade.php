                    <!-- library.partials.main-nav - @see LibraryController::NAV -->
                    <ul id="main-nav" class="navbar-nav mr-auto">
                        @foreach($mainNav as $mainNavCurrent => $mterialIconClass)
                            <li class="nav-item{{ $mainNavSection === $mainNavCurrent ? ' active' : '' }}">
                                <a class="nav-link" href="{{ route("library::{$mainNavCurrent}.summary") /*url('/library/summary')*/ }}">
                                   <i class="material-icons-sharp d-md-none">{{$mterialIconClass}}</i>
                                   <strong class="d-sm-none d-md-inline">{{ ucfirst($mainNavCurrent) }}</strong><span></span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
