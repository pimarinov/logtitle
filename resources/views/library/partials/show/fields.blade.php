    <div class="fields">
        @foreach ($fields as $field)
            <div class="d-flex align-content-center flex-wrap">
                @if (!in_array($field, ['created_at', 'updated_at']))
                    <div class="m-1 pt-3 pb-3 col-lg-3 text-right text-muted align-self-center">
                        {{ucfirst(str_replace('_', ' ', $field))}}:
                    </div>
                    <div class="m-1 flex-grow-1 align-self-center">
                        {{$$item->$field}}
                    </div>
                @else
                    <div class="m-1 pt-2 pb-2 col-lg-3 text-right text-muted align-self-center">
                        {{ucfirst(str_replace('_', ' ', $field))}}:
                    </div>
                    <div class="m-1 flex-grow-1 align-self-center">
                        <span title="{{$$item->$field}}">{{$$item->$field->diffForHumans()}}</span>
                    </div>
                @endif
            </div>
        @endforeach
    </div>