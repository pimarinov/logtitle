@if (isset($subNav))
	<!-- cp.partials.sub-nav -->
	<ul id="sub-nav" class="nav nav-tabs pt-3 px-3"> 
	@foreach ($subNav as $subNavCurrent)
		<li class="nav-item">
			<a class="nav-link{{ $subNavCurrent===$subNavSection? ' active' :'' }}" 
			   href="{{ url(/*route("library::{$mainNavSection}.{$subNavCurrent}")*/'/library/' . ($subNavCurrent!=='home'?$subNavCurrent:$section))/*route($subNavCurrent . '.index')*/ }}">
				{{ ucfirst(str_replace(['_', '-'], ' ', $subNavCurrent)) }}
			</a>
		</li>
	@endforeach
	</ul>
@endif
