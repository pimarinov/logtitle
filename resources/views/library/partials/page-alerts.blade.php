            <!-- cp.partials.page-alerts -->
            <div id="page-alerts" class="mb-3">
                @if (session()->get('danger'))
                    <div class="alert alert-danger col">
                        {{ session()->get('danger') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = 'none';},400); this.parentNode.setAttribute('class',(this.parentNode.getAttribute('class')+' msg-fadeout'));">
                            <button type="button" class="close material-icons" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = 'none';},400); this.parentNode.setAttribute('class',(this.parentNode.getAttribute('class')+' msg-fadeout'));">
                                close
                            </button>
                        </button>
                    </div>
                @elseif(session()->get('success'))
                    <div class="alert alert-success col">
                        {{ session()->get('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = 'none';},400); this.parentNode.setAttribute('class',(this.parentNode.getAttribute('class')+' msg-fadeout'));">
                            <button type="button" class="close material-icons" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = 'none';},400); this.parentNode.setAttribute('class',(this.parentNode.getAttribute('class')+' msg-fadeout'));">
                                close
                            </button>
                        </button>
                    </div>
                @endif
            </div>
