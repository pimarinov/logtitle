@extends('library::layouts.library')

@section('page-content')
	<div class="container-fluid" style="margin-top: 20px">
		@include('library.partials.page-header')
		<div class="row no-gutters my-2">
			<div class="col-lg mb-3 mr-lg-1">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">
							<a class="text-decoration-none" href="{{ route('library::home') }}">Read titles</a>
						</h5>
						<p class="card-subtitle font-italic text-muted"><small>Your recent tiles</small></p>
					</div>
					<ul class="list-group border-0">
						<li class="list-group-item">
							<a class="flex-fill" href="{-{ route('cp::dummy-users.index') }-}">
								Окото на света (Колелото на времето #1): 2021-06-26 - 
							</a>
						</li>
						<li class="list-group-item">
							<a class="flex-fill" href="{-{ route('cp::dummy-users.index') }-}">
								Нова пролет (прелюдия към Клелото на времето #0): 2021-06-21 - 2021-06-26
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg mb-3 ml-lg-1">
				<div class="card">
					<div class="card-body py-0">
						<h5 class="card-title mb-1">
							<a class="text-decoration-none" href="{{ route('library::settings.summary') }}">
								Settings
							</a>
						</h5>
						<p class="font-italic text-muted mb-1"><small>Cp administration.</small></p>
					</div>
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{-{ route('library::users.index') }-}">
								Users
							</a>
							<small class="text-muted">{{ $users_count }}</small>
							<a class="text-primary ml-4" href={-{ route('library::users.create') }-}"">+ New</a>
						</li>
						<li class="list-group-item d-flex">
							<a class="flex-fill" href="{-{ route('cp::vars.index') }-}">
								Vars
							</a>
							<a class="text-primary ml-4" href="{-{ route('cp::vars.create') }-}">+ New</a>
						</li>
						<li class="list-group-item">
							<a href="{{ route('library::settings.account') }}">Account</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		
	</div>
@endsection
