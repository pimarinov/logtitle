@extends('library::layouts.library')

@section('page-content')
	<div class="container-fluid" style="margin-top: 20px">
		@include('library::partials.page-header')
		<div class="row no-gutters mt-2">
			<div class="col-lg mb-3 mr-lg-1">
				<div class="card mb-2">
					<div class="card-body">
						<h5 class="card-title">Cp Settings</h5>
						<div class="theme form-group row">
							<label for="staticEmail" class="col-sm-2 col-form-label">Theme</label>
							<div class="col-sm-10 pt-1">
								<div class="form-check form-check-inline mt-1">
									<input class="form-check-input" name="theme" type="radio" value="dark" id="dark">
									<label class="form-check-label" for="dark">Dark</label>
								</div>
								<div class="form-check form-check-inline mt-1">
									<input class="form-check-input" name="theme" type="radio" value="light" id="light">
									<label class="form-check-label" for="light">
										Light
									</label>
								</div>
								<div class="form-check form-check-inline mt-1">
									<input class="form-check-input" name="theme" type="radio" value="system" id="system">
									<label class="form-check-label" for="system" 
										title="Get the preffered theme by the OS or the browsers UserAgent">System's Default</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<h5 class="card-title">System Info</h5>
					</div>
					<ul class="list-group">
						<li class="list-group-item d-flex justify-content-between align-items-center text-secondary">
							<strong>Laravel</strong>
							<span>{{ app()->version() }}</span>
						</li>
						<li class="list-group-item d-flex justify-content-between align-items-center text-secondary">
							<strong>Server</strong>
							<span>{{ Request::server('SERVER_SOFTWARE') }}</span>
						</li>
						<li class="list-group-item d-flex justify-content-between align-items-center text-secondary">
							Php
							<span>{{ phpversion() }}</span>
						</li>
						<li class="list-group-item d-flex justify-content-between align-items-center text-secondary">
							MySql
							<span>
                                <kbd class="text-monospace text-secondary mr-3">Database name: {{ \DB::connection()->getDatabaseName() }}</kbd>
                                {{ DB::select( DB::raw('select version() as `version`;') )[0]->{'version'} }}
                            </span>
						</li>
						<li class="list-group-item d-flex justify-content-between align-items-center text-secondary">
							Redis
							<span>{{ phpversion('redis') }}</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg mb-3 ml-lg-1">
				<div class="card">
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('library::home') }}">
								Users
							</a>
							<small class="text-muted">{{ $users_count }}</small>
							<a class="text-primary ml-4" href={{ route('library::home') }}"">+ New</a>
						</li>
						<li class="list-group-item d-flex">
							<a class="flex-fill" href="{{ route('library::home') }}">
								Vars
							</a>
							<small class="text-muted">0</small>
							<a class="text-primary ml-4" href="{{ route('library::home') }}"><i class="material-icons small">add</i> New</a>
						</li>
						<li class="list-group-item">
							<div class="row">
								<div class="col d-flex">
									<a class="flex-fill" href="{{ route('library::settings.account') }}">
										Account
									</a>
									<a class="text-mutted ml-4 font-weight-bold" href="{{ route('library::logout') }}" onclick="return confirm('Logging out, please confirm?')">Logout <i class="material-icons small">arrow_forward</i></a>
								</div>
							</div>
							<div class="row">
								<div class="col d-flex">
									<div class="flex-fill">
										<div class="form-group row mt-3 mb-0">
											<label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
											<div class="col-sm-10">
												<span class="form-control-sm form-control-plaintext text-inherit">{{ Auth::user()->name }}</span>
											</div>
										</div>
										<div class="form-group row mb-0">
											<label for="name" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
											<div class="col-sm-10">
												<span class="form-control-sm form-control-plaintext text-inherit">{{ Auth::user()->email }}</span>
											</div>
										</div>
									</div>
									<div class="align-self-start pt-2">
										<p class="mb-0"><img class="card-img-top border" src="{{ $gravatar }}" alt="Your photo" style="max-width:60px;"></p>
										<p class="text-center mb-n1"><a href="http://en.gravatar.com/emails/" target="_blank" class="text-primary">Change</a></p>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		
	</div>
@endsection

@push('head')
<script src="{{ asset("pimarinov/cp/js/settings.js") }}" defer></script>
@endpush
