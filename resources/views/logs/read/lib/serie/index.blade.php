@extends('layouts.read')

@section('content')
    @csrf
    {!! $table::index($undeletableKeys ?? []) !!}
@endsection
