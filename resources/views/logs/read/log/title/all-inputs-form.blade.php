@if('create' === $nav->currentMethod)
    <form method="post" id="title-form" class="d-block mx-lg-5 px-lg-5"
        action="{{ route('read.log.titles.store') }}">
@else
    <form method="post" id="title-form" class="d-block mx-lg-5 px-lg-5"
        action="{{ route('read.log.titles.update', ["title" => $title]) }}">
@endif
        @csrf
        <div class="form-row">
            <div class="col-lg">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input name="title" type="text" id="title"
                        class="form-control @error('title') is-invalid @enderror"
                        oninput="clearError(this);"
                           value="{{ (old('title') ?? $title->title ?? null) }}">
                    @error('title')
                        <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg">
                <div class="form-group {{ $errors->language_id ? 'invalid' : '' }}">
                    <label for="language_id">Language</label>
                    <select name="language_id" id="language_id"
                        class="form-control @error('language_id') is-invalid @enderror"
                        oninput="clearError(this);">
                        @if(!$errors->any() || $errors->has('language_id'))
                            <option selected vlaue>Choose...</option>
                        @endif
                        @foreach($languages as $language)
                            <option value="{{$language->id}}"
                                @if($language->id == (old('language_id') ?? $title->language_id ?? null) ) selected @endif
                            >
                                {{$language->name}}
                            </option>
                        @endforeach
                    </select>
                    @error('language_id')
                        <div class="invalid-feedback">{{ $errors->first('language_id') }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg">
                <div class="form-group {{ $errors->redable_serie_id ? 'invalid' : '' }}">
                    <label for="serie">Serie</label>
                    <select name="redable_serie_id" id="serie"
                        class="form-control @error('redable_serie_id') is-invalid @enderror"
                        oninput="clearError(this);">
                        @if(!(old('redable_serie_id') ?? $title->redable_serie_id ?? null))
                            <option selected value="">Choose...</option>
                        @endif
                        <option>...</option>
                    </select>
                    @error('redable_serie_id')
                        <div class="invalid-feedback">{{ $errors->first('redable_serie_id') }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea name="summary" id="summary"
                        class="form-control @error('summary') is-invalid @enderror"
                        oninput="clearError(this);">{{ 
                            (old('summary') ?? $title->summary ?? null) 
                    }}</textarea>
                    @error('summary')
                        <div class="invalid-feedback">{{ $errors->first('summary') }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description"
                        class="form-control @error('description') is-invalid @enderror"
                        oninput="clearError(this);">{{ 
                            (old('description') ?? $title->description ?? null) 
                        }}</textarea>
                    @error('description')
                        <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg">
                <div class="form-group">
                    <div class="form-group">
                        <label for="set_reading">Set reading</label>
                        <input name="set_reading" type="text" readonly id="set_reading"
                            value="{{ (old('set_reading') ?? $title->set_reading ?? null) }}"
                            class="form-control js-datepicker @error('set_reading') is-invalid @enderror"
                            oninput="clearError(this);">
                        @error('set_reading')
                            <div class="invalid-feedback">{{ $errors->first('set_reading') }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-lg">
                <div class="form-group">
                    <div class="form-group">
                        <label for="end_reading">End reading</label>
                        <input name="end_reading" type="text" readonly id="end_reading"
                            value="{{ (old('end_reading') ?? $title->end_reading ?? null) }}"
                            class="form-control js-datepicker @error('end_reading') is-invalid @enderror"
                            oninput="clearError(this);">
                        @error('end_reading')
                            <div class="invalid-feedback">{{ $errors->first('end_reading') }}</div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-4">
            <div class="col text-right">
            @if('create' === $nav->currentMethod)
                <a href="{{ route('read.log.titles.index') }}" class="btn btn-outline-secondary float-left">Cancel</a>
                <button type="submit" class="btn btn-outline-success px-5">Create title</button>
            @else
                <button type="button" class="btn btn-outline-secondary float-left">Cancel</button>
                <button type="submit" class="btn btn-primary">Update title</button>
            @endif
            </div>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script>
        function clearError(element)
        {
            if(element.classList.contains("is-invalid"))
            {
                element.classList.remove("is-invalid");
            }
        }
        (function() {
            $(".js-datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker('option', {
                dateFormat: "mm/dd/y"
            });
            $('#title-form').on('submit', function(){
                $(".js-datepicker").datepicker('option', {
                    dateFormat: "yy-mm-dd"
                });
            });
        })();
        @if($errors->any())
            /*console.log('has change');*/
        @endif
    </script>
