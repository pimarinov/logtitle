@extends('layouts.read')

@section('content')
<div class="card m-3">
    <div class="card-body">
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Title</label> {{$title->title}} </p>
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Serie</label> {{$title->serie}} </p>
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Summary</label> {!! nl2br($title->summary) !!}</p>
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Description</label> {!! nl2br($title->description) !!}</p>
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Started</label> {{ $title->set_reading_formatted }}</p>
        <p class="card-text clearfix mb-0"><label class="text-muted mb-0 mr-3 float-left">Completed</label> {{ $title->end_reading_formatted }}</p>
        <p class="card-text clearfix mb-0" title="{{ $title->created_at_formatted }}"><label class="text-muted mb-0 mr-3 float-left">Created</label> {{ $title->created_at_diff }}</p>
        <p class="card-text clearfix mb-0" title="{{ $title->updated_at_formatted }}"><label class="text-muted mb-0 mr-3 float-left">Updated</label> {{ $title->updated_at_diff }}</p>
    </div>
    <div class="card-footer text-right">
        <a href="#" 
            class="btn btn-sm btn-outline-danger mr-3">
             <span class="material-icons md-18">delete</span>
             Delete
         </a>
        <a href="{{ route('read.log.titles.edit', ['title'=>$title]) }}"
           class="btn btn-sm btn-outline-primary px-5 mr-n2">
            <span class="material-icons md-18">edit</span>
            Edit
        </a>
  </div>
</div>
<style>
    /*https://stackoverflow.com/a/57492027/819118*/
    .material-icons {
       vertical-align: middle;
       line-height: 0 !important;
       position: relative;
       top: -1px;
   }
</style>
@endsection
