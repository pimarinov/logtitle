@extends('layouts.read')

@section('content')
	<div class="container-fluid">
		<div class="row no-gutters my-2">
			<div class="col-lg mb-3 mr-lg-1">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">
							<a class="text-decoration-none" href="{{ route('read.log.titles.index') }}">Read titles</a>
						</h5>
						<p class="card-subtitle font-italic text-muted"><small>Your recently logged tiles</small></p>
					</div>
					<ul class="list-group border-0">
						<li class="list-group-item">
							<a class="flex-fill" href="{-{ route('cp::dummy-users.index') }-}">
								Окото на света (Колелото на времето #1): 2021-06-26 - 
							</a>
						</li>
						<li class="list-group-item">
							<a class="flex-fill" href="{-{ route('cp::dummy-users.index') }-}">
								Нова пролет (прелюдия към Клелото на времето #0): 2021-06-21 - 2021-06-26
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg mb-3 ml-lg-1">
				<div class="card">
					<div class="card-body py-0">
						<h5 class="card-title mb-1">
							<a class="text-decoration-none" href="{{ route('read.log.home') }}">
								Summary
							</a>
						</h5>
						<p class="font-italic text-muted mb-1"><small>Your logged resources.</small></p>
					</div>
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('read.log.titles.index') }}">
								Titles
							</a>
							<small class="text-muted"> 0 </small>
							<a class="text-primary ml-4" href="{{ route('read.log.titles.create') }}">+ New</a>
						</li>
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('read.log.writers.index') }}">
								Writers
							</a>
							<small class="text-muted"> 0 </small>
							<a class="text-primary ml-4" href="{{ route('read.log.writers.create') }}">+ New</a>
						</li>
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('read.log.series.index') }}">
								Series
							</a>
							<small class="text-muted"> 0 </small>
							<a class="text-primary ml-4" href="{{ route('read.log.series.create') }}">+ New</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		
	</div>
@endsection
