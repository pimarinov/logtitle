<nav class="d-none d-sm-block px-1" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-no-gutter m-0 p-0">
        @foreach($nav->breadcrumbs as $breadcrumb)
            @if(isset($breadcrumb['url'])) 
                <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['name'] }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb['name'] }}</li>
            @endif
        @endforeach
    </ol>
</nav>
