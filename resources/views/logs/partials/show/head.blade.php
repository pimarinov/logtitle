    {{-- cp.partials.show.head --}}
    <div class="page-header border-bottom d-flex align-items-center  align-items-middle justify-content-between mb-5 align-items-stretch ">
        <h1 class="p-2">{{ucfirst($item)}}</h1>
        @if (isset($icon) || isset($image))
            <div class="col-md-2 right" id="{{ $icoDivId ?? 'show-head-item' }}">
            @if (isset($icon))
                <i class="material-icons">{{ $icon }}</i>
            @else
                <img src="{{ $image }}" class="right" alt="...">
            @endif
            </div>
        @endif
    </div>