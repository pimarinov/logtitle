            {{-- logs.partials.page-header --}}
			<div class="page-header border-bottom">
				<div class="row align-items-end">
					<div class="col-sm mb-2 mb-sm-0">
                        @include('logs.partials.breadcrumbs')
						<h3 class="page-header-title">{{ ucfirst(str_replace(['_', '-'], ' ', $items ?? $nav->currentSubSection)) }}</h3>
					</div>
					@if (isset($nav->create))
						<div class="col-sm-auto mb-2">
							<a href="{{ route($nav->create['blade']) }}" class="btn-add">
								<i class="material-icons md-18 align-text-bottom">add</i> Create {{ $nav->create['item'] }}
							</a>
						</div>
					@endif
				</div>
			</div>
