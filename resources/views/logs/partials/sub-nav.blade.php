@if (isset($nav->navSubSections))
	<!-- cp.partials.sub-nav -->
	<ul id="sub-nav" class="nav nav-tabs pt-3 px-3">
        @foreach ($nav->navSubSections as $sub_node)
            <li class="nav-item">
                <a class="nav-link{{ $sub_node === $nav->currentSubSection? ' active' :'' }}" 
                   href="{{ url('/read/' . ($nav->currentSection !== 'home' ? $nav->currentSection. '/' : '')  . $sub_node)/*route($sub_node . '.index')*/ }}">
                    {{ ucfirst(str_replace(['_', '-'], ' ', $sub_node)) }}
                </a>
            </li>
        @endforeach
	</ul>
@endif
