                {{-- cp.partials.form.submit --}}
                <div class="form-group row mt-5">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary col-md-2">Save {{ $item }}</button>
                        <a class="btn btn-outline-secondary col-md-2" href="{{ route('cp::' . $items . '.show', $user->id) }}" title="User overview">Cancel</a>
                    </div>
                </div>