                    <!-- logs.partials.main-nav -->
                    <ul id="main-nav" class="navbar-nav mr-auto">
                        @foreach($nav->navSections as $section)
                        
                            <li class="nav-item{{{ ($section===$nav->currentSection ? ' active' : '') }}}">
                                <a class="nav-link" href="{{ url('/read/' . ($section !== 'home' ? $section . '/' : '') . 'home') }}">
                                    <i class="material-icons-sharp d-md-none">home</i><strong class="d-sm-none d-md-inline">{{ ucfirst($section) }}</strong><span></span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
