<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-white text-black text-opacity-75 dark:bg-dark-bg dark:text-white dark:text-opacity-75">
    <div>
		{{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white bg-opacity-75 border border-gray-300 dark:bg-dark-panel dark:bg-opacity-75 dark:border-dark-panel shadow-md overflow-hidden">
        {{ $slot }}
    </div>
</div>
