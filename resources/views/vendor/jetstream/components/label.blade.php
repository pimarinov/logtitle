@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-sm dark:text-opacity-25']) }}>
    {{ $value ?? $slot }}
</label>
