<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('logs.partials.head')
</head>
<body>
    <section id="header">
        <nav class="navbar navbar-expand-sm align-items-start">
            <div class="navbar-nav mr-auto">
                <div class="navbar-brand">
                    <a href="{{ route('site.home') }}">{{ $nav->websiteName }}</a>
                    <small>&rsaquo;</small>
                    <a href="{{ route('read.home') }}">Read</a>
                </div>
            </div>
            <div class="navbar-nav ml-auto align-items-end">
                <button class="navbar-toggler d-md-none" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarsExample03">
                    @include('logs.partials.main-nav') 
                </div>
            </div>
        </nav>
    </section>
    <section id="header-ext">
        @include('logs.partials.sub-nav')
    </section>
    <section id="page-header">
        <div class="container-fluid">
            @include('logs.partials.page-alerts')
            @include('logs.partials.page-header')
        </div>
    </section>
    <section id="page-content">
        @yield('content')
    </section>
    <section id="footer-breadcrumbs" class="breadcrumbs"></section>
    <div id="footer">
        @include('logs.partials.footer') 
    </div>
</body>
</html>
