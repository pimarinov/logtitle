<?php

namespace Pimarinov\Table\Sections;

class Thead extends Tbody
{
    /** @param array Attributes for the table Html tag */
    public static $attributes = [];
    /** @param null|string Extension (from $_SERVER['REQUEST_URI']) */
    public static $pageExt;

    protected static function load()
    {
        $items = self::$t['items'];
        $vars = ['items' => $items];

        if (self::config('FILTER_ACTIVE')) {
            self::filterValues($vars['f_value'], $vars['f_options']);
        }

        $vars['div_attributes'] = self::tagAttributes('div', $items);

        $vars['table_attributes'] = self::tagAttributes('table', $items);

        $vars['ths'] = self::$cols ? self::ths() : null;

        $vars['body'] = self::rowsTbody();

        $vars['footer'] = self::rowsTfoot();

        return self::view('table::table', $vars);
    }

    protected static function filterValues(&$filter, &$opts = [])
    {
        $filter = filter_input(INPUT_GET, 'filter', FILTER_SANITIZE_STRING) ?: null;

        $filterBy = filter_input(INPUT_GET, 'filter-by', FILTER_VALIDATE_INT);
        foreach (self::$cols as $key => $value) {
            if (isset($value[2]['sort']) && $value[2]['sort'] === false) {
                continue;
            }
            $selected = $filterBy === $key ? ' selected' : null;
            $opts[] = "<option value=\"{$key}\"{$selected}>{$value[0]}</option>";
        }
    }

    private static function tagAttributes($tag, $items)
    {
        $attr = $tag === 'div'
            ? ['id' => $items.'-list', 'class' => 'table'] 
            : ['id' => $items.'-table', 'data-table' => 'js',
                'data-sort-a' => self::config('UTF8_ASC_SYMBOL'),
                'data-sort-d' => self::config('UTF8_DESC_SYMBOL'),
            ];
        if (array_key_exists($tag, self::$attributes)) {
            $attr += self::$attributes[$tag];
        }
        if (isset(self::$attributes[$tag]['class'])) {
            $attr['class'] .= ' '.self::$attributes[$tag]['class'];
        }

        return self::attributes($attr);
    }

    protected static function ths()
    {
        $ths = [];
        $length = count(self::$cols);
        for ($i = 0; $i < $length; $i++) {
            list($lbl, $col, $arg) = array_pad(self::$cols[$i], 3, null);

            if (is_null($col)) {
                $arg['sort'] = false;
            }

            $sort = $del = null;

            $attr = self::thAttributes($col, $arg, $sort, $del);

            $ths[] = self::thTag($i, $attr, $sort, $del, $lbl);
        }

        return implode('', $ths);
    }

    private static function thAttributes($col, $arg, &$sort, &$del)
    {
        if (isset($arg['width'])) { // Width attribute -> style
            $width = 'width:'.$arg['width'].';';
            $arg['style'] = isset($arg['style'])
                ? $width.$arg['style']
                : $width;
        }

        if (($del = isset($arg['type']) && $arg['type'] == 'delete')) {
            $sort = false;
        } else {
            $sort = isset($arg['sort']) ? $arg['sort'] : $col;
        }

        return array_diff_key((array) $arg, ['sort', 'type', 'width']);
    }

    private static function thTag($index, $attr, $sort, $del, $lbl)
    {
        $tag = '<th'.self::attributes($attr).'>';
        if ($sort) {
            $tag .= '<a onclick="table.Sort('.$index.',this);">';
        }
        if (!$del) {
            if ($sort == self::$t['order']['col']) {
                $span = self::$t['order']['dir'] === 'desc'
                    ? self::config('UTF8_DESC_SYMBOL')
                    :  self::config('UTF8_ASC_SYMBOL');
            } else {
                $span = '';
            }
            $tag .= '<span>'.$span.'</span>'.$lbl;
        } else {
            $tag .= '<input id="'.self::$t['items'].'CheckDeleteAll"'.
                ' onclick=\"checkAllDeleteCheckboxes(this,'.
                ' \''.self::$t['items'].'\')" type="checkbox"/>';
        }
        if ($sort) {
            $tag .= '</a>';
        }
        $tag .= '</th>';

        return $tag;
    }
}
