<?php

namespace Pimarinov\Table\Sections\Traits;

trait Request
{
    protected static function request(&$export)
    {
        $export = self::setExport();

        $add = [
            'order' => [
                'dir' => self::orderDir(),
                'col' => self::orderCol(),
            ],
            'filter' => self::filter(),
            'page'   => self::page(),
        ];

        return array_merge(self::$t, $add);
    }

    private static function filter()
    {
        $filter = filter_input(INPUT_GET, 'filter') ?: false;
        if ($filter) {
            $filterBy = filter_input(INPUT_GET, 'filter-by', FILTER_VALIDATE_INT);
            if ($filterBy === false || is_null($filterBy)) {
                $filterBy = self::filterByAll();
            } else {
                $filterBy = self::$cols[$filterBy][1];
            }
            $filterBy = 'CONCAT(" ",'.$filterBy.', " ")';
            if (self::config('FILTER_CASE_SENSITIVE') !== true) {
                $filterBy .= ' COLLATE '.self::config('DB_COLLATION_CI');
            }
            $filter = $filterBy.' LIKE '.'"%'.$filter.'%"';
        }

        return $filter;
    }

    private static function filterByAll()
    {
        $all = [];
        foreach (self::$cols as $v) {
            if (isset($v[2]['sort']) && $v[2]['sort'] === false) {
                continue;
            }
            $all[] = 'IFNULL('.$v[1].', "")';
        }

        return 'CONCAT('.implode(',', $all).')';
    }

    private static function orderCol()
    {
        if (($col = filter_input(INPUT_GET, 'col', FILTER_VALIDATE_INT))) {
            return isset(self::$cols[$col][2]['sort'])
                ? self::$cols[$col][2]['sort']
                : self::$cols[$col][1];
        }

        return self::$t['order']['col'];
    }

    private static function orderDir()
    {
        $reset = filter_has_var(INPUT_GET, 'col') ? 'asc' : null;

        return in_array(filter_input(INPUT_GET, 'ord'), ['asc', 'desc'])
            ? filter_input(INPUT_GET, 'ord')
            :  ($reset ?: self::$t['order']['dir']);
    }

    private static function setExport()
    {
        $exp = filter_input(INPUT_GET, 'export', FILTER_SANITIZE_STRING)
            ?: filter_var(@$_GET['export'], FILTER_SANITIZE_STRING); //4 testing

        return in_array($exp, self::config('SAVES')) ? $exp : false;
    }

    private static function page()
    {
        return filter_has_var(INPUT_GET, 'pg') && self::$export == false
            ? (int) filter_input(INPUT_GET, 'pg', FILTER_SANITIZE_NUMBER_INT)
            : self::$t['page'];
    }

    /**
     * While active $_GET['reload-total-cache'] or $_GET['total-cache-active'];
     * Drops the time limit, keeping its current: ini_get('max_execution_time').
     *
     * @return void
     */
    protected static function prepareForExtensiveOperation() : void
    {
        foreach (['reload-total-cache', 'total-cache-active'] as $var) {
            self::$t[$var] = filter_has_var(INPUT_GET, $var);
            if (self::$t[$var]) {
                $maxExecutionTime = (int) ini_get('max_execution_time');
                if ($maxExecutionTime > 0) {
                    self::config('PHP_MAX_EXECUTION_TIME', $maxExecutionTime);
                    set_time_limit(0);
                }
            }
        }
    }

    protected static function completeAnExtensiveOperation(string $name) : bool
    {
        if( self::$t[$name] ?? null ) {

            $maxExecutionTime = self::config('PHP_MAX_EXECUTION_TIME');

            if ($maxExecutionTime > 0) {
                set_time_limit($maxExecutionTime);
            }
            return true;
        }
        return false;
    }
}
