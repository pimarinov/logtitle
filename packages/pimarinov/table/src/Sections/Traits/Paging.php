<?php

namespace Pimarinov\Table\Sections\Traits;

trait Paging
{
    protected static function paging(&$vars)
    {
        if (self::$t['pages'] > 1) {
            $vars['pages'] = self::$t['pages'];
            $vars['page'] = self::$t['page'];
            $vars['jumpL'] = self::jumps();
            $vars['jumpR'] = self::jumps();
            $vars['arrowL'] = self::config('UTF8_LEFT_SYMBOL');
            $vars['arrowR'] = self::config('UTF8_RIGHT_SYMBOL');

            $limit = 10;

            $vars['start'] = $vars['page'] > ($limit / 2)
                ? ($vars['page'] - $limit / 2)
                : 1;

            if ($vars['page'] > ($vars['pages'] - ($limit / 2))) {
                $vars['final'] = $vars['pages'];
            } elseif ($vars['page'] > ($limit / 2)) {
                $vars['final'] = $vars['start'] + $limit;
            } else {
                $vars['final'] = $limit;
            }
        }
    }

    /** Jump links -1M|-100K|-10K|-1K|100|{paging}|100|+1K|+10K|+100K|+1M
     * @param string   $html       Code to show
     * @param null|int $multiplier Multiplier
     *
     * @return string */
    protected static function jumps($html = '', $multiplier = null)
    {
        static $direction;

        if (is_null($multiplier)) {
            $direction = !isset($direction) ? '-' : '+';
            $multiplier = 100;
        }

        $jump = self::jump($multiplier, $direction);
        $add = '<li class="jump"><a>'.$jump.'</a></li>';

        $jumpExists = $direction === '-'
            ? (self::$t['page'] - $multiplier) > 0
            : (self::$t['page'] + $multiplier) <= self::$t['pages'];

        if ($jumpExists) {
            $html = $direction === '-' ? $add.$html : $html.$add;

            return self::jumps($html, ($multiplier * 10));
        }

        return $html;
    }

    private static function jump($multiplier, $direction)
    {
        if ($multiplier >= 1000000) {
            return $direction.($multiplier / 1000000).'M';
        } elseif ($multiplier >= 1000) {
            return $direction.($multiplier / 1000).'K';
        } else {
            return $direction.$multiplier;
        }
    }
}
