<?php

namespace Pimarinov\Table\Sections\Traits;

trait Config
{
    /** @param array Re-settable values for current table call */
    protected static $config = [
        'APP'                   => 'App',
        'DB_COLLATION_CI'       => 'utf8mb4_general_ci', //UTF8_GENERAL_CI
        'EMPTY_TABLE_MSG'       => 'No results found.',
        'EXPORT_FILE_NAME'      => ':app - :items export (:datetime)',
        'FILTER_ACTIVE'         => true,
        'FILTER_CASE_SENSITIVE' => false,
        'SAVES'                 => ['CSV', 'Excel'],
        'UTF8_ASC_SYMBOL'       => '&#9650;',
        'UTF8_DESC_SYMBOL'      => '&#9660;',
        'UTF8_LEFT_SYMBOL'      => '&lsaquo;',
        'UTF8_RIGHT_SYMBOL'     => '&rsaquo;',
        'SHOWING_FROM_TO_ITEMS' => 'Showing :from to :upto from :total :items.',
        'TOTAL_COUNT_CACHE'     => 0,
        'PHP_MAX_EXECUTION_TIME'=> 0,
    ];
    /** @param array Errors collector */
    protected static $errors = [];

    /**
     * Set/Get config value.
     *
     * @param mixed $value (string) Get if exists, (array) Set if valid
     *
     * @return mixed
     */
    public static function config($value)
    {
        try {
            switch (gettype($value)) {
                case 'array':
                    foreach ($value as $k => $v) {
                        self::$config[$k] = self::valid((string) $k, $v);
                    }
                    break;
                case 'string':
                    return self::valid($value);
                default:
                    throw new \Exception('Invalid value type.');
            }
        } catch (\Exception $e) {
            self::error('ERROR: '.$e->getMessage());
        }
    }

    protected static function error($message = null)
    {
        if (isset($message)) {
            self::$errors[] = $message;
        } else {
            return empty(self::$errors) ? null :
                '<p class="tbl-err">'.implode('</br>', self::$errors).'</p>';
        }
    }

    private static function valid($key, $val = null)
    {
        if (!array_key_exists($key, self::$config)) {
            throw new \Exception('Request to undefined value: '.$key);
        }

        if ($val !== null) {
            if (empty($val) || gettype($val) !== gettype(self::$config[$key])) {
                throw new \Exception("Setting invalid value: $val (:$key)");
            }
        }

        return $val === null ? self::$config[$key] : $val;
    }
}
