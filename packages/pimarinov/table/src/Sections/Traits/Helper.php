<?php

namespace Pimarinov\Table\Sections\Traits;

trait Helper
{
    /** @param array Columns (config) */
    public static $cols;
    /** @param array Query result, allows altering before table::load() */
    public static $data = [];
    /** @param bool Export flag (for current request) */
    public static $export;
    /** @param bool Export is allowed (shows buttons in the tfoot) */
    public static $exportActive = true;
    /** @param bool Export data altered (use false for pure result) */
    public static $exportDataAsDisplayed = true;
    /** @param array Collector (values for current table call) */
    protected static $t = ['page' => 1, 'tables' => []];

    public static function assets($path = '/public/table')
    {
        return "<script src=\"{$path}/js/table.js\" defer></script>\n\t".
            "<link href=\"{$path}/css/table.css\" rel=\"stylesheet\">\n";
    }

    /**
     * Array to space separated key value list.
     *
     * @param array $attributes
     *
     * @return string
     */
    protected static function attributes(array $attributes)
    {
        $list = [' '];
        foreach ($attributes as $key => $value) {
            if ($value === true || (empty($value) && $value != 0)) {
                $list[] = $key;
            } else {
                $list[] = $key.'="'.$value.'"';
            }
        }

        return rtrim(implode(' ', $list));
    }

    /**
     * Parses view to string.
     *
     * @param string $template
     * @param array  $vars
     * @uses vendor/laravel/framework/src/Illuminate/Foundation/helpers.php
     *
     * @return string
     */
    protected static function view($template, $vars = [])
    {
        try {
            if (function_exists('view'))
            { // ^ ?Laravel
                return view($template)
                    ->with($vars)
                    ->render();
            } else {
                $templateHtml = sprintf('views/html/%s.html', substr($template, 7));
                return self::html($templateHtml, $vars);    
            }
        } catch (\Exception $e) {
            $templateHtml = sprintf('views/html/%s.html', substr($template, 7));
            return self::html($templateHtml, $vars);
        }
    }

	protected static function html($template, $vars = [])
	{
		extract($vars);
        ob_start();
        require dirname(__DIR__, 2).'/resources/'. $template;

        return (string) ob_get_clean();
	}

    /**
     * Needed for more than one table on page.
     *
     * @param string $items
     */
    protected static function reset($items)
    {
        if (!in_array($items, self::$t['tables'])) {
            self::$t['tables'][] = $items;
            self::$cols = [];
            self::$t['rows'] = null;
        } else {
            echo 'Existing table-id used in table::create(): '.$items;
        }
    }

    /**
     * Tests value protected value checker.
     */
    public static function confirmValue($property, $value)
    {
        try {
            $nodes = explode('.', $property);
            $name = array_shift($nodes);
            if (property_exists(__CLASS__, $name)) {
                $val = function (&$val) use ($nodes) {
                    $temp = &$val;
                    foreach ($nodes as $key) {
                        if ($key === 'last') {
                            return array_pop($temp);
                        }
                        $temp = &$temp[$key];
                    }

                    return $temp;
                };

                if (($v = $val(static::$$name)) !== null) {
                    return $v === $value;
                }

                throw new \Exception('Missing value ('.implode($nodes).').');
            }

            throw new \Exception('Undefined property ('.$name.').');
        } catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        return false;
    }
}
