<?php

namespace Pimarinov\Table\Sections\Traits;

trait Query
{
    /**
     * Adds conditions, orderBy and Limits to query.
     *
     * @param string $query
     * @param mixed  $cond
     * @param array  $order
     * @param mixed  $limit     -> 5 or [$start, $count]
     * @param bool   $useHaving Flag - to use HAVING instead of WHERE
     *
     * @return string
     */
    protected static function query(
        $query,
        $cond = null,
        array $order = [],
        $limit = 0,
        $useHaving = false
    ) {
        //Condition: '' | ' (HAVING|WHERE|AND) ' . $cond
        self::queryConditions($query, $cond, $useHaving);

        //Order: '' | 'ORDER BY ' . array_keys($order)[0] . ' ' . $order[0]
        self::queryOrder($query, $order);

        //Limit: '' | ' LIMIT ' . '(20, 40|20)'
        self::queryLimit($query, $limit);

        return $query;
    }

    private static function queryConditions(&$query, $cond, $useHaving)
    {
        if (!empty($cond)) {
            $clause = !$useHaving ? 'WHERE' : 'HAVING';
            $clue = !strpos($query, $clause) ? $clause : 'AND';
            $query .= (' '.$clue.' '.$cond);
        }
    }

    private static function queryOrder(&$query, $order)
    {
        if (!empty($order)) {
            $arr = array_map(function (&$value, $key) {
                return $key.' '.strtoupper($value);
            }, $order, array_keys($order));
            $query .= (' ORDER BY '.implode(', ', $arr));
        }
    }

    private static function queryLimit(&$query, $limit)
    {
        if (!empty($limit)) {
            $value = (is_array($limit) ? implode(', ', $limit) : $limit);
            $query .= (' LIMIT '.$value);
        }
    }
}
