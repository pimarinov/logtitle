<?php

namespace Pimarinov\Table\Sections;

class Tfoot
{
    use Traits\Config,
        Traits\Helper,
        Traits\Paging,
        Traits\Query,
        Traits\Request;

    /** @param string class name, where $helper::prepare() is expected */
    public static $helperClass;
    /** @param closure to MySql select function (example: db::select()) */
    public static $select;

    protected static function jsonTfoot()
    {
        if (count(self::$data) > 0) {
            $vars = [];
            $vars['showing'] = self::getShowing();

            self::exportLinksVars($vars);

            $vars['errors'] = self::error();

            self::paging($vars);

            $html = self::view('table::table-footer', $vars);

            return [[[$html, ['colspan' => count(self::$cols)]]]];
        }

        return [];
    }

    private static function exportLinksVars(&$vars) : void
    {
        if (self::$exportActive === true) {
            $url = strtok(self::$t['slug'], '.?&').'.json?table='.
                self::$t['items'].'&export=:type';

            $click = self::$t['cache'] ?? false
                ? 'return table.Export(this,\':type\', true);'
                : 'return table.Export(this,\':type\');';

            $links = [];
            //download attr -> https://stackoverflow.com/a/42696866/819118
            $f = "<a href=\"$url\" onclick=\"$click\" class=\"exp\" download>:type</a>";

            $exportTypes = self::config('SAVES');
            foreach ($exportTypes as $exportType) {
                $links[] = strtr($f, [':type' => $exportType]);
            }

            $vars['exportLinks'] = implode('', $links);
        }        
    }

    private static function getShowing() : string
    {
        $pageNo = self::$t['page'];

        if (self::config('TOTAL_COUNT_CACHE') 
            && !self::$t['filter'] 
            && !self::$t['reload-total-cache'] ?? false)
        {
            self::$t['cache'] = true;
            $totalCountCache = self::config('TOTAL_COUNT_CACHE');
        }
        return self::showing($pageNo, $totalCountCache ?? null);
    }

    private static function showing($pageNo, $totalCountCache) : string
    {
		if ($pageNo === 1 && count(self::$data) < self::$t['paging']) {
            //Skips total count query
            self::$t['rows'] = count(self::$data);
        } else if (isset($totalCountCache)) {
            self::$t['rows'] = $totalCountCache;
        } else {
            $query = 'SELECT COUNT(*) FROM ('.self::$t['qAll'].') AS dt';
            self::$t['rows'] = self::select($query);
        }
        self::$t['pages'] = ceil(self::$t['rows'] / self::$t['paging']);

        return self::buildSwhowing($pageNo, $totalCountCache ?? null);
    }

    private static function buildSwhowing($pageNo, $totalCountCache) : string
    {
        $arr = [];
        $arr[':items'] = ucfirst(str_replace('_', ' ', self::$t['items']));

        $arr[':from'] = ($pageNo - 1) * self::$t['paging'] + 1;
        $arr[':upto'] = ($pageNo * self::$t['paging'] < self::$t['rows'])
            ? $pageNo * self::$t['paging']
            : self::$t['rows'];

        $arr[':total'] = number_format(self::$t['rows'], 0, '.', '&nbsp;');
        
        if (isset($totalCountCache)) {
            $arr[':total'] = '(<a href="javascript:table.ReloadTotalCache(\''
                . self::$t['items']
                . '-table\')" title="Click to refresh the total count.">~'
                . $arr[':total']
                . '</a>)';
        } else {
            if (static::completeAnExtensiveOperation('reload-total-cache')) {
                //Sets the new value to can be cached.
                self::config(['TOTAL_COUNT_CACHE' => self::$t['rows']]);
            }
        }

        return strtr(self::config('SHOWING_FROM_TO_ITEMS') ?? '', $arr);
    }

    protected static function rowsTfoot()
    {
        $trs = '';
        if (self::$t['rows'] > 0) {
            $ftr = self::jsonTfoot()[0][0];
            $trs .= '<tr><td'.self::attributes($ftr[1]).'>'
                .$ftr[0].'</td></tr>';
        } elseif (count(self::$errors) > 0) {
            $trs .= '<tr><td colspan="'.count(self::$cols).'">'.
                self::error().'</td></tr>';
        }

        return $trs;
    }

    public static function select(string $expression, array $bindings = [])
    {
        if (is_object(static::$select) && (static::$select instanceof \Closure)) {
            $select = static::$select;

            $res = $select($expression, $bindings);
            //if result is single cell value ($res[0]->value), return the value
            return (count($res) === 1 && count((array) $res[0]) === 1)
                ? reset($res[0])
                : $res;
        } else {
            throw new \Exception('ERROR: table::$select is not a closure. ');
        }
    }
}
