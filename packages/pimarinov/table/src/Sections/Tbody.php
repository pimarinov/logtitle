<?php

namespace Pimarinov\Table\Sections;

class Tbody extends Tfoot
{
    protected static function jsonTbody()
    {
        $outColumns = array_column(self::$cols, 1);

        $json = [];

        if ((self::$t['rows'] = count(self::$data)) > 0) {
            foreach (self::$data as $r) {
                $rOut = [];
                foreach ($outColumns as $dbCol) {
                    $rOut[] = array_key_exists($dbCol, $r) ? $r[$dbCol] : '';
                }
                $json[] = $rOut;
            }
        } else {
            $attr = ['colspan' => count(self::$cols), 'class' => 'no-results'];
            $json[] = [[self::config('EMPTY_TABLE_MSG'), $attr]];
        }

        return $json;
    }

    protected static function rowsTbody()
    {
        $trs = '';
        foreach (self::jsonTbody() as $r) {
            if (isset($r[0][1], $r[0][1]['class']) &&
                $r[0][1]['class'] === 'no-results'
            ) {
                $trs .= '<tr><td'.self::attributes($r[0][1]).'>'
                    .$r[0][0].'</td></tr>';
            } else {
                $trs .= '<tr><td>'.implode('</td><td>', $r)
                    .'</td></tr>';
            }
        }

        return $trs;
    }

    protected static function export()
    {
        list($columns, $header) = self::exportColumnsAndHeader();

        $rows = [$header];

        $data = self::$exportDataAsDisplayed
            ? self::$data
            : self::select(self::$t['q']);

        if (count($data) > 0) {
            foreach ($data as $row) {
                $cells = [];
                foreach ($columns as $column) {
                    $cells[] = is_array($row[$column])
                        ? $row[$column][0]
                        : $row[$column];
                }
                $rows[] = $cells;
            }
        }

        return self::exportFile($rows);
    }

    private static function exportColumnsAndHeader()
    {
        $columns = $header = [];
        foreach (self::$cols as $c) {
            if (isset($c[2]['sort']) && $c[2]['sort'] === false) {
                continue;
            }
            $columns[] = $c[1];
            $header[] = $c[0];
        }

        return [$columns, $header];
    }

    private static function exportFile($eData)
    {
        $filename = self::exportFilename();
        switch (self::$export) {
            case 'Excel':
            case 'CSV':
                $escape = function ($value) {
                    return str_replace("\t", '&#9;', $value);
                };

                $output = fopen('php://output', 'w');
                if (substr($filename, -2) !== '()') { //4 testing
                    header('Content-Type:application/csv');
                    header('Content-Disposition:attachment; filename='.
                            $filename.'.csv');
                }
                foreach ($eData as $v) {
                    self::$export === 'CSV'
                        ? fputcsv($output, $v)
                        : fwrite($output, implode("\t", array_map($escape, $v))."\r\n");
                }

                return fclose($output);
        }
    }

    private static function exportFilename()
    {
        $fnReplace = [];
        $fnReplace[':app'] = self::config('APP');
        $fnReplace[':items'] = self::$t['items'];
        $format = str_replace(':', '.', '%d.%m.%Y  %H:%i:%s');
        $timeQuery = 'SELECT DATE_FORMAT(Now(), "'.$format.'") AS `now`;';
        $fnReplace[':datetime'] = self::select($timeQuery);

        return strtr((string) self::config('EXPORT_FILE_NAME'), $fnReplace);
    }
}
