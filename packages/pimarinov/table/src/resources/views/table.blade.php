<div<?=$div_attributes?>><?php
if (isset($f_options)) { ?>
<div class="filter">
    <div>
        <select onchange="table.Filter(this)">
            <option value="all">Filter by...</option>
            <?= implode('', $f_options)?>
        </select><input type="text" onkeyup="table.Filter(this); var clear = document.getElementById('filter-input-clear'); if(this.value!==''){clear.removeAttribute('class');} else{clear.setAttribute('class','d-none');}"
                    data-table-id="<?=$items?>-table"
                    placeholder="Type filter..." value="<?=$f_value?>"><a
                    class="d-none"
                    id="filter-input-clear" href="javascript:void(0);"
                    onclick="this.previousSibling.value='';this.previousSibling.setAttribute('data-reset',true);table.Filter(this.previousSibling);this.setAttribute('class','d-none');">&times;</a>
    </div>
</div><?php
} ?>
<table<?=$table_attributes?>>
    <thead>
        <tr><?=$ths?></tr>
    </thead>
    <tbody>
        <?=$body?>
    </tbody>
    <tfoot>
        <?=$footer?>
    </tfoot>
</table><script>window.addEventListener('load', function(){table.init("<?=$items?>-table");});</script>
</div>
