<div class="footer">
    <div class="data"><?= ($exportLinks ?? '') ?><?=$showing?>
    </div><div class="paging">
        <ul>
        <?php if (isset($pages)) { ?>
            <?=$jumpL;?>
            <li data-page="<?=($page-1)?>"><?=($page !== 1) ? "<a>$arrowL</a>" : "&nbsp;";?></li><?php
            for ($i=$start; $i <= $final; $i++) {
                $attr = $i !== $page ? null : ' class="a"';
                ?><li<?=$attr?>><?=!isset($attr) ? "<a>$i</a>" : $i;?></li><?php
            }
            ?><li data-page="<?=($page+1)?>"><?=($page < $pages) ? "<a>$arrowR</a>" : "&nbsp;";?></li>
            <?=$jumpR;?>
        <?php } ?>
        </ul>
    </div>
</div>
