<?php
/**
 * The Table package base class.
 *
 * @category Helper
 *
 * @author Plamen Marinov <plamen@webdiv.live>
 * @license https://bitbucket.org/pimarinov/table/src/master/LICENSE.md MIT
 */

namespace Pimarinov\Table;

use Pimarinov\Table\Sections\Thead;
/**
 * Note: Table::prepare() - call must come before `headers_sent()`;
 *     1. Create (initial setup):
 *        <code>Table::create('tableId', 'sortByDbCol', 'sortDirection');</code>
 *     1.1. Set columns to be displayed: <code>
 *          Table::$cols[] = ['innerHtml', 'dbColumn|Name',
 *                                      ['width'=>'?px','sort'=>false]];</code>
 *     2. Execute (data query)
 *        <code>Table::execute(sqlQuery);</code>
 *     2.1. Alter loaded data, before table load <code>
 *          foreach(Table::$data as $r => &$cells){
 *              $cells['id'] = $cells['id']==3 ?
 *                              [$cells['id'] ,['class'=>'red']] : $cells['id'];
 *          }</code>
 *     3. Loads the markup, or json data (on sort/page/export etc. actions).
 *        <code>Table::load();</code>.
 */
class Table extends Thead
{
    public static $totalCountCache = null;

    /**
     * Adds needed JavaScript and CSS into the page header, it has to be
     * included before "headers_sent()".
     *
     * @param string|bool $setOrCheck - uses helper class to verify own load
     */
    public static function prepare($setOrCheck = false)
    {
        //@see  http://php.net/manual/es/function.filter-input.php#77307
        $uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)
            ?: filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);

        $extension = (string) pathinfo(strtok($uri, '?'), PATHINFO_EXTENSION);

        self::$t['slug'] = pathinfo($uri, PATHINFO_BASENAME);

        self::prepareForExtensiveOperation();

        self::$pageExt = strtolower(/** @scrutinizer ignore-type */ $extension);

        if (self::$helperClass::prepare(__METHOD__, $setOrCheck) !== true) {

            if (self::$pageExt === 'json' && !isset(self::$t['prepared'])) {

                self::$t['prepared'] = true;
            }
        }
    }

    /**
     * #1. Create (setup).
     *
     * @param string $items    - The items name
     * @param string $orderBy  - a db column name
     * @param string $orderDir - (Default: self::DEFAULT_SORT_ORDER)
     * @param int    $paging   - Rows per page
     */
    public static function create(
        $items,
        $orderBy,
        $orderDir = 'asc',
        $paging = 10
    ) {
        self::reset((self::$t['items'] = (string) $items));
        self::prepare(true);
        self::$t['order']['col'] = $orderBy;
        $dir = strtolower($orderDir);
        self::$t['order']['dir'] = in_array($dir, ['asc', 'desc']) ?
            $dir : self::error('Invalid orderDir (Asc/Desc): '.$orderDir);
        self::$t['paging'] = ($num = abs($paging)) >= 10 ?
            $num : self::error('Invalid paging (<10): '.$paging);
    }

    /**
     * #2. Execute (queries).
     *
     * @param string        $query  - query for the table data;
     * @param (null)|string $qTotal - simple version of $sql if applicable
     *                              (used only for Total count in the table footer).
     *
     * Example: <pre>$sql = 'SELECT `id`, ... FROM `table` WHERE ...'</pre>
     * For query to many thousands results, will be faster to have:
     * <pre>$sqlTotals = 'SELECT `id` FROM `table` WHERE ...'</pre>
     */
    public static function execute($query, $qTotal = null)
    {
        self::$t = self::request(self::$export);

        $order = [self::$t['order']['col'] => self::$t['order']['dir']];
        $offset = (self::$t['page'] - 1) * self::$t['paging'];
        $limit = [$offset, self::$t['paging']];
        self::$t['q'] = self::query($query, self::$t['filter'], $order, $limit, true);

        $qAll = isset($qTotal) && !self::$t['filter'] ? $qTotal : $query;
        $ordAll = !self::$export ? [] : $order;
        self::$t['qAll'] = self::query($qAll, self::$t['filter'], $ordAll, 0, true);

        $q = !self::$export ? self::$t['q'] : self::$t['qAll'];

        try {
            self::$data = self::select($q);
        } catch (\Exception $e) {
            self::error('ERROR: '.$q.'<br />'.$e->getMessage());
        }
    }

    /**
     * #3. Loads output (HTML, JSON or CSV file).
     */
    public static function load()
    {
        if (self::$pageExt !== 'json') {
            echo parent::load();
        } else {
            $tableId = filter_input(INPUT_GET, 'table-id') ?: //4 testing
                       filter_var($_GET['table-id'], FILTER_SANITIZE_STRING);
            if ($tableId === self::$t['items'].'-table') {

                if (!self::$export) {

                    $jsonArr = ['body' => self::jsonTbody(),
                        'footer'       => self::jsonTfoot(), ];
                    return $jsonArr;
                    echo json_encode($jsonArr);
                } else {
                    self::completeAnExtensiveOperation('total-cache-active');
                    return self::export();
                }
            }
        }
    }
}
