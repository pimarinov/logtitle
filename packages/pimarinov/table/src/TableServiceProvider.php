<?php

namespace Pimarinov\Table;

use Illuminate\Support\ServiceProvider;

class TableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'table');
        $this->publishes([
            __DIR__.'/resources/css' => public_path('vendor/table/css'),
            __DIR__.'/resources/js' => public_path('vendor/table/js'),
        ], 'table');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make(Table::class);
    }
}
