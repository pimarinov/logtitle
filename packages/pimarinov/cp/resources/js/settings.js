var page = {
    name:   'settings',
    onload: function ()
    {
        console.log("current page name is: " + this.name);
        settingsHomeInitThemeForm();
    }
}.onload(); //@webdiv: without the `.onload()` the cp-pages will not call it, the cp-base.js is loaded earlier

function settingsHomeInitThemeForm()
{
    var radios = document.querySelectorAll('.theme.form-group input.form-check-input');

    var current_value = localStorage.getItem("cp");

    var current = current_value ? JSON.parse(current_value).theme : "system";

    for (var i = 0; i < radios.length; i++)
    {
        radios[i].setAttribute("onchange", "settingsHomeThemeChanges(this);");

        if (current === radios[i].getAttribute("value"))
        {
            radios[i].setAttribute("checked", false);
        }
    }
}
function settingsHomeThemeChanges(radio)
{
    var new_value = radio.getAttribute("value");

    if(new_value === "system")
    {
        localStorage.removeItem("cp");
    } 
    else 
    {
        localStorage.setItem("cp", JSON.stringify({theme: new_value}));
    }

    if(document.body.classList.contains("dark"))
    {
        document.body.classList.remove("dark");
    }

    return setPageTheme();
}

/*
 * User theme detection:
 * 
 * @see https://www.freecodecamp.org/news/how-to-detect-a-users-preferred-color-scheme-in-javascript-ec8ee514f1ef/
 * @see https://ourcodeworld.com/articles/read/1114/how-to-detect-if-the-user-prefers-a-light-or-dark-color-schema-in-the-browser-with-javascript-and-css
 * 
 * The method by which the user expresses their preference can vary. 
 * It might be a system-wide setting exposed by the Operating System,
 * or a setting controlled by the User Agent.
 * 
 * lets define 3 options:
 * Dark
 * Light
 * System Preference (OS|UA) (DEFAULT)
 */
/*const userPrefersDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
if(userPrefersDark){
    console.log("User prefers a dark interface");
}*/
