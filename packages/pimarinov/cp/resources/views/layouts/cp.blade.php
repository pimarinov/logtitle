<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('cp::partials.head')
</head>
<body>
    <section id="header">
        <nav class="navbar navbar-expand-sm align-items-start">
            <div class="navbar-nav mr-auto">
                <div class="navbar-brand">
                    <a href="{{ route('site.home') }}">{{ $website_name }}</a>
                    <small>&rsaquo;</small>
                    <a class="cp" href="{{ route('cp::home') }}">cP</a>
                </div>
            </div>
            <div class="navbar-nav ml-auto align-items-end">
                <button class="navbar-toggler d-md-none" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarsExample03">
                    @include('cp::partials.main-nav') 
                </div>
            </div>
        </nav>
    </section>
    <section id="header-ext">
        @include('cp::partials.sub-nav')
    </section>
    <section id="header-breadcrumbs" class="breadcrumbs"></section>
    <section id="page-content">
        @if (isset($content))
            @yield($content)
        @else 
            {{-- <h0>UNSET BLADE</h0>
            <var>$section: `{{ $section }}`</var>
            <var>$sub: `{{ $sub }}`</var>
            <var>--> message by: `cp.layout.cp`</var> --}}
            @if (!isset($blade))
                @if (!isset($sub))
                    @yield('content.' . $section) 
                @else
        123 {{ 'content.' . $section . '.' . $sub }}
                    @yield('content.' . $section . '.' . $sub) 
                @endif
            @else
                    <h1>Eha: 'content.' . {{$blade}}</h1>
                @yield('content.' . $blade)
            @endif 
        @endif 
    </section>
    <section id="footer-breadcrumbs" class="breadcrumbs"></section>
    <div id="footer">
        @include('cp::partials.footer') 
    </div>
</body>
</html>
