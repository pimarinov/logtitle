@if (!isset($extension))
    @extends('cp.layouts.cp')

    @section('content.users.users')
        <div class="container-fluid">
            <div class="page-header border-bottom d-flex">
                <h1 class="mr-auto p-2">{{ucfirst($section)}}</h1>
                <div class="mt-auto p-2">
                    @php ($item = str_singular($section))
                    <a href="/cp/{{$section}}/create" class="btn-add btn btn-primary d-flex" 
                       title="Add {{$item}}"><i class="material-icons">add</i>Add {{$item}}
                    </a>
                </div>
                </div>
            </div>
        </div>
        {!! \App\Http\Controllers\Cp\Tables::$sub() !!}
    @endsection
@else{!! die(\App\Http\Controllers\Cp\Tables::$sub()) !!}@endif
