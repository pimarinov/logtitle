
    <div class="container">
        <div class="row mt-5">
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">
                            <a href="/cp/purls">
                                <strong>Purls</strong>
                                <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->purls_count}}</span></h5>
                            </a>
                    </div>
                    <div class="card-body">
                        <h6 class="card-subtitle text-black-50 mb-1 small">Recently changed:</h6>
                        @foreach ($panels->purls_recents as $p)
                            <p class="card-text small mb-1">
                                <a href="/cp/purls{{$p->id}}">Purl#{{$p->id}}</a>
                                <span class="text-muted float-right" title="{{$p->datetime}}">{{$p->updated->diffForHumans()}}</span>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">
                            <a href="/cp/sections">
                                <strong>Sections</strong>
                                <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->sections_count}}</span></h5>
                            </a>
                    </div>
                    <div class="card-body">
                        <h6 class="card-subtitle text-black-50 mb-1 small">Recently changed:</h6>
                        @foreach ($panels->sections_recents as $p)
                            <p class="card-text small mb-1">
                                <a href="/cp/sections{{$p->id}}">Section#{{$p->id}}</a>
                                <span class="text-muted float-right" title="{{$p->datetime}}">{{$p->updated->diffForHumans()}}</span>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">
                            <a href="/cp/pages">
                                <strong>Pages</strong>
                                <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->pages_count}}</span></h5>
                            </a>
                    </div>
                    <div class="card-body">
                        <h6 class="card-subtitle text-black-50 mb-1 small">Recently changed:</h6>
                        @foreach ($panels->pages_recents as $p)
                            <p class="card-text small mb-1">
                                <a href="/cp/pages{{$p->id}}">Page#{{$p->id}}</a>
                                <span class="text-muted float-right" title="{{$p->datetime}}">{{$p->updated->diffForHumans()}}</span>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 mb-5">
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text mb-0">
                            <a href="/cp/users">Users</a>
                            <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->users_count}}</span></h5>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text mb-0">
                            <a href="/cp/langs">Langs</a>
                            <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->langs_count}}</span></h5>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm mb-3">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text mb-0">
                            <a href="/cp/vars">Vars</a>
                            <span class="badge badge-pill badge-dark float-right" title="Count">{{$panels->vars_count}}</span></h5>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
