@extends('cp::layouts.cp')

@section('home.home')
    {{-- @include('cp::pages.home.home_panels') --}}
    {{-- @include('cp::pages.home.home_analytics') --}}
	<div class="container-fluid" style="margin-top: 20px">
		@include('cp::partials.page-header')
		<div class="row no-gutters my-2">
			<div class="col-lg mb-3 mr-lg-1">
				<div class="card">
					<div class="card-body py-0">
						<h5 class="card-title mb-1">
							<a class="text-decoration-none" href="{{ route('cp::website.home') }}">Website</a>
						</h5>
						<p class="font-italic text-muted mb-1"><small>Front website administration.</small></p>
					</div>
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('cp::dummy-users.index') }}">
								Dummy users
							</a>
							<small class="text-muted">~{{ $dummy_users_count }}</small>
							<a class="text-primary ml-4" href="{{ route('cp::dummy-users.create') }}">+ New</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg mb-3 ml-lg-1">
				<div class="card">
					<div class="card-body py-0">
						<h5 class="card-title mb-1">
							<a class="text-decoration-none" href="{{ route('cp::settings.home') }}">
								Settings
							</a>
						</h5>
						<p class="font-italic text-muted mb-1"><small>Cp administration.</small></p>
					</div>
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('cp::users.index') }}">
								Users
							</a>
							<small class="text-muted">{{ $users_count }}</small>
							<a class="text-primary ml-4" href={{ route('cp::users.create') }}"">+ New</a>
						</li>
						<li class="list-group-item d-flex">
							<a class="flex-fill" href="{{ route('cp::vars.index') }}">
								Vars
							</a>
							<a class="text-primary ml-4" href="{{ route('cp::vars.create') }}">+ New</a>
						</li>
						<li class="list-group-item">
							<a href="{{ route('cp::settings.account') }}">Account</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		
	</div>
@endsection
