                @foreach($langs as $code => $name)
                    @if ($name!==$lang_default)<hr /> @endif
                    <div class="form-group row">
                        <label for="content-{{ $code }}" class="col-sm-12 col-form-label">Content ({{ $name }}):</label>
                        <div class="col-sm-12">
                            <textarea class="form-control{{$errors->has('content-' . $code) ? ' is-invalid' : ''}}" name="content-{{ $code }}" id="content-{{ $code }}" rows="1"/>{{ old('content-' . $code) ?? $vals['content-' . $code] }}</textarea>
                            <div class="invalid-feedback">{{ preg_replace('/(-)([A-Z]{2})/i', ' ($2)', $errors->first('content-' . $code)) ?? '' }}</div>
                        </div>
                    </div>
                @endforeach