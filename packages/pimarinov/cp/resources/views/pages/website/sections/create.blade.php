@extends('cp.layouts.cp')

@section('content.website.sections.create')
        <!-- website.sections.create -->
        <div class="container">
            @include('cp.partials.create.head')
            <form method="post" action="{{ route($items . '.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @csrf
                @include('cp.pages.website.sections.fields') 
                @include('cp.partials.create.submit')
            </form>
        </div>
@endsection