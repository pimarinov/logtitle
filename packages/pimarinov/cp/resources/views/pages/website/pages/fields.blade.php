                    <div class="border-bottom mb-3">
                        <div class="form-group row">
                            <div class="col-sm-2 d-flex justify-content-start">
                                <div class="dropdown show pull-right">
                                    <a class="btn-add btn btn-primary d-flex dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="material-icons">add</i>Pageable
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuLink">
                                      <a class="dropdown-item" href="/cp/purls/create">Purl</a>
                                    </div>
                                </div>
                            </div>
                            <label for="pageable" class="col-sm-2 col-form-label">Pageable:</label>
                            <div class="col-sm-8">
                                @if (isset($pageables)) 
                                    <select class="form-control{{$errors->has('pageable') ? ' is-invalid' : ''}}" name="pageable" value="{{ old('pageable') }}" id="pageable" tabindex="1"{{isset($page->id)?" readonly":""}}/>
                                        <option disabled selected value>Please select</option>
                                    @foreach($pageables as $k=>$v)
                                        <option value="{{ $k }}"{{ (old('pageable') ?? $page->pageable) === $k ? ' selected' : ''}}>{{ $v }}</option>
                                    @endforeach
                                    </select>
                                    <div class="invalid-feedback">{{$errors->first('pageable') ?? '' }}</div>
                                @else
                                    <span class="form-control-plaintext">{{$page->pageable}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="border-bottom mb-2">
                        <div class="form-group row">
                            <div class="col-sm-2 d-flex justify-content-start">
                                <a href="/cp/sections/create" class="btn-add btn btn-primary d-flex" title="Add page"><i class="material-icons">add</i>Add section</a>
                            </div>
                            <label for="pageable" class="col-sm-2 col-form-label">Section:</label>
                            <div class="col-sm-8" id="select-container">
                                <select class="form-control{{$errors->has('section') ? ' is-invalid' : ''}}" name="section" value="{{ old('section') }}" id="section" tabindex="1" onchange="allowAssigning();"/>
                                    <option disabled selected value>Please select</option>
                                @foreach($sections_available as $k=>$v)
                                    <option value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                                </select>
                                <div class="invalid-feedback">{{$errors->first('pageable') ?? '' }}</div>
                            </div>
                            <div class="col-sm-2 d-none" id="assign-container">
                                <a href="javascript:asignSelectedSection();" class="btn-add btn btn-primary d-flex" title="Assign section"><i class="material-icons">link</i>Assign section</a>
                            </div>
                        </div>
                    </div>
                    <!--div class="nav border-bottom d-flex mb-5">
                        <h4 class="mr-auto p-2">Assigned sections</h4>
                    </div-->
                    <ul id="sortable" class="list-group">
                        @foreach($sections as $section)
                            <li class="list-group-item list-group-item-action mt-2">
                                <div class="d-flex w-100 justify-content-between mb-1 mt-1">
                                    <div class="text-left">
                                        <span class="material-icons align-middle bg-primary text-white">reorder</span>
                                        <small class="align-middle"></small>
                                    </div>
                                    <div class="text-right">
                                        <a href="{{route('sections.edit', $section['section_id'])}}"><i class="material-icons md-18 align-text-top text-primary">edit</i></a>
                                        <small class="align-middle ml-2" title="{{$section['created_at']}}">Created: {{$section['created_human']}}</small>,
                                        <small class="align-middle" title="{{$section['updated_at']}}">Updated: {{$section['updated_human']}}</small>
                                        <span onclick="javascript:remove(this);"><span class="material-icons align-middle bg-primary text-white ml-2">clear</span></span>
                                    </div>
                                </div>
                                <input type="hidden" name="sections[]" value="{{ $section['section_id'] }}"/>
                                <div class="form-group mb-2 mt-2">
                                    <textarea class="form-control border-primary text-monospace" disabled rows="2">{{ substr($section['sections'][0], 2) }}</textarea>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <script src="{{ asset('add/cp/incl/pages.js') }}" defer></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" async></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" defer></script>
                    <script>
                        var linkElement = this.document.createElement('link');
                            linkElement.setAttribute('href', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');
                            linkElement.setAttribute('rel', 'stylesheet');
                        document.head.appendChild(linkElement);
                        (function(){
                            $(function() {
                                $( "#sortable" ).sortable({ axis: "y" });
                                $( "#sortable" ).disableSelection();
                            });
                        })();
                    </script>
                    <fieldset disabled class="d-none">
                        <ul id="sections_available" class="list-group">
                        @foreach($sections_content as $id => $section)
                            <li id="sec-{{ $id }}" class="list-group-item list-group-item-action mt-2">
                                <div class="d-flex w-100 justify-content-between mb-1 mt-1">
                                    <div class="text-left">
                                        <span class="material-icons align-middle bg-primary text-white">reorder</span>
                                        <small class="align-middle"></small>
                                    </div>
                                    <div class="text-right">
                                        <a href="{{route('sections.edit', $id)}}"><i class="material-icons md-18 align-text-top text-primary">edit</i></a>
                                        <small class="align-middle ml-2" title="{{$section['created_at']}}">Created: {{ $section['created_human'] }}</small>,
                                        <small class="align-middle" title="{{$section['updated_at']}}">Updated: {{ $section['updated_human'] }}</small>
                                        <span onclick="javascript:remove(this);"><span class="material-icons align-middle bg-primary text-white ml-2">clear</span></span>
                                    </div>
                                </div>
                                <input type="hidden" name="sections[]" value="{{ $id }}"/>
                                <div class="form-group mb-2 mt-2">
                                    <textarea class="form-control border-primary text-monospace" disabled rows="2">{{ $section['content'] }}</textarea>
                                </div>
                            </li>
                        @endforeach
                        </ul>
                    </fieldset>