 @extends('cp.layouts.cp')

<!-- website.pages.create.blade.php -->
@section('content.website.pages.create')
<div class="container">
    @include('cp.partials.create.head')
    <form method="post" action="{{ route($items . '.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
        @csrf
        @include('cp.pages.website.pages.fields_purl') 
        @include('cp.pages.website.pages.fields') 
        @include('cp.partials.create.submit')
    </form>
</div>
@endsection