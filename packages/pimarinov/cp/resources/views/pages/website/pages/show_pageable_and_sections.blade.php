            <div class="fields">
                <div class="d-flex align-content-start flex-wrap">
                    <div class="m-1 pt-3 pb-3 col-lg-3 text-right bg-light align-self-start">
                        Url (<a href="{{ $page->pageable_url }}">{{ $page->pageable }}</a>)
                    </div>
                    <div class="m-1 flex-grow-1 align-self-center">
                        @foreach ($slugs as $lang_id => $slug)
                            <span class="form-control-plaintext">
                                <kbd>{{ $langs_codes[$lang_id] }}</kbd>
                                <a href="{{ env('APP_URL') }}/{{$langs_codes[$lang_id]}}/{{ $slug }}" target="_blank">{{ env('APP_URL') }}/{{$langs_codes[$lang_id]}}/{{ $slug }}</a>
                            </span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="fields">
                <div class="d-flex align-content-start flex-wrap">
                    <div class="m-1 pt-3 pb-3 col-lg-3 text-right bg-light align-self-start">
                        Sections 
                    </div>
                    <div class="m-1 flex-grow-1 align-self-start">
                        @foreach ($page->sections as $section)
                            <span class="form-control-plaintext">
                                <a href="/cp/sections/{{ $section['section_id'] }}" target="_blank">
                                    Section#{{ $section['section_id'] }}
                                </a>,
                                @foreach ($section['sections'] as $content) 
                                    <kbd>{{ substr($content, 0, 2) }}</kbd>
                                @endforeach
                            </span>
                        @endforeach
                    </div>
                </div>
            </div>