@extends('cp.layouts.cp')

@section('content.website.pages.show')
        <!-- website.purls.show -->
        <div class="container">
            @include('cp.partials.page-alerts')
            @include('cp.partials.show.head', ['icon'=>'link'])
            @include('cp.pages.website.pages.show_pageable_and_sections')
            @include('cp.partials.show.fields', ['filed'=>['created_at', 'updated_at']])
            @include('cp.partials.show.foot')
        </div>
@endsection