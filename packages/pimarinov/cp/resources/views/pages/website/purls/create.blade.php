@extends('cp.layouts.cp')

<!-- wedite.purls.create.blade.php -->
@section('content.website.purls.create')
<div class="container">
    @include('cp.partials.create.head')
    <form method="post" action="{{ route($items . '.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
        @csrf
        @include('cp.pages.website.purls.fields') 
        @include('cp.partials.create.submit')
    </form>
</div>
@endsection