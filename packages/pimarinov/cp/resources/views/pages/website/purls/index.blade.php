@if (!isset($extension))
    @extends('cp.layouts.cp')

    <!-- website.purls.index.blade.php -->
    @section('content.website.purls.index')
        <div class="container-fluid">
            @include('cp.partials.page-alerts')
            @include('cp.partials.index.page-header')
        </div>
        @csrf
        {!! $table_class::$sub($undeletableKeys, $langs) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die($table_class::$sub($undeletableKeys, $langs)) !!}@endif
