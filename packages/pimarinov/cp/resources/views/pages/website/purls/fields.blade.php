                @foreach($langs as $code => $name)
                    @if ($name!==$lang_default)<hr /> @endif
                    <div class="form-group row">
                        <label for="slug-{{ $code }}" class="col-sm-2 col-form-label">Slug ({{ $name }}):</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control{{$errors->has('slug-'.$code) ? ' is-invalid' : ''}}" name="slug-{{ $code }}" value="{{ old('slug-' . $code) ?? $vals['slug-' . $code] }}" id="slug-{{ $code }}"/>
                            <div class="invalid-feedback">{{ preg_replace('/(-)([A-Z]{2})/i', ' ($2)', $errors->first('slug-' . $code)) ?? '' }}</div>
                        </div>
                    </div>
                    <div class="-inform-header d-flex mt-2 mb-3">
                        <h4 class="mr-auto">Head tags:</h4>
                    </div>
                    <div class="form-group row">
                        <label for="title-{{ $code }}" class="col-sm-2 col-form-label">Title ({{ $name }}):</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control{{$errors->has('title-'.$code) ? ' is-invalid' : ''}}" name="title-{{ $code }}" value="{{ old('title-' . $code) ?? $vals['title-' . $code] }}" id="title-{{ $code }}"/>
                            <div class="invalid-feedback">{{ preg_replace('/(-)([A-Z]{2})/i', ' ($2)', $errors->first('title-' . $code)) ?? '' }}</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="head_tags-{{ $code }}" class="col-sm-2 col-form-label">Keywords ({{ $name }}):</label>
                        <div class="col-sm-10">
                            <textarea class="form-control{{$errors->has('keywords-' . $code) ? ' is-invalid' : ''}}" name="keywords-{{ $code }}" id="keywords-{{ $code }}" rows="1"/>{{ old('keywords-' . $code) ?? $vals['keywords-' . $code] }}</textarea>
                            <div class="invalid-feedback">{{ preg_replace('/(-)([A-Z]{2})/i', ' ($2)', $errors->first('keywords-' . $code)) ?? '' }}</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="head_tags-{{ $code }}" class="col-sm-2 col-form-label">Description ({{ $name }}):</label>
                        <div class="col-sm-10">
                            <textarea class="form-control{{$errors->has('description-' . $code) ? ' is-invalid' : ''}}" name="description-{{ $code }}" id="head_tags-{{ $code }}" rows="1"/>{{ old('description-' . $code) ?? $vals['description-' . $code] }}</textarea>
                            <div class="invalid-feedback">{{ preg_replace('/(-)([A-Z]{2})/i', ' ($2)', $errors->first('description-' . $code)) ?? '' }}</div>
                        </div>
                    </div>
                @endforeach