@extends('cp.layouts.cp')

@section('content.website.purls.edit')
        <!-- website.purls.edit -->
        <div class="container">
            @include('cp.partials.edit.head')
            <form method="post" action="{{ route($items.'.update', $$item->id) }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @method('PUT')
                @csrf
                @include('cp.pages.website.purls.fields') 
                @include('cp.partials.edit.submit')
            </form>
        </div>
@endsection