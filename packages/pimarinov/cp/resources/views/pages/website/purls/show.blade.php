@extends('cp.layouts.cp')

@section('content.website.purls.show')
        <!-- website.purls.show -->
        <div class="container">
            @include('cp.partials.page-alerts')
            @include('cp.partials.show.head', ['icon'=>'link'])
            <div class="fields">
                @foreach ($langs as $code=>$lang_id)
                    @foreach ($lists as $name=>$values)
                        <div class="d-flex align-content-center flex">
                            <div class="m-1 pt-3 pb-3 col-lg-3 text-right bg-light align-self-center">
                                {{ucfirst(str_replace('_',' ',$name))}}
                            </div>
                            <div class="m-1 flex-grow-1 align-self-start">
                                <kbd>{{ $code }}</kbd>
                                @if (@$values[$lang_id]) 
                                    @if ($name==='slug')
                                        <a href="{{ env('APP_URL') }}/{{$code}}/{{ $values[$lang_id] }}" target="_blank">{{ env('APP_URL') }}/{{$code}}/{{ $values[$lang_id] }}</a>
                                    @else    
                                        {{ $values[$lang_id] }}
                                    @endif
                                @else
                                    <span class="text-muted">
                                        @if ($name==='slug')
                                            <a href="{{ env('APP_URL') }}/{{$code}}/{{ $values[$lang_default] }}" target="_blank">{{ env('APP_URL') }}/{{$code}}/{{ $values[$lang_default] }}</a>
                                        @else
                                            {{ $values[$lang_default] }}
                                        @endif
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    <hr>
                @endforeach
            </div>
            @include('cp.partials.show.fields', ['filed'=>['created_at', 'updated_at']])
            @include('cp.partials.show.foot')
        </div>
@endsection