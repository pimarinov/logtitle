@if (!isset($extension))
    @extends('cp.layouts.cp')

    <!-- settings.users.index.blade.php -->
    @section('content.users.users.index')
        <div class="container-fluid">
            @include('cp.partials.page-alerts')
            @include('cp.partials.index.page-header')
        </div>
        @csrf
        {!! \App\Http\Controllers\Cp\Tables::$sub($undeletableKeys) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die(\App\Http\Controllers\Cp\Tables::$sub($undeletableKeys)) !!}@endif
