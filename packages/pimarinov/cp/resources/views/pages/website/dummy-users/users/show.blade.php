@extends('cp.layouts.cp')

<!-- users.users.show.blade.php -->
@section('content.users.users.show')
<div class="container">
    @include('cp.partials.page-alerts')
    @include('cp.partials.show.head', ['icon'=>'account_circle'])
    @include('cp.partials.show.fields')
    @include('cp.partials.show.foot')
</div>
@endsection