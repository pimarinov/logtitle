<?php

namespace \resources\views\cp\pages\users\users;

use Table as table;

class index_users {

    final static function users($undeletableKeys) {
        $items = __FUNCTION__;
        $item = 'user';

        if(filter_has_var(INPUT_GET, "time")){ self::$rustart = getrusage(); }

        table::create("users", "id", "desc", 50);
        table::$cols[] = ["ID", "id", ["width"=>"50px"]];
        table::$cols[] = ["Name", "name"];
        table::$cols[] = ["Email", "email"];
        table::$cols[] = ["test","test",["sort"=>false]];
        table::$cols[] = ["Created", "created_at_formatted", 
                            ["sort"=>"created_at","width"=>"130px"]];
        table::$cols[] = ["Updated", "updated_at_formatted", 
                            ["sort"=>"updated_at","width"=>"130px"]];
        table::$cols[] = ["...", "links", ["width"=>"78px", "class"=>"links","sort"=>false]];
        table::$cols[] = ["...", "link", ["width"=>"40px", "class"=>"link","sort"=>false]];

        table::execute("SELECT *" . self::qdates() . " FROM users ");

        foreach(table::$data as &$cells){
            $cells = get_object_vars($cells); //object -> array
            $id = (int)$cells['id'];
            $cells["link"] = self::icoshow($items, $id, $item);
            $links = in_array($id, $undeletableKeys) ? 
                    [self::icoedit($items, $id, $item)] :
                    [self::icodel($items, $id, $item), self::icoedit($items, $id, $item)];
            $cells["links"] = implode('', $links);
            if(table::$export === false){
                $cells['name'] = self::show($items, $id, $cells['name']);
                $cells['id'] = self::id($items, $id);
                $cells['email'] = self::mailto($cells['email']);
            }
        }

        table::$attributes["table"]["class"] = "table-striped table-sm table-hover table-bordered";

        table::load();

        if(isset(self::$rustart)){ self::time(); }
    }
}