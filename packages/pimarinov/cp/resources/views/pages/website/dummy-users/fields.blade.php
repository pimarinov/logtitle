                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid' : ''}}" name="name" value="{{ old('name') ?? $$item->name }}" id="name"/>
                        <div class="invalid-feedback">{{$errors->first('name') ?? '' }}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Email:</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid' : ''}}" name="email" value="{{ old('email') ?? $$item->email }}" id="code" minlength="2" maxlength="2"/>
                        <div class="invalid-feedback">{{$errors->first('email') ?? '' }}</div>
                    </div>
                </div>
            @if ($action === 'create')
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Password:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control{{$errors->has('password') ? ' is-invalid' : ''}}" name="password" required autocomplete="new-password"/>
                        <div class="invalid-feedback">{{$errors->first('password') ?? '' }}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Confirm password:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control{{$errors->has('password_confirmation') ? ' is-invalid' : ''}}" name="password_confirmation" required autocomplete="new-password"/>
                    </div>
                </div>
            @endif