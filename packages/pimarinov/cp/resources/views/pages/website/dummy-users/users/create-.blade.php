<!-- users.users.create.blade.php -->

@extends('cp.layouts.cp')

@section('content.users.users.create')
<div class="container">
    <div class="page-header border-bottom d-flex mb-5">
            <h1 class="mr-auto p-2">Add {{ucfirst($item)}}</h1>
    </div><!--
    @if ($errors->any())
        <div class="alert alert-danger mb-5">
          <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div><br />
    @endif-->
    <form method="post" action="{{ route($items . '.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
        @csrf
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid' : ''}}" name="name" value="{{ old('name') }}" id="name"/>
                <div class="invalid-feedback">{{$errors->first('name') ?? '' }}</div>
            </div>
        </div>
        <div class="form-group row">
            <label for="code" class="col-sm-2 col-form-label">Code:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control{{$errors->has('email') ? ' is-invalid' : ''}}" name="email" value="{{ old('email') }}" id="email"/>
                <div class="invalid-feedback">{{$errors->first('email') ?? '' }}</div>
            </div>
        </div>
        <div class="form-group row">
            <label for="code" class="col-sm-2 col-form-label">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control{{$errors->has('password') ? ' is-invalid' : ''}}" name="password"/>
                <div class="invalid-feedback">{{$errors->first('password') ?? '' }}</div>
            </div>
        </div>
        <div class="form-group row">
            <label for="code" class="col-sm-2 col-form-label">Confirm password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control{{$errors->has('password_confirmation') ? ' is-invalid' : ''}}"  name="password_confirmation" required autocomplete="new-password"/>
                <div class="invalid-feedback">{{$errors->first('password_confirmation') ?? '' }}</div>
            </div>
        </div>
        <div class="form-group row mt-5">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary col-md-2">Create {{ucfirst($item)}}</button>
                <button type="button" class="btn btn-light col-md-2" onclick="location.assign('{{ route($items . '.index') }}')">Cancel</button>
            </div>
        </div>
    </form>
</div>
@endsection