@if (!isset($extension))
    @extends('cp::layouts.cp')
    <!-- website.users.index.blade.php -->
    @section('website.dummy-users.index')
        <div class="container-fluid">
            @include('cp::partials.page-alerts')
            @include('cp::partials.page-header')
        </div>
        @csrf
        {!! Pimarinov\Cp\App\Http\Tables\DummyUsers::users([]) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die(Pimarinov\Cp\App\Http\Tables\DummyUsers::users([])) !!}@endif
