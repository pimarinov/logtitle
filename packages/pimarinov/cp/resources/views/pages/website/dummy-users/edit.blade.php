@extends('cp.layouts.cp')

@section('content.users.users.edit')
        <!-- settings.users.edit -->
        <div class="container">
            @include('cp.partials.edit.head')
            <form method="post" action="{{ route($items.'.update', $$item->id) }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @method('PUT')
                @csrf
                @include('cp.pages.users.users.fields') 
                @include('cp.partials.edit.submit') 
            </form>
        </div>
@endsection