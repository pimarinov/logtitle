@extends('cp.layouts.cp')

@section('content.users.users.create')
        <!-- users.users.create -->
        <div class="container">
            @include('cp.partials.create.head')
            <form method="post" action="{{ route($items . '.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @csrf
                @include('cp.pages.users.users.fields') 
                @include('cp.partials.create.submit') 
            </form>
        </div>
@endsection