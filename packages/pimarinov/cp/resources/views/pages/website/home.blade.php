@extends('cp::layouts.cp')

@section('website.home')
	<div class="container-fluid" style="margin-top: 20px">
		@include('cp::partials.page-header')
		<div class="row no-gutters pt-2">
			<div class="col-lg mb-3 mr-lg-01">
				<div class="card">
					<ul class="list-group">
						<li class="list-group-item d-flex align-items-center">
							<a class="flex-fill" href="{{ route('cp::dummy-users.index') }}">
								Dummy users
							</a>
							<small class="text-muted">~{{ $dummy_users_count }}</small>
							<a class="text-primary ml-4" href="{{ route('cp::dummy-users.create') }}">+ New</a>
						</li>
					</ul>
				</div>
			</div>
			<!--div class="col-lg mb-3 ml-lg-1">
				<div class="card">
				</div>
			</div-->
		</div>
		
		
		
	</div>
@endsection
