@if (!isset($extension))
    @extends('cp::layouts.cp')

    <!-- settings.vars.index.blade.php -->
    @section('settings.vars.index')
        <div class="container-fluid">
            @include('cp::partials.page-alerts')
            @include('cp::partials.page-header')
        </div>
        @csrf
        {!! Pimarinov\Cp\App\Http\Tables\Vars::vars([]) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die(Pimarinov\Cp\App\Http\Tables\Vars::vars([])) !!}@endif
