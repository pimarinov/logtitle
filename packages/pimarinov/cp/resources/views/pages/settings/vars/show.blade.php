@extends('cp.layouts.cp')

<!-- settings.vars.show.blade.php -->
@section('content.settings.vars.show')
<div class="container">
    @include('cp.partials.page-alerts')
    @include('cp.partials.show.head', ['icon'=>'settings_ethernet'])
    @include('cp.partials.show.fields')
    @include('cp.partials.show.foot')
</div>
@endsection