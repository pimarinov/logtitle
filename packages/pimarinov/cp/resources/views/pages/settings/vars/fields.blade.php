                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid' : ''}}" name="name" value="{{ old('name') ?? $var->name  }}" id="name"/>
                        <div class="invalid-feedback">{{$errors->first('name') ?? '' }}</div>
                    </div>
                    <label for="group" class="col-sm-1 col-form-label">Group:</label>
                    <div class="col-sm-3">
                        <select class="form-control{{$errors->has('group') ? ' is-invalid' : ''}}" name="group" value="{{ old('group') }}" id="group"/>
                            <option disabled selected value>Please select</option>
                            @foreach($groups as $k=>$v)
                                <option value="{{ $k }}"{{ (old('group') ?? $var->group) === $k ? ' selected' : ''}}>{{ ucfirst(str_replace('-',' ',$v)) }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">{{$errors->first('group') ?? '' }}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="value" class="col-sm-2 col-form-label">Value:</label>
                    <div class="col-sm-10">
                        <textarea class="form-control{{$errors->has('value') ? ' is-invalid' : ''}}" name="value" id="value"/>{{ old('value') ?? $var->value }}</textarea>
                        <div class="invalid-feedback">{{$errors->first('value') ?? '' }}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="comment" class="col-sm-2 col-form-label">Comment:</label>
                    <div class="col-sm-10">
                        <textarea class="form-control{{$errors->has('comment') ? ' is-invalid' : ''}}" name="comment" id="commen"/>{{ old('comment') ?? $var->comment }}</textarea>
                        <div class="invalid-feedback">{{$errors->first('comment') ?? '' }}</div>
                    </div>
                </div>