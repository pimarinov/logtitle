@extends('cp::layouts.cp')

@section('settings.users.edit')
        <!-- settings.users.edit -->
        <div class="container">
            
            <form method="post" action="{{ route('cp::users.update', $user->id) }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @method('PUT')
                @csrf
                @include('cp::pages.settings.users.fields')
                @include('cp::partials.edit.submit') 
            </form>
        </div>
@endsection