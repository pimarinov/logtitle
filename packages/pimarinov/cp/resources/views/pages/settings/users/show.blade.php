@extends('cp::layouts.cp')

<!-- users.users.show.blade.php -->
@section('settings.users.show')
    @include('cp::partials.breadcrumbs', ['items'=>'users', 'item'=>'user'])
<div class="container mb-5">
    @include('cp::partials.page-alerts')
    @include('cp::partials.show.head', ['icon'=>'account_circle', 'item'=>'user'])
    @include('cp::partials.show.fields', ['item'=>'user', 'fields'=>[
            'name', 
            'email',
            'created_at',
            'updated_at'
        ]
    ])
    @include('cp::partials.show.foot', ['item'=>'user', 'items'=>'users', 'undeletableKeys'=>[]])
</div>
@endsection