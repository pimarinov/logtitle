@extends('cp::layouts.cp')

@section('settings.users.create')
        <div class="container-fluid">
            @include('cp::partials.page-alerts')
            @include('cp::partials.page-header', ['items'=>null])
        </div>
        <!-- users.users.create -->
        <div class="container">
            @include('cp::partials.create.head', ['items'=>'users','item'=>'user'])
            <form method="post" action="{{ route('cp::users.store') }}"{{($errors->any()) ? ' class="was-validated"': ''}}>
                @csrf
                @include('cp::pages.settings.users.fields', ['item'=>'user']) 
                @include('cp::partials.create.submit', ['items'=>'users','item'=>'user']) 
            </form>
        </div>
@endsection