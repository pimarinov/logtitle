@if (!isset($extension))
    @extends('cp::layouts.cp')
    <!-- settings.users.index.blade.php -->
    @section('settings.users.index')
        <div class="container-fluid">
            @include('cp::partials.page-alerts')
            @include('cp::partials.page-header')
        </div>
        @csrf
        {!! Pimarinov\Cp\App\Http\Tables\Users::users([]) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die(Pimarinov\Cp\App\Http\Tables\Users::users([])) !!}@endif
