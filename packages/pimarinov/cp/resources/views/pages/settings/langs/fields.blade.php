                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="{{ old('name') ?? $$item->name }}" id="name" class="form-control{{$errors->has('name') ? ' is-invalid' : ''}}"/>
                        <div class="invalid-feedback">{{$errors->first('name') ?? '' }}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Code:</label>
                    <div class="col-sm-10">
                        <input type="text" name="code" value="{{ old('code') ?? $$item->code }}" id="code" class="form-control{{$errors->has('code') ? ' is-invalid' : ''}}" minlength="2" maxlength="2"/>
                        <div class="invalid-feedback">{{$errors->first('code') ?? '' }}</div>
                    </div>
                </div>