@extends('cp.layouts.cp')

<!-- settings.langs.show.blade.php -->
@section('content.settings.langs.show')
<div class="container">
    @include('cp.partials.page-alerts')
    @include('cp.partials.show.head', ['icoDivId'=>'show-head-flag', 'image'=>asset('add/img/flags/'.($lang->code==='en'?'gb':$lang->code).'.png')])
    @include('cp.partials.show.fields')
    @include('cp.partials.show.foot')
</div>
@endsection