@extends('cp.layouts.cp')

@section('content.settings.langs.create') 
        <!-- settings.lang.create -->
        <div class="container">
            @include('cp.partials.create.head') 
            <form method="post" action="{{ route($items . '.store') }}"{{ ($errors->any()) ? ' class="was-validated"': '' }}>
                @csrf 
                @include('cp.pages.settings.langs.fields') 
                @include('cp.partials.create.submit') 
            </form>
        </div>
@endsection