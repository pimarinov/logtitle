@if (!isset($extension))
    @extends('cp.layouts.cp')

    <!-- settings.langs.index.blade.php -->
    @section('content.settings.langs.index')
        <div class="container-fluid">
            @include('cp.partials.page-alerts')
            @include('cp.partials.index.page-header')
        </div>
        @csrf
        {!! $table_class::$sub($undeletableKeys) !!}
    @endsection
{{-- do not put any formatting spaces below as this brakes the json output --}}
@else{!! die($table_class::$sub($undeletableKeys)) !!}@endif
