            {{-- cp::partials.page-header --}}
			<div class="page-header border-bottom">
				<div class="row align-items-end">
					<div class="col-sm mb-2 mb-sm-0">
						<nav class="d-none d-sm-block px-1" aria-label="breadcrumb">
							<ol class="breadcrumb breadcrumb-no-gutter m-0 p-0">

								<li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('cp::home') }}">Cp</a></li>

								@if($section !== 'home')
									<li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('cp::' . $section . '.home') }}">{{ ucfirst($section) }}</a></li>
								@endif

								@if(isset($items))
									<li class="breadcrumb-item active" aria-current="page">{{ ucfirst(str_replace(['_', '-'], ' ', $items)) }}</li>
								@else
									<li class="breadcrumb-item active" aria-current="page">{{ ucfirst(str_replace(['_', '-'], ' ', $action)) }}</li>
								@endif

							</ol>
						</nav>

						<h3 class="page-header-title">{{ ucfirst(str_replace(['_', '-'], ' ', $items ?? $section)) }}</h3>
					</div>
					@if (isset($items) && isset($create))
						<div class="col-sm-auto mb-2">
							<a href="{{ $create['url'] }}" class="btn-add">
								<i class="material-icons md-18 align-text-bottom">add</i> Create {{ str_replace(['_', '-'], ' ', $create['suffix']) }}
							</a>
						</div>
					@endif
				</div>
			</div>