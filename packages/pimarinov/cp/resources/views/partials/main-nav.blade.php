                    <!-- cp.partials.main-nav -->
                    <ul id="main-nav" class="navbar-nav mr-auto">
                        <li class="nav-item{{{ ($section==='home' ? ' active' : '') }}}">
                            <a class="nav-link" href="{{ url('/cp/home') }}">
                                <i class="material-icons-sharp d-md-none">home</i><strong class="d-sm-none d-md-inline">CP Home</strong><span></span>
                            </a>
                        </li>
                        <li class="nav-item{{{ ($section==='website' || (isset($item) && $item==='section') ? ' active' : '') }}}">
                            <a class="nav-link" href="{{ url('/cp/website') }}">
                                <i class="material-icons-sharp d-md-none">public</i><strong class="d-sm-none d-md-inline">Website</strong><span></span>
                            </a>
                        </li>
                        <li class="nav-item{{{ ($section==='settings' ? ' active' : '') }}}">
                            <a class="nav-link" href="{{ url('/cp/settings') }}">
                                <i class="material-icons-sharp d-md-none">settings</i><strong class="d-sm-none d-md-inline">Settings</strong><span></span>
                            </a>
                        </li>
                        <!--li class="nav-item{{{ ($section==='account' ? ' active' : '') }}}">
                            <a class="nav-link" href="{{ url('/cp/account') }}">
                                <i class="material-icons">person</i><strong class="d-sm-none d-md-inline">Account</strong><span></span>
                            </a>
                        </li-->
                        <!--li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown03">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                        </li-->
                    </ul>