@if (isset($sub_nav))
	<!-- cp.partials.sub-nav -->
	<ul id="sub-nav" class="nav nav-tabs pt-3 px-3"> 
	@foreach ($sub_nav as $sub_node)
		<li class="nav-item">
			<a class="nav-link{{ $sub_node===$sub? ' active' :'' }}" 
			   href="{{ url('/cp/' . ($sub_node!=='home'?$sub_node:$section))/*route($sub_node . '.index')*/ }}">
				{{ ucfirst(str_replace(['_', '-'], ' ', $sub_node)) }}
			</a>
		</li>
	@endforeach
	</ul>
@endif