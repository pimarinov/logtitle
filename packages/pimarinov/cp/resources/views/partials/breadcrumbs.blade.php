        <div class="container-fluid">
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-sm mb-2 mb-sm-0">
						<nav class="d-none d-sm-block px-1" aria-label="breadcrumb">
							<ol class="breadcrumb breadcrumb-no-gutter m-0 p-0">
								<li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('cp::home') }}">Cp</a></li>

								@if($section !== 'home')
									<li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('cp::' . $section . '.home') }}">{{ ucfirst($section) }}</a></li>
								@endif

								@if(isset($items))
									<li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('cp::' . $items . '.index') }}">{{ ucfirst(str_replace(['_', '-'], ' ', $items)) }}</a>
                                    </li>

                                    @if(isset($item) && isset($$item))
                                        @if($action!='show')
                                        <li class="breadcrumb-item" aria-current="page">
                                            <a href="{{ route('cp::' . $items . '.show', $$item->id) }}">
                                                {{ ucfirst(str_replace(['_', '-'], ' ', $$item->name)) }}
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">
                                            {{ ucfirst(str_replace(['_', '-'], ' ', $action)) }}
                                        </li>
                                        @else
                                            <li class="breadcrumb-item active" aria-current="page">
                                                {{ ucfirst(str_replace(['_', '-'], ' ', $$item->name)) }}
                                            </li>
                                        @endif
                                    @else
                                        <li class="breadcrumb-item active" aria-current="page">{{ ucfirst(str_replace(['_', '-'], ' ', $action)) }}</li>
                                    @endif
								@else
									<li class="breadcrumb-item active" aria-current="page">{{ ucfirst(str_replace(['_', '-'], ' ', $action)) }}</li>
								@endif

							</ol>
						</nav>
                    </div>
                </div>
            </div>
        </div>
