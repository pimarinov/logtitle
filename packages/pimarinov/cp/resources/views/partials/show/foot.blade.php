        <hr class="mt-5"/>
        <div class="d-flex align-content-center flex-wrap small">
            <div class="">
                @csrf
                <a href="javascript:cp.destroy({{$$item->id}}, '{{$item}}')" class="btn btn-outline-danger align-content-center align-middle{{in_array($$item->id, $undeletableKeys)?' disabled':''}}"><i class="material-icons md-18 mr-1 align-middle">delete</i>Delete {{ $item }}</a>
            </div>
            <div class="align-self-center flex-grow-1 text-right">
                <a class="btn btn-primary col-md-2" href="{{ route('cp::' . $items.'.edit',$$item->id) }}"><i class="material-icons md-18 align-middle mr-1">edit</i>Edit {{ $item }}</a>
                <a class="btn btn-outline-secondary col-md-2" href="{{ route('cp::' . $items . '.index') }}">{{ ucfirst($items) }} list</a>
            </div>
        </div>
