                {{-- cp.partials.form.create --}}
                <div class="form-group row mt-5">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary col-md-2">Create {{ $item }}</button>
                        <button type="button" class="btn btn-outline-secondary col-md-2" onclick="location.assign('{{ route('cp::' . $items . '.index') }}')">Cancel</button>
                    </div>
                </div>