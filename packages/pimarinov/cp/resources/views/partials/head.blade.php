	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/cp.ico') }}">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Cp &#10093; {!! $head_title !!} &mdash; {{ $website_name }}</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Sharp" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Montserrat:100,400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<!-- Scripts -->
	<!--script src="{{ asset('js/app.js') }}" defer></script-->
	<script src="{{ asset('pimarinov/cp/js/pages.js') }}" defer></script>
	<script src="{{ asset('pimarinov/cp/js/cp-base.js') }}" defer></script>
    @stack('head')
	<!-- Styles -->
	<link href="{{ asset('pimarinov/cp/css/base.css') }}" rel="stylesheet">
    <link href="{{ asset('pimarinov/cp/css/' . ($sub ?? $section) . '.css') }}" rel="stylesheet">
	{!! $assets ?? '' !!}
