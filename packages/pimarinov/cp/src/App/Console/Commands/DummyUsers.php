<?php

namespace Pimarinov\Cp\App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DummyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports `dummy_users` table records (1.3M)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$db_name = \DB::connection()->getDatabaseName();
		
		$config = \DB::connection()->getConfig();
		
		$db_user = $config['username'];
		$db_pass = $config['password'];

		$path = base_path(
			'packages/pimarinov/cp/database/catalogs/dummy_users.sql'
			);
		
		if($db_pass)
		{
			$format = 'mysql -u %s -p%s %s < %s';

			$command = sprintf($format, $db_user, $db_pass, $db_name, $path);
		}
		else 
		{
			$format = 'mysql -u %s %s < %s';

			$command = sprintf($format, $db_user, $db_name, $path);
		}
		
		echo "Executing:\r\n" . $command . "\r\n";
		
		exec($command);

		return 1;
    }
}
