<?php

namespace Pimarinov\Cp\App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\DB;
use Pimarinov\Cp\App\Http\Tables\DummyUsers;
use Pimarinov\Cp\App\Http\Tables\Users;

class HomeController extends CpController
{
    //use BaseTrait;

    public static $section = "home";

    const RECENTS_COUNT = 3;

    /**
     * The guard implementation.
     *
     * @var \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\StatefulGuard  $guard
     * @return void
     */
    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

	public function index()
	{
		$vars = [
			'action'  => self::$section,
			'section' => self::$section,
			'sub_nav' => [self::$section],
			'sub'     => self::$section,
		];

		$vars['users_count'] = Users::count();

		$vars['dummy_users_count'] = sprintf('%s Millions'
			, number_format((DummyUsers::count()/1000000), 2)
			);
		
		$this->vars($vars);

		return view('cp::pages.home.home', $vars);
	}

    /**
     * Cp Logout action
     * @see return AuthenticatedSessionController::destroy();
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        
        $this->guard->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('site.home'); //->with('success', $msg);
    }

	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(/* Request $request */)
	{
		$vars = self::vars(self::$section);
        
        /*$vars['langs'] = Lang::get(($ids_flag = true));
        $vars['lang_default'] = $vars['langs'][array_keys($vars['langs'])[0]];*/

        $this->panels_vars($vars);

        return view($vars['view'], $vars);
	}

    private function panels_vars(&$vars)
    {
        /*$ld_id = $vars['lang_default'];*/
        $d = DB::select('SELECT 
                (SELECT COUNT(`id`) FROM `purls`) AS `purls_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `purls` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                            ) AS `purls_r`
                    ) AS `purls_recents`,
                (SELECT COUNT(`id`) FROM `sections`) AS `sections_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `sections` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                            ) AS `sections_r`
                    ) AS `sections_recents`,
                (SELECT COUNT(`id`) FROM `pages`) AS `pages_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `pages` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                        ) AS `pages_r`
                    ) AS `pages_recents`,
                (SELECT COUNT(`id`) FROM `users`) AS `users_count`,
                (SELECT COUNT(`id`) FROM `langs`) AS `langs_count`,
                (SELECT COUNT(`id`) FROM `vars`) AS `vars_count`
            ')[0];
        
        foreach (['purls', 'sections', 'pages'] as $p) {
            $name = $p . '_recents';
            
            $current = [];
            
            $dd = explode(',', $d->$name);
            foreach ($dd as $ddd) {
                $id_date  = explode('|', $ddd);
                $updated = \Carbon\Carbon::createFromDate($id_date[1]);
                $current[] = (object)[  'id'=>$id_date[0],
                                        'updated'=>$updated,
                                        'datetime'=>$id_date[1]
                                    ];
            }
            
            $d->$name = $current;
        }
        
        $vars['panels'] = $d;
    }
	
}
