<?php

namespace Pimarinov\Cp\App\Http\Controllers\Website;

use Illuminate\Http\Request;
use Pimarinov\Cp\App\Http\Tables\DummyUsers;

/**
 * Description of HomeController
 */
class HomeController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$vars = [
			'action'=> 'home',
			'sub'	=> 'home',
		];

		$vars['dummy_users_count'] = DummyUsers::count();

		$this->vars($vars);

        return view('cp::pages.website.home', $vars);
    }
}
