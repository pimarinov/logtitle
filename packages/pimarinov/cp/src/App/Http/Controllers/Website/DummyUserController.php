<?php

namespace Pimarinov\Cp\App\Http\Controllers\Website;

use Illuminate\Http\Request;
use Pimarinov\Cp\App\Models\DummyUser;
use Pimarinov\Cp\App\Http\Tables\DummyUsers;

class DummyUserController extends BaseController
{
    public static $model = "DummyUser";
	const ITEMS = 'dummy-users';
    const ITEM = 'dummy-user';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$vars = [
			'sub'	=> DummyUser::ITEMS,
			'action'=> __FUNCTION__,
			'items' => DummyUser::ITEMS,
			'item'	=> DummyUser::ITEM,
			'create'=> [
				'url'=>route('cp::dummy-users.create')
				, 'suffix'=>DummyUser::ITEM
			]
		];
		
		$vars['assets'] = DummyUsers::init();

		$vars['extension'] = DummyUsers::$pageExtension;
		
		$this->vars($vars);

        return view('cp::pages.website.dummy-users.index', $vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DummyUser  $dummyUser
     * @return \Illuminate\Http\Response
     */
    public function show(DummyUser $dummyUser)
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DummyUser  $dummyUser
     * @return \Illuminate\Http\Response
     */
    public function edit(DummyUser $dummyUser)
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DummyUser  $dummyUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DummyUser $dummyUser)
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DummyUser  $dummyUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(DummyUser $dummyUser)
    {
        abort(403, 'Uncompleted Action.');
    }
}
