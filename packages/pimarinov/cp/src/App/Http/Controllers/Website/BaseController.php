<?php

namespace Pimarinov\Cp\App\Http\Controllers\Website;

use Pimarinov\Cp\App\Http\Controllers\CpController;

/**
 * Description of BaseController
 */
class BaseController extends CpController
{
	public static $section = "website";

	/**
     * @param array $vars
     */
	protected function vars(array &$vars)
	{
		parent::vars($vars);

		$vars['section'] = self::$section;
		$vars['sub_nav'] =  ['home', 'dummy-users'];
	}
}
