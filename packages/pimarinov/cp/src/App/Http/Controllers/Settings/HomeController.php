<?php

namespace Pimarinov\Cp\App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use Pimarinov\Cp\App\Models\User;
use Pimarinov\Cp\App\Http\Tables\Users;

/**
 * Description of HomeController
 */
class HomeController extends BaseController
{
    /**
     * Display a listing of the resource.
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$vars = [
			'action'=> 'home',
			'sub'	=> 'home',
			'gravatar'=> User::find(\Auth::user()->id)->gravatar,
			'users_count'=> Users::count(),
		];

		$this->vars($vars);

        return view('cp::pages.settings.home', $vars);
    }
}
