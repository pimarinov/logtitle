<?php

namespace Pimarinov\Cp\App\Http\Controllers\Settings;

/**
 * Description of AccountController
 */
class AccountController extends BaseController
{
    //const ITEMS = 'users';
    const ITEM = 'account';
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	function edit()
	{
		$vars = [
			'action'=> self::ITEM,
			'sub'	=> self::ITEM,
		];

		$this->vars($vars);

		return view('cp::pages.settings.account', $vars);
	}
}
