<?php

namespace Pimarinov\Cp\App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use Pimarinov\Cp\App\Models\Variable;
use Pimarinov\Cp\App\Http\Tables\Vars;

/**
 * Variables Controller
 */
class VariablesController extends BaseController
{
    const ITEMS = 'vars';
    const ITEM = 'var';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		abort(403, 'Uncompleted Action - missing table.');
		$vars = [
			'action'=> __FUNCTION__,
			'sub'	=> self::ITEMS,
			'items' => self::ITEMS,
			'item'	=> self::ITEM,
		];

		$vars['assets'] = Vars::init();

		$this->vars($vars);

        return view('cp::pages.settings.vars.index', $vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(403, 'Uncompleted Action.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		abort(403, 'Uncompleted Action.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function show(Variable $variable)
    {
		abort(403, 'Uncompleted Action.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function edit(Variable $variable)
    {
		abort(403, 'Uncompleted Action.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variable $variable)
    {
		abort(403, 'Uncompleted Action.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variable $variable)
    {
		abort(403, 'Uncompleted Action.');
    }
}
