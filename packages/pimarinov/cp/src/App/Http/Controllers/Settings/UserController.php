<?php

namespace Pimarinov\Cp\App\Http\Controllers\Settings;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Pimarinov\Cp\App\Models\User;
use Pimarinov\Cp\App\Http\Tables\Users;
//use Pimarinov\Table\Table as table;

class UserController extends BaseController
{
    public static $model = "User";
    const ITEMS = 'users';
    const ITEM = 'user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$vars = [
			'sub'	=> User::ITEMS,
			'action'=> __FUNCTION__,
			'items' => User::ITEMS,
			'item'	=> User::ITEM,
			'create'=> ['url'=>route('cp::users.create'), 'suffix'=>User::ITEM]
		];
		
		$vars['assets'] = Users::init();

		$vars['extension'] = Users::$pageExtension;
		
		$this->vars($vars);

        return view('cp::pages.settings.users.index', $vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vars = [
            'sub'	=> User::ITEMS,
            'action'=> __FUNCTION__,
        ];

        $vars['user'] = (object)['name' => null, 'email' => null, 
                                 'password' => null];
        $this->vars($vars);

        return view('cp::pages.settings.users.create', $vars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'=>'required|max:255|min:2',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8|confirmed'
        ]);

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $msg = $user->name . ' (#' . $user->id . ') is successfully saved.';
        return redirect()->route('cp::users.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //$vars = self::vars(($a = __FUNCTION__));
        $vars = [
            'sub'	=> User::ITEMS,
            'action'=> __FUNCTION__,
        ];
        $vars['user'] = $user;
        $this->vars($vars);

        return view('cp::pages.settings.users.show', $vars);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //$vars = self::vars(($a = __FUNCTION__));
        
        $vars = [
			'sub'	=> User::ITEMS,
            'action'=> __FUNCTION__,
			'items' => User::ITEMS,
			'item'	=> User::ITEM,
        ];

        $vars['user'] = $user;

        $this->vars($vars);

        return view('cp::pages.settings.users.edit', $vars);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            'name'=>'required|max:255|min:2',
            'email'=>'required|email',
        ]);

        $user->update($data);

        $msg = $user->name . ' (#' . $user->id . ') is updated successfully.';
        return redirect()->route('cp::users.show', $user->id)
                ->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();

            $msg = $user->name . ' (#' . $user->id . ') deleted successfully.';
            $status = 'success';
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $status = 'danger';
        }

        if (filter_has_var(INPUT_POST, 'js-addon')) {
            $json = ['message'=>$msg, 'status'=>$status, 'items'=>self::ITEMS];

            if (filter_input(INPUT_POST, 'js-addon') !== 'get-message' 
                && $status !== 'danger'
            ) {
                $json['redirect'] = route(self::ITEMS . '.index');
            }

            return response()->json($json);
        }

        return redirect()->route(self::ITEMS . '.index')->with($status, $msg);
    }
}
