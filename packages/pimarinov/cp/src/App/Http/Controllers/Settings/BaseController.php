<?php

namespace Pimarinov\Cp\App\Http\Controllers\Settings;

use Pimarinov\Cp\App\Http\Controllers\CpController;

/**
 * Description of BaseController
 */
class BaseController extends CpController
{
	public static $section = "settings";

	/**
     * @param array $vars
     */
	protected function vars(array &$vars)
	{
		parent::vars($vars);

		$vars['section'] = self::$section;
		$vars['sub_nav'] =  ['home', 'users', 'vars', 'account'];
	}
}
