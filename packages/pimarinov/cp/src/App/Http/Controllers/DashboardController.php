<?php

namespace Pimarinov\Cp\App\Http\Controllers;

use Illuminate\Http\Request;
use Pimarinov\Cp\App\Http\Controllers\CpController;


/**
 * Dashboard Controller
 */
class DashboardController extends CpController
{
	public function index()
	{
		return view('cp::dashboard');
	}
}
