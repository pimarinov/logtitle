<?php

namespace Pimarinov\Cp\App\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Cp base Controller class
 */
class CpController extends Controller
{
    
	/**
     * @param array $vars
     */
	protected function vars(array &$vars)
	{
		$class = static::class;

		$pieces = [$class::$section];

		if(defined($class.'::ITEMS'))
		{
			$pieces[] = $class::ITEMS;
		}

		$pieces[] = $vars['action'];

		$vars['website_name'] = env('APP_NAME', 'SomeSite');
		$vars['content'] = implode('.', $pieces);

		if(in_array(end($pieces), ['index', 'home'])) {
			array_pop($pieces);
		}

		$p = array_map(function($value){
			return ucfirst(str_replace(['_', '-'], ' ', $value));
		}, $pieces);
		$vars['head_title'] = implode(' &#10093; ', $p);
	}
}
