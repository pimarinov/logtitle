<?php

namespace Pimarinov\Cp\App\Http\Tables;

use Illuminate\Support\Facades\DB as db;
use Illuminate\Support\Facades\Request;
use Pimarinov\Table\Table as table;

/** Table helper
 * @author Plamen Marinov <plamen@webdiv.live> */
class Base {
	
	protected static $helper_class = null;

	public static function __callStatic($name, $args){
        //if($name=='icoedit'){dd($args);}
        switch ($name){
            case 'mailto':  return sprintf('<a href="mailto:%1$s" title="Send email">%1$s<i class="material-icons md-18">mail</i></a>', $args[0]);
            case 'id':      return sprintf('<a href="/cp/%1$s/%2$d" title="View">%2$d</a>', $args[0], (int)$args[1]);
            case 'show':    return sprintf('<a href="/cp/%1$s/%2$d" title="View">%3$s</a>', $args[0], (int)$args[1], $args[2]);
            case 'edit':    return sprintf('<a href="/cp/%1$s/%d" title="View">%s</a>', (int)$args[0], $args[1]);
            case 'icoshow': return sprintf('<a href="/cp/%1$s/%2$d" class="tbl-ico" title="View %3$s"><span class="material-icons">folder</span></a>', $args[0], (int)$args[1], $args[2]);
            case 'icoedit': return sprintf('<a href="/cp/%1$s/%2$d/edit" class="tbl-ico" title="Edit %3$s"><span class="material-icons">edit</span></a>', $args[0], (int)$args[1], $args[2]);
            case 'icodel':  return sprintf('<a href="javascript:cp.destroy(%2$d,\'%3$s\');" class="tbl-ico" title="Delete %3$s"><span class="material-icons">delete</span></a>', $args[0], (int)$args[1], $args[2]);
            case 'qdates':  return 
                ', DATE_FORMAT(`created_at`, "' . GLOBAL_MYSQL_DATETIME . 
                '") AS `created_at_formatted`' .
                ', DATE_FORMAT(`updated_at`, "' . GLOBAL_MYSQL_DATETIME . 
                '") AS `updated_at_formatted`' .
                ', "" AS `lnks`';
            case 'purl_slug': return sprintf('<a href="/cp/%3$s/%2$d" title="View %4$s">%1$s</a>&nbsp;<a href="%1$s" target="_blank" class="tbl-ico" title="Open %4$s in a new tab"><small class="material-icons">open_in_new</small></a>', $args[0], (int)$args[1], $args[2], $args[3]);
            default: dd(__METHOD__, $name, $args);
        }
    }

    static $rustart, $pageExtension, $preparePassed = [];

    final static function init(){
        table::$select = function(string $expression, array $bindings = []){
            return db::select($expression, $bindings);
        };

        table::$helperClass = static::class;

        table::prepare(false);

        return table::assets('/vendor/table');
    }

	/**https://stackoverflow.com/questions/535020/tracking-the-script-execution-time-in-php */
    static function time() {
        if(strtolower(pathinfo(Request::path(), PATHINFO_EXTENSION)) === "json"){ return;}
        
        
        function rutime($ru, $rus, $index, $f=1000) {
            return ($ru["ru_$index.tv_sec"]*$f + intval($ru["ru_$index.tv_usec"]/$f))
             -   ($rus["ru_$index.tv_sec"]*$f + intval($rus["ru_$index.tv_usec"]/$f));
        }

        $ru = getrusage(); $bt = debug_backtrace()[1];
        $c = $bt["class"] . $bt["type"] . $bt["function"]. "() #" . $bt["line"];
        echo "<div class=\"alert alert-primary timing-info\"><span>$c</span> ",
             "<strong>Timing</strong>: " . rutime($ru, self::$rustart, "utime"),
             "ms in computations, " . rutime($ru, self::$rustart, "stime"),
             "ms in system calls.</div>";
    }

    static function prepare($classMethod, $setOrCheck = false){
        self::$pageExtension = pathinfo(Request::path(), PATHINFO_EXTENSION)?:null;
        $continuePreparation = $setOrCheck === false;
        if($setOrCheck === true){
            if(!in_array($classMethod, self::$preparePassed) && empty(self::$pageExtension)){
                die("ERROR (in a ".__CLASS__." call): Please call the ".$classMethod."(); method before the page headers sent."); 
            }
        } else if(!empty($setOrCheck)) {
            self::$pageExtension = $setOrCheck;
        } 
        if(!in_array($classMethod, self::$preparePassed)){
            self::$preparePassed[] = $classMethod;
        }
        return $continuePreparation;
    }
}

define('ASTERISK', '<span title="Required field">*</span>');

define('GLOBAL_PHP_DATE', 'd.m.Y');
define('GLOBAL_PHP_DATE_LONG', 'd F Y');
define('GLOBAL_PHP_DATETIME', GLOBAL_PHP_DATE . ' H:i');
define('GLOBAL_PHP_DATETIME_LONG', GLOBAL_PHP_DATE_LONG . ' H:i:s');

define('GLOBAL_MYSQL_DATE', '%d.%m.%Y');
//define('GLOBAL_MYSQL_DATE_LONG', '%d %M %Y');
define('GLOBAL_MYSQL_DATETIME', GLOBAL_MYSQL_DATE . ' %H:%i');
//define('GLOBAL_MYSQL_DATETIME_LONG', GLOBAL_MYSQL_DATE_LONG . ' %H:%i:%s');

define('TXT_SAVE_OK', 'The data has been saved successfully.');
define('TXT_SAVE_ERROR', 'The data could not be saved. Please try again later.');
define('TXT_DELETE_CONFIRM', 'Do you really want to delete this row and connected data?');
define('TXT_DELETE_OK', 'The selected items were successfully deleted.');
define('TXT_DELETE_ERROR', 'The selected items could not be deleted. Please try again later.');
