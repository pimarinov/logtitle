<?php

namespace Pimarinov\Cp\App\Http\Tables;

use Pimarinov\Table\Table as table;

class Vars extends Base
{
    final static function vars($undeletableKeys){
        $items = __FUNCTION__;
        $item = 'var';
        
        if(filter_has_var(INPUT_GET, "time")){ self::$rustart = getrusage(); }
        
        table::$attributes["table"]["class"] = "table-striped table-sm table-hover table-bordered";
        table::create($items, "id", "desc", 50);
        table::$cols[] = Array("ID", "id", array("width"=>"50px"));
        table::$cols[] = Array("Group", "group", array("width"=>"150px"));
        table::$cols[] = Array("Name", "name", array("width"=>"200px"));
        table::$cols[] = Array("Value", "value");
        table::$cols[] = Array("Comment", "comment");
        table::$cols[] = Array("Created", "created_at_formatted", array("sort"=>"created_at","width"=>"130px"));
        table::$cols[] = Array("Updated", "updated_at_formatted", array("sort"=>"updated_at","width"=>"130px"));
        table::$cols[] = ["...", "links", ["width"=>"78px", "class"=>"links","sort"=>false]];
        table::$cols[] = ["...", "link", ["width"=>"40px", "class"=>"link","sort"=>false]];
        
        $dF = ", DATE_FORMAT(`created_at`, \"".GLOBAL_MYSQL_DATETIME."\") AS `created_at_formatted`" .
              ", DATE_FORMAT(`updated_at`, \"".GLOBAL_MYSQL_DATETIME."\") AS `updated_at_formatted`" .
              ", \"\" AS `lnks`";
        table::execute("SELECT *" . $dF . " FROM `{$items}`");
        
        foreach(table::$data as $r => &$cells){
            $cells = get_object_vars($cells); //object -> array
            $id = (int)$cells['id'];
            $cells["link"] = self::icoshow($items, $id, $item);
            $links = in_array($id, $undeletableKeys) ? 
                    [self::icoedit($items, $id, $item)] :
                    [self::icodel($items, $id, $item), self::icoedit($items, $id, $item)];
            $cells["links"] = implode('', $links);
            if(table::$export === false){
                $cells["id"] = self::id($items, $id);
                $cells['group'] = ucfirst(str_replace('-',' ', $cells['group']));
                $cells['name'] = '<kbd>' . $cells['name'] . '</kbd>';
                $cells['value'] = '<kbd>' . $cells['value'] . '</kbd>';
            }
        }
        table::load();

        if(isset(self::$rustart)){ self::time(); }
    }
}
