<?php

namespace Pimarinov\Cp\App\Http\Tables;

use Illuminate\Support\Facades\Cache;
use Pimarinov\Table\Table as table;

class DummyUsers extends Base {
	
	const TOTAL_COUNT = 1320065; //real value is 1320066
	const TOTAL_COUNT_CACHE_KEY = 'DummyUsers::TOTAL_COUNT';

    final static function users($undeletableKeys) {
        $items = 'dummy-users';
        $item = 'dummy-user';

        if(filter_has_var(INPUT_GET, "time")){ self::$rustart = getrusage(); }

		$totalCount = Cache::get(self::TOTAL_COUNT_CACHE_KEY, self::TOTAL_COUNT);
		table::config(['TOTAL_COUNT_CACHE' => $totalCount]);

        table::create("dummy_users", "id", "desc", 50);
        table::$cols[] = ["ID", "id", ["width"=>"50px"]];
        table::$cols[] = ["Name", "name"];
        table::$cols[] = ["Email", "email"];
        table::$cols[] = ["test","test",["sort"=>false]];
        table::$cols[] = ["Created", "created_at_formatted", 
                            ["sort"=>"created_at","width"=>"130px"]];
        table::$cols[] = ["Updated", "updated_at_formatted", 
                            ["sort"=>"updated_at","width"=>"130px"]];
        table::$cols[] = ["...", "links", ["width"=>"78px", "class"=>"links","sort"=>false]];
        table::$cols[] = ["...", "link", ["width"=>"40px", "class"=>"link","sort"=>false]];

        table::execute("SELECT *" . self::qdates() . " FROM dummy_users ");

        foreach(table::$data as &$cells){
            $cells = get_object_vars($cells); //object -> array
            $id = (int)$cells['id'];
            $cells["link"] = self::icoshow($items, $id, $item);
            $links = in_array($id, $undeletableKeys) ? 
                    [self::icoedit($items, $id, $item)] :
                    [self::icodel($items, $id, $item, route('cp::dummy-users.destroy', $id)), self::icoedit($items, $id, $item)];
            $cells["links"] = implode('', $links);
            if(table::$export === false){
                $cells['name'] = self::show($items, $id, $cells['name']);
                $cells['id'] = self::id($items, $id);
                $cells['email'] = self::mailto($cells['email']);
            }
        }

        table::$attributes["table"]["class"] = "table-striped table-sm table-hover table-bordered";

        table::load();

		$totalCountReloaded = table::config('TOTAL_COUNT_CACHE');

		if($totalCountReloaded !== $totalCount) {
			Cache::set(self::TOTAL_COUNT_CACHE_KEY, $totalCountReloaded);
		}

        if(isset(self::$rustart)){ self::time(); }
    }

	final static function count()
	{
		return Cache::get(self::TOTAL_COUNT_CACHE_KEY, self::TOTAL_COUNT);
	}
}
