<?php

namespace Pimarinov\Cp\App\Models;

use App\Models\User as BaseUser;

class User extends BaseUser
{
    const ITEMS = 'users';
    const ITEM = 'user';

	protected $appends = ['gravatar'];

    /**
	 * Sets $user->gravatar
	 * @see https://stackoverflow.com/a/23724888/819118
	 *
	 * @return string
	 */
	public function getGravatarAttribute()
	{
		$hash = md5(strtolower(trim($this->attributes['email'])));
		return "https://www.gravatar.com/avatar/$hash?s=200&d=mp";
		//-> &d=robohash
	}
}
