<?php

namespace Pimarinov\Cp\App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    const ITEMS = 'vars';
    const ITEM = 'var';	
}
