<?php

namespace Pimarinov\Cp;

use Illuminate\Support\ServiceProvider;
//use App\Http\Controllers\Settings\VariablesController;

class CpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		$this->loadRoutesFrom(__DIR__.'/../routes/cp.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'cp');
		$this->publishes([
			__DIR__.'/../resources/css' => public_path('pimarinov/cp/css'),
			__DIR__.'/../resources/js' => public_path('pimarinov/cp/js'),
		], 'cp');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->app->make('Pimarinov\Cp\App\Http\Controllers\CpController');
    }
}
