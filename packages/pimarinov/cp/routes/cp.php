<?php

use Illuminate\Support\Facades\Route;


/** CP Routes  */
Route::group([
	'prefix' => config('cp.prefix', 'cp'),
	'middleware'=>['web', 'auth:sanctum', 'verified']
	], function (){

	Route::redirect('', '/cp/home', 301);

	Route::get(
		'/home'
		, 'Pimarinov\Cp\App\Http\Controllers\HomeController@index'
	)->name('cp::home');

	Route::get(
		'/website'
		, 'Pimarinov\Cp\App\Http\Controllers\Website\HomeController@index'
		)->name('cp::website.home');

	Route::resource('/dummy-users'
		, 'Pimarinov\Cp\App\Http\Controllers\Website\DummyUserController'
		, ['names'=>'cp::dummy-users']
		);
    Route::get('/dummy-users.json'
		, 'Pimarinov\Cp\App\Http\Controllers\Website\DummyUserController@index'
		);

	Route::resource('/users'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\UserController'
		, ['names'=>'cp::users']
		);
    Route::get('/users.json'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\UserController@index'
		);

	Route::get(
		'/settings'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\HomeController@index'
		)->name('cp::settings.home');

	Route::resource(
		'/vars'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\VariablesController'
		, ['names'=>'cp::vars']
		);

	Route::get(
		'/account'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\AccountController@edit'
		)->name('cp::settings.account');
	Route::post(
		'/account'
		, 'Pimarinov\Cp\App\Http\Controllers\Settings\AccountController@store'
		)->name('cp::settings.account-store');

    Route::get(
		'/logout'
		, 'Pimarinov\Cp\App\Http\Controllers\HomeController@logout'
		)->name('cp::logout');

	Route::get(
		'/dashboard'
		, 'Pimarinov\Cp\App\Http\Controllers\DashboardController@index'
	)->name('cp.dashboard');

});
