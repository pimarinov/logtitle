<?php

namespace Database\Seeders\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class InitSeeder extends Seeder
{
    const USERS = [
        [
            'id'=>1,
            'name'=>'Plamen Marinov',
            'email'=>'plamen@webdiv.live',
            'password'=>123456,
        ],
        [
            'id'=>100,
            'name'=>'Jane Dowe',
            'email'=>'janedowe@logtitle.com',
            'password'=>123456,
        ],
        [
            'id'=>101,
            'name'=>'John Dowe',
            'email'=>'johndowe@logtitle.com',
            'password'=>123456,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->seedUsers();
    }

    private function seedUsers()
    {
        User::truncate();

        foreach (self::USERS as $user) {

            $password = env('PRIVATE_USER_PASS', $user['password']);

            $user['password'] = Hash::make($password);

            User::firstOrCreate($user)
                ->save();
        }
    }
}
