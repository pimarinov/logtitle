<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\User\InitSeeder as UserInitSeeder;
use Database\Seeders\Language\InitSeeder as LanguageInitSeeder;

class AppInitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserInitSeeder::class,
            LanguageInitSeeder::class,
        ]);
    }
}
