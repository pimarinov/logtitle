<?php

namespace Database\Factories;

use App\Models\Title;
use App\Models\User;
use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\Factory;

class TitleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Title::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'language_id' => Language::all()->random()->id,
            'heading' => $this->faker->title(),
            'summary' => $this->faker->paragraph(1),
            'description' => $this->faker->paragraph(3),
            'set_reading' => $this->faker->date,
            //'end_reading' => 
        ];
    }
}
