<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Language;
use App\Models\Readable\ReadableSerie;


class ReadableTitleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = User::all()->random()->id;
        return [
            'user_id' => User::all()->random()->id,
            'language_id' => Language::all()->random()->id,
            'readable_serie_id' => ReadableSerie::where('user_id', $userId)->random()->id,
            'name' => $this->faker->title(),
        ];
    }
}
