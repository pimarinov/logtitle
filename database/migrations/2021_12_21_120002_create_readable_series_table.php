<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadableSeriesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('readable_series', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('title');

            $table->timestamps();
            $table->softDeletes();

            $table->index('user_id');
            $table->index('title');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('readable_series');
    }
}
