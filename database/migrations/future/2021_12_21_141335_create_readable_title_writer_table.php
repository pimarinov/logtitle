<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadableTitleWriterTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('readable_title_writer', function (Blueprint $table) {
            $table->id();

            $table->foreign('readable_title_id');
            $table->foreign('writer_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('readable_title_writer');
    }
}
