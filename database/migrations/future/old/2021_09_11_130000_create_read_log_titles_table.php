<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadLogTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('read_log_titles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('language_id');
			$table->string('heading');
			$table->text('summary')->nullable();
			$table->text('description')->nullable();
            $table->foreignId('serie_id')->nullable();
			$table->timestamp('set_reading')->nullable();
			$table->timestamp('end_reading')->nullable();
            $table->foreignId('lib_id')->nullable();
            $table->timestamps();
            
            $table->unique(['user_id', 'language_id', 'heading'], 'unique_user_language_heading');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('read_log_titles');
    }
}
