<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadLibTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('read_lib_titles', function (Blueprint $table) {
            $table->id();
			$table->foreignId('serie_id')->nullable();
			$table->string('heading');
			$table->text('summary')->nullable();
			$table->text('description')->nullable();
            $table->foreignId('source_log_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('read_lib_titles');
    }
}
