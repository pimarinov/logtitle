<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadLogLogWritersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('read_log_writers', function (Blueprint $table) {
            $table->id();
			$table->foreignId('user_id');
			$table->string('name');
			$table->text('summary')->nullable();
			$table->text('description')->nullable();
            $table->foreignId('lib_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('read_log_writers');
    }
}
