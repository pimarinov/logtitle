<?php

use App\Http\Controllers\Logs\Read\Home\HomeController as ReadHomeController;
use App\Http\Controllers\Logs\Read\Log\HomeController as ReadLogHomeController;
use App\Http\Controllers\Logs\Read\Log\SerieController as ReadLogSerieController;
use App\Http\Controllers\Logs\Read\Log\TitleController as ReadLogTitleController;
use App\Http\Controllers\Logs\Read\Log\WriterController as ReadLogWriterController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'read',
    'middleware' => ['web', 'auth:sanctum', 'verified'],
    ], function () {
        Route::get(
            '/home',
            [ReadHomeController::class, 'index']
        )->name('read.home');

        Route::group([
            'prefix' => 'log',
            'as' => 'read.log.',
        ], function () {
            Route::get('home', [ReadLogHomeController::class, 'index'])
                ->name('home');
            Route::resources([
                'titles' => ReadLogTitleController::class,
                'writers' => ReadLogWriterController::class,
                'series' => ReadLogSerieController::class,
            ]);
            Route::get('titles.json', [ReadLogTitleController::class, 'index'])
                ->name('titles.json');
            Route::get('writers.json', [ReadLogWriterController::class, 'index'])
                ->name('writers.json');
            Route::get('series.json', [ReadLogSerieController::class, 'index'])
                ->name('series.json');
        });
        Route::group([
            'prefix' => config('read.prefix', 'lib'),
            'as' => 'read.lib.',
            'namespace' => 'App\Http\Controllers\Logs\Read\Lib',
        ], function () {
            Route::get('home', 'HomeController@index')->name('home');
            Route::resources([
                'titles' => TitleController::class,
                'writers' => WriterController::class,
                'series' => SerieController::class,
            ]);
        });
        Route::group([
            'prefix' => config('read.prefix', 'settings'),
            'namespace' => 'App\Http\Controllers\Logs\Read\Settings',
        ], function () {
            Route::get('home', 'HomeController@index')->name('read.settings.home');
            Route::get('account', 'AccountController@index')->name('read.settings.account');
        });

        /*Route::group([
            'prefix' => config('play.prefix', 'play'),
            'namespace' => 'App\Http\Controllers\Logs\Play',
        ], function() {

            Route::get(
                'home'
                , 'HomeController@index'
                )->name('play.home');
        });*/

        /*Route::group([
            'prefix' => config('library.prefix', 'all'),
            'namespace' => 'App\Http\Controllers\Library\Main',
        ], function() {

            Route::get(
                'summary'
                , 'SummaryController@index'
                )->name('library::all.summary');
        });

        Route::group([
            'prefix' => config('library.prefix', 'settings'),
            'namespace' => 'App\Http\Controllers\Library\Settings',
        ], function() {

            Route::get(
                'summary'
                , 'SummaryController@index'
                )->name('library::settings.summary');
            Route::get(
                'account'
                , 'AccountController@index'
                )->name('library::settings.account');
        });
        */
        //Route::redirect('', '/my-log/choose', 301);

        Route::get(
            '/dashboard',
            'Pimarinov\Cp\App\Http\Controllers\DashboardController@index'
        )->name('cp.dashboard');
    });
