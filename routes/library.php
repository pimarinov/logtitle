<?php

use Illuminate\Support\Facades\Route;


/** CP Routes  */
Route::group([
	'prefix' => config('library.prefix', 'library'),
	'middleware'=>['web', 'auth:sanctum', 'verified']
	], function (){

	Route::redirect('', '/library/summary', 301);

	Route::get(
		'/home'
		, 'App\Http\Controllers\Library\HomeController@index'
	)->name('library::home');

    Route::group([
        'prefix' => config('library.prefix', 'read'),
        'namespace' => 'App\Http\Controllers\Library\Read',
    ], function() {
        
        Route::get(
            'summary'
            , 'SummaryController@index'
            )->name('library::read.summary');
        
        Route::resource(
		'/titles'
		, 'TitleController'
		, ['names'=>'library::read.titles']
		);
    });

    Route::group([
        'prefix' => config('library.prefix', 'all'),
        'namespace' => 'App\Http\Controllers\Library\Main',
    ], function() {
        
        Route::get(
            'summary'
            , 'SummaryController@index'
            )->name('library::all.summary');
    });

    Route::group([
        'prefix' => config('library.prefix', 'settings'),
        'namespace' => 'App\Http\Controllers\Library\Settings',
    ], function() {
        
        Route::get(
            'summary'
            , 'SummaryController@index'
            )->name('library::settings.summary');
        Route::get(
            'account'
            , 'AccountController@index'
            )->name('library::settings.account');
    });
    

	

    Route::get(
		'/logout'
		, 'Pimarinov\Cp\App\Http\Controllers\HomeController@logout'
		)->name('library::logout');

	Route::get(
		'/dashboard'
		, 'Pimarinov\Cp\App\Http\Controllers\DashboardController@index'
	)->name('cp.dashboard');

});
