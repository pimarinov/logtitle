<?php
use Illuminate\Support\Facades\Route;


/** CP Routes  */

/*Route::get('cp/login', 'App\Http\Controllers\Cp\LoginController@showLoginForm')->name('cp.login')->middleware('web');
Route::post('cp/login', 'App\Http\Controllers\Cp\LoginController@login')->name('cp.login.submit')->middleware('web');
Route::get('cp/logout', 'App\Http\Controllers\Cp\LoginController@logout')->name('cp.logout')->middleware('web');*/

Route::group([
	'prefix' => config('cp.prefix', 'cp'),
    'namespace' => 'App\Http\Controllers\Cp'
	], function (){


	Route::get('/home', 'HomeController@index')->name('cp.home')->middleware('auth:admin');

    /*Route::group([
        'prefix' => config('cp.prefix', 'read'),
        'middleware'=>['auth:cp_user'],
        'namespace' => '\App\Http\Controllers\Cp',
    ], function() {
        
        Route::get(
            'home'
            , 'HomeController@index'
            )->name('cp.read.home');
        
    });*/

    Route::namespace('Auth')/*->middleware('guest')*/->group(function(){

        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('cp.login')->middleware('guest:admin', 'web');
        Route::post('/login','LoginController@login')->name('cp.login.submit');
        Route::get('/logout','LoginController@logout')->name('cp.logout');

        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');

    });
});
