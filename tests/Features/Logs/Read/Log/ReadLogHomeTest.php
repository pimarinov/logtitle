<?php

namespace Tests\Features\Logs\Read\Log;

use App\Models\User;
use Tests\TestCase;

/**
 * @internal
 * @covers App\Features\Logs\Read\Lib\HomeFeature
 */
class ReadLogHomeTest extends TestCase
{
    /**
     * An authorized user logs home access.
     */
    public function testReadLogHomeAuthorisedAccess()
    {
        $this->actingAs($user = User::factory()->create());
        $this->assertAuthenticated();

        $response = $this->get('/read/log/home');

        $response->assertStatus(200);
    }
}
