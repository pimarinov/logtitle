<?php

namespace Tests\Features\Logs\Read\Home;

use App\Models\User;
use Tests\TestCase;

/**
 * @internal
 * @covers App\Features\Logs\Read\Home\HomeFeature
 */
class ReadHomeTest extends TestCase
{
    /**
     * An unauthorized user home redirect.
     */
    public function testReadHomeUnatuthorisedRedirect()
    {
        $response = $this->get('/read/home');

        $response->assertRedirect('login');
    }

    /**
     * An authorized user home access.
     */
    public function testReadHomeAuthorisedAccess()
    {
        $this->actingAs($user = User::factory()->create());
        $this->assertAuthenticated();

        $response = $this->get('/read/home');

        $response->assertStatus(200);
    }
}
