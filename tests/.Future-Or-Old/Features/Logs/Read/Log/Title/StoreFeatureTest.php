<?php

namespace Tests\Features\Logs\Read\Log\Title;

use Tests\TestCase;
use App\Models\Read\Log\ReadLogTitle;
use App\Models\User;
use App\Features\Logs\Read\Log\Title\StoreFeature;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function test_success_store_feature()
    {
        $this->actingAs($user = User::factory()->create());
        $this->assertAuthenticated();
        
        $response = $this->get('/read/home');
        $response->assertStatus(200);
        $this->markTestIncomplete();


        //@todo -> change columns below
        $title = [
            'heading' => 'test 123',
            'language_id' => 27,
            'description' => 'descr',
        ];

        $response = $this->post('/read/log/titles', $title);
        $response->assertStatus(302);

        $stored = ReadLogTitle::orderBy('id', 'desc')->first();
die(var_dump($stored));
die();
        $response->assertRedirect(route('read.log.titles.show', ['id' => $stored->id]));

        $this->assertEquals($title, $stored->toArray());
    }
}
