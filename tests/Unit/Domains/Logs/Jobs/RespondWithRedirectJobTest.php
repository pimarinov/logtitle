<?php

namespace Tests\Unit\Domains\Logs\Jobs;

use Tests\TestCase;
use App\Domains\Logs\Jobs\RespondWithRedirectJob;
use App\Domains\Logs\Read\Home\Home\Jobs\NavReadHomeJob;
use App\Http\Controllers\Logs\Read\Home\HomeController;
use Illuminate\Http\RedirectResponse;

class RespondWithRedirectJobTest extends TestCase
{
    public function test_rspond_with_view_job()
    {
        $status = 'Hi There!';
        $job = new RespondWithRedirectJob('read.home', [], $status);
        
        $resp = $job->handle();
        
        $this->assertEquals('Illuminate\Http\RedirectResponse', get_class($resp));
        
        $this->assertEquals($status, $resp->getSession()->get('status'));
    }
}
