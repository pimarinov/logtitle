<?php

namespace Tests\Unit\Domains\Logs\Jobs;

use App\Domains\Logs\Jobs\RespondWithViewJob;
use stdClass;
use Tests\TestCase;

/**
 * @internal
 * @coversNothing
 */
class RespondWithViewJobTest extends TestCase
{
    public function testRspondWithViewJob()
    {
        $job = new RespondWithViewJob('logs.read.home.home', new stdClass(), []);

        $resp = $job->handle();

        $this->assertEquals(get_class($resp), 'Illuminate\View\View');
    }
}
