var table_singleton_methods = {
    ColumnHover: function(tableContainer, index){
        var rows = document.getElementById(tableContainer).rows;
        var upto = rows.length - 1;
        if(typeof index === "undefined"){
            HoverRelease(rows, upto);
        }else{
            for(var i = 0; i < upto; i++){
                rows[i].cells[index].setAttribute("lang", "col-hover");
            }
        }
        function HoverRelease(rows, upto){
            for(var i = 0; i < upto; i++){
                for(var j = 0; j < rows[i].cells.length; j++){
                    if(rows[i].cells[j].lang){
                        rows[i].cells[j].removeAttribute("lang");
                    }
                }
            }
        };
    },
    Draw: {
        Section: function(tableContainer, dt, tSection){
            var section = tSection === "tfoot" ? "tfoot" : "tbody";
            var tSec = document.getElementById(tableContainer)
                        .getElementsByTagName(section)[0];
            tSec.innerHTML="";
            for(var i = 0; i < dt.length; i++){
                tSec.appendChild( Row(dt[i]) );
            }
            if(section === "tfoot"){
                this.ProcessPaginationLinks(tSec);
            }
            function Row(row){
                var tRow = document.createElement("tr");
                for(var i = 0; i < row.length; i++){
                    tRow.appendChild(Cell(row[i]));
                }
                return tRow;
            }
            function Cell(cell){
                var tCell = document.createElement("td");
                if(typeof cell === "string" ||
                    typeof cell === "number"
                ){
                    tCell.innerHTML = cell;
                }else if(typeof cell === "object"){
                    tCell.innerHTML = cell[0];
                    for(var attr in cell[1]){
                        tCell.setAttribute(attr, cell[1][attr]);
                    }
                }
                return tCell;
            }
        },
        Run: function(tableContainer, d, instance){
            this.Section.call(instance, tableContainer, d.body);
            this.Section.call(instance, tableContainer, d.footer, "tfoot");
            if(instance.rq !== null){
                var hover = document.getElementById(instance.rq.tableId)
                        .getElementsByTagName("th")[instance.rq.colNo].lang;
                if(hover){
                    instance.ColumnHover(tableContainer, instance.rq.colNo);
                }
            }
        }
    },
    Export: function(lnk, eType, hasTotalCache = false){
        var tableId = this.GetParent(lnk, "table").getAttribute("id");
        var rq = this.Request(tableId);
        if(hasTotalCache){
            if(!confirm("This export action can take a while.")){ return false;
            } else { rq.totalCacheActive = 1; }
        }
        rq.exportType = ["CSV", "Excel"].indexOf(eType) >= 0 ? eType : "CSV";
        var lnkUrl = this.Url(rq);
        lnk.setAttribute("href", lnkUrl);
        return this.ExportIt(rq.tableId, ("totalCacheActive" in rq));
    },
    ExportIt: function(tableId, large){
        window.exportTime = window.performance.now();
        console.log("%c Requesting the file" + (large ? '(large)' : '') + "... "
            , "background: #222; color: #3399ff");
        this.Busy(tableId, false);

        window.exportDoneFunc = function () {
            if(!window.exportTime || false) { return; }
            window.removeEventListener("focus", window.exportDoneFunc);
            var secs = (window.performance.now() - window.exportTime)/1000;
            delete window.exportTime, window.exportDoneFunc;
            console.log("%c File saved. (" + Math.round(secs) + " seconds) "
                , "background: #222; color: #00cc00");
            window.table.Busy(tableId, true);
        };
        window.addEventListener("focus", window.exportDoneFunc);
        return true;
    },
    Filter: {
        Run: function(field){
            var tableId = this.Filter.GetTableId(field);
            if(tableId !== null){
                this.rq = this.Request(tableId);
                this.Load(this);
            }
        },
        GetTableId: function(field){
            var f = field.tagName.toLowerCase() !== "select" ?
                    field :
                    field.parentNode.parentNode.getElementsByTagName("input")[0];
            if(f.hasAttribute("data-reset")){
                f.removeAttribute("data-reset");
                return f.getAttribute("data-table-id");
            }
            return '' === f.value ? null : f.getAttribute("data-table-id");
        }
    },
    GoPage: {
        Run: function(lnk){
            var tableId = this.GetParent(lnk, "table").getAttribute("id");
            this.rq = this.Request(tableId);
            this.rq.pageNo = this.GoPage.GetNo(lnk, this.rq.tableId);
            this.Load(this);
            return false;
        },
        GetNo: function(lnk, tableId){
            //check & serve pagination jump links
            var jumpDir = lnk.innerHTML.trim().substr(0, 1);
            if(jumpDir === "+" || jumpDir === "-"){
                var current = document.getElementById(tableId)
                        .querySelector("tfoot .paging .a").innerHTML;
                var jump = lnk.innerHTML.replace("K", "000")
                        .replace("M", "000000000");
                var jumpPage = (parseInt(current) + parseInt(jump));
                lnk.parentNode.setAttribute("data-page", jumpPage);
                lnk.style.transform = "none";
            }
            return lnk.parentNode.hasAttribute("data-page")
                ? lnk.parentNode.getAttribute("data-page")
                : lnk.innerHTML;
        }
    },
    Init: function(tableId){
        var tContainer = document.getElementById(tableId);
        if(!this.IePrior(9)){
            SetColumnsHover(tContainer, tableId);
        }
        var tfoot = tContainer.getElementsByTagName("tfoot")[0];
        this.ProcessPaginationLinks(tfoot);

        function SetColumnsHover(tContainer, tableId){
            var tHcells = tContainer.rows[0].cells;
            for(var i = 0; i < tHcells.length; i++){
                if(tHcells[i].firstChild.tagName === "A"){
                    tHcells[i].firstChild.setAttribute("onmouseover",
                            "table.ColumnHover('" + tableId + "'," + i + ");");
                    tHcells[i].firstChild.setAttribute("onmouseout",
                            "table.ColumnHover('" + tableId + "');");
                }
            }
        }
    },
    LoadTail: null,
    Load: function(instance){
        if(this.LoadTail !== null){
            this.LoadTail.abort();
        }
        var inst = instance;
        inst.Busy(inst.rq.tableId, false);
        var xmlhttp = window.XMLHttpRequest
            ? new XMLHttpRequest() //IE7+, Firefox, Chrome, Opera, Safari
            : new window.ActiveXObject("Microsoft.XMLHTTP"); //IE6, IE5
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState === 4 && xmlhttp.status === 200){
                var d = JSON.parse(xmlhttp.responseText);
                inst.Draw.Run(inst.rq.tableId, d, inst);
                inst.Busy(inst.rq.tableId, true);
                window.table.LoadEndCalback(inst.rq.tableId);
            } else if(xmlhttp.readyState === 4 && xmlhttp.status > 0) {
                inst.Busy(inst.rq.tableId, true);
                var msg = "\u26A0 The server was unable to handle: " + xmlhttp.statusText;
                alert(msg + (
                    504 === xmlhttp.status && inst.rq.filter && !inst.rq.filterBy
                    ? "\r\nTry narrow the filter by." : ""));
                console.log("%c table.rq:", "background: #222; color: #3399ff"
                    , inst.rq, xmlhttp);
            }
        };
        xmlhttp.open("GET", inst.Url(inst.rq), true);
        xmlhttp.send();
        this.LoadTail = xmlhttp; //put at tail to can abort later previous request
    },
    ReloadData: function(tableId){
        this.rq = this.Request(tableId);
        this.Load(this);
    },
    ReloadTotalCache: function(tableId){
        if(confirm("This refresh action can take a while.\r\nAre you sure?")){
            this.rq = this.Request(tableId);
            this.rq.reloadTotalCache=1;
            this.Load(this);
        }
    },
    Sort: function(colNo, lnk){
        var tableId = this.GetParent(lnk, "table").getAttribute("id");
        this.rq = this.Request(tableId);
        if(Math.round(colNo) === this.rq.colNo){
            this.rq.colOrd = (this.rq.colOrd === "asc" ? "desc" : "asc");
        }else{
            this.rq.colNo = Math.round(colNo);
            this.rq.colOrd = "asc";
        }
        this.Load(this);
        ClearAndSetNewSortArrow(this.rq);

        function ClearAndSetNewSortArrow(rq){
            var table = document.getElementById(rq.tableId);
            var headSpans = table.getElementsByTagName("thead")[0]
                    .getElementsByTagName("span");
            var length = headSpans.length;
            for(var i = 0; i < length; i++){
                headSpans[i].innerHTML = "";
            }
            lnk.getElementsByTagName("span")[0].innerHTML =
                    rq.colOrd === 'desc' ?
                    table.getAttribute('data-sort-d') :
                    table.getAttribute('data-sort-a');
        }
    },
    Busy: function(tableId, set){
        var tbl = document.getElementById(tableId);
        if(set === true){
            tbl.style.filter = "none";
            tbl.style.opacity = "1";
            tbl.style.cursor = "auto";
        }else if(set === false){
            tbl.style.filter = "blur(1px)";
            tbl.style.opacity = "0.8";
            tbl.style.cursor = "wait";
        }else{ console.error("table error in the flag value"); }
    },
    ProcessPaginationLinks: function(tfoot){
        var pLinks = tfoot.querySelectorAll(".paging a");
        if(pLinks.length > 0){
            for(var j = 0; j < pLinks.length; j++){
                pLinks[j].setAttribute("onclick", "return table.GoPage(this);");
                pLinks[j].setAttribute("href", "javascript:void(0);");
            }
        }
    },
    Request: function(tableId){
        var rq = {tableId: tableId};

        var table = document.getElementById(rq.tableId);
        //processes sort
        var spanOrd = table.querySelector("thead th a span:not(:empty)");
        if(spanOrd !== null) {
            var dSymbol = table.getAttribute('data-sort-d');
            rq.colNo = spanOrd.parentNode.parentNode.cellIndex;
            rq.colOrd = spanOrd.innerHTML === dSymbol ? "desc" : "asc";
        }
        //processes filter
        var filterDiv = table.parentNode.querySelector(".filter");
        if(filterDiv !== null) {
            var input = filterDiv.getElementsByTagName("input")[0];
            if(input && input.value && input.value.length !== 0){
                rq.filter = input.value;
            }
            var select = filterDiv.getElementsByTagName("select")[0];
            if(select && select.options[select.selectedIndex].value !== "all"){
                rq.filterBy = select.options[select.selectedIndex].value;
            }
        }
        return rq;
    },
    Url: function(rq){
        var url = location.pathname + ".json" + location.search;
        var getUrlVarName = { colNo: "col", colOrd: "ord", filter: "filter"
            , filterBy: "filter-by", pageNo: "pg", exportType: "export"
            , reloadTotalCache: "reload-total-cache"
            , totalCacheActive: "total-cache-active", tableId: "table-id"
        };
        var flagFirst = location.search.length < 1 ? true : false;
        for(var r in rq){
            var clue = flagFirst === true ? "?" : "&";
            url += clue + getUrlVarName[r] + "=" + rq[r];
            flagFirst = false;
        }
        return url;
    },
    IePrior: function(v){
        var rv = false;
        if(window.navigator.appName === 'Microsoft Internet Explorer'){
            var ua = window.navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if(re.exec(ua) !== null){
                rv = parseFloat(RegExp.$1);
            }
            rv = rv < v ? true : false;
        }
        return rv;
    },
    GetParent: function(obj, objType){
        while(obj && obj.tagName !== objType.toUpperCase()){
            obj = obj.parentNode;
        }
        return obj;
    }
};

//https://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
var table_singleton = (function(){
    var instance; //stores a reference to the Singleton
    function initInstance(){

        var static = table_singleton_methods;

        return {
            init: static.Init.bind(static),
            ColumnHover: static.ColumnHover,
            Export: static.Export.bind(static),
            Filter: static.Filter.Run.bind(static),
            GoPage: static.GoPage.Run.bind(static),
            LoadEndCalback: function(){}, /*Allows override: function(tableId){if(tableId){...}}*/
            ReloadData: static.ReloadData.bind(static),
            ReloadTotalCache: static.ReloadTotalCache.bind(static),
            Sort: static.Sort.bind(static),
            Busy: static.Busy.bind(static)
        };
    }
    return {
        //Get the Singleton instance if one exists, or create one if it doesn't
        getInstance: function(){
            if(!instance){
                instance = initInstance();
            }
            return instance;
        }
    };
})();
var table = table_singleton.getInstance();
