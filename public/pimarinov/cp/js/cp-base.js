function setPageTheme()
{
    var cp_var = localStorage.getItem("cp");

    var cp = cp_var  ? JSON.parse(cp_var) : null;

    function systemTheme()
    {
        let userPrefersDark = window.matchMedia 
            && window.matchMedia('(prefers-color-scheme: dark)').matches;

        return userPrefersDark ? "dark" : "light";
    }

    var cssClass = cp ? cp.theme : systemTheme();

    if("light" !== cssClass) {
        document.body.classList.add(cssClass);
    }
}
/**
 * System's theme change detection
 *  
 * @see https://web.dev/prefers-color-scheme/
 */
window.matchMedia('(prefers-color-scheme: dark)').addListener((e) => {
    var darkModeOn = e.matches;
    
    var current_value = localStorage.getItem("cp");

    var current = current_value ? JSON.parse(current_value).theme : "system";
    
    if("system" === current)
    {
        setPageTheme();
    }
    
    console.log(`Dark mode is ${darkModeOn ? 'on' : 'off'}.`);
});

// https://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
var CpSingleton = (function(){

    // Instance stores a reference to the Singleton
    var instance;
    
    function init(){
        
        // Singleton
        // Private methods and variables
        function destroy(object_id, object_name, route){
            var item = object_name !== null ? object_name.toLowerCase() : 'item';
            var r = confirm('Do you really want to delete this ' + item + ' and connected data?');
            if (r === true) {
                if (window.XMLHttpRequest) {var xhttp = new XMLHttpRequest();//modern browsers
                } else {xhttp = new ActiveXObject("Microsoft.XMLHTTP"); }//for IE6, IE5

                xhttp.onreadystatechange = function() {
                    if (this.readyState === 4 && this.status === 200) {
                        //console.log(this.responseText);
                        var resp = JSON.parse(this.responseText);
                        if(typeof resp.redirect !== 'undefined'){
                            resp.location = location;
                            console.log(resp);
                            sessionStorage.setItem("redirected", JSON.stringify(resp));
                            location.replace(resp.redirect);
                        } else if (typeof resp.message !== 'undefined') {
                            message(resp.status, resp.message, resp.items);
                        }
                    }
                };

                if ((csfr = document.querySelector('input[name="_token"]'))=== null) {
                    console.error('missing security token.');
                    return;
                }
                var token = csfr.value;

                if (route === null) {
                    var path = [];
                    var path_pieces = window.location.pathname.split('/');
                    for (var i = 0; i < path_pieces.length; i++) {
                        if (path_pieces[i] !== 'edit' && isNaN(path_pieces[i])) {
                            path.push(path_pieces[i]);
                        }
                    }
                    path.push(object_id);
                    var route = window.location.protocol + '//' 
                                + window.location.hostname + '/'
                                + path.join('/');

                }

                var jsAddon = 'set-message-and-get-redirect';
                if ((addLink = document.querySelector('#page-content .btn-add'))!== null
                    && addLink.hasAttribute('title')
                    && addLink.getAttribute('title').toLowerCase()===('add ' + item) 
                ) {
                    jsAddon = 'get-message';
                }

                xhttp.open("POST", route, true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("_method=DELETE&_token="+token+"&js-addon=" + jsAddon + "&id=" + object_id);
            }
        }

        function message(status, message, items){
            var msgs = document.getElementById('page-alerts');
            if (msgs === null) {
                console.error('Element: #page-messages is missing on the page');
                //return;
                var pre = (status === 'danger' ? 'error' : status);
                return alert(pre.toUpperCase() + ": " + message);
            }
            msgs.innerHTML +=   '<div class="alert alert-' + status + ' col">' + message +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = \'none\';},400); this.parentNode.setAttribute(\'class\',(this.parentNode.getAttribute(\'class\')+\' msg-fadeout\'));"><button type="button" class="close material-icons" data-dismiss="alert" aria-label="Close" onclick="var _this = this; setTimeout(function(){_this.parentNode.style.display = \'none\';},400); this.parentNode.setAttribute(\'class\',(this.parentNode.getAttribute(\'class\')+\' msg-fadeout\'));">close</button></button></div>';

            if (typeof window.table !== "undefined") {
                table.ReloadData(items + '-table');
            }
        }

        //something as boot -> onload func
        (function (that){
            var msg = sessionStorage.getItem("redirected");
            if (msg !== null) {
                var session = JSON.parse(msg);
                message(session.status, session.message);
                sessionStorage.removeItem("redirected");
            }

            setPageTheme();

            if ("undefined" !== typeof window.page 
             && "undefined" !== typeof window.page.onload
            ) {
                window.page.onload();
            }
        })(); 

        return {
            // Public methods and variables
            destroy: function(object_id, object_name = null, route = null){
                destroy(object_id, object_name, route);
            },
            message: function(status, message){
                message(status, message);
            },
        };

    }
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function(){
            if(!instance){
                instance = init();
            }
            return instance;
        }
    };
})();
var cp = CpSingleton.getInstance();
