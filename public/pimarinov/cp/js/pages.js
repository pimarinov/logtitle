function allowAssigning() {
    var aC = document.getElementById('assign-container');
    var sC = document.getElementById('select-container');

    elementToggleClass(aC, 'd-none');
    elementToggleClass(sC, 'col-sm-8');
    elementToggleClass(sC, 'col-sm-6');
}

function elementToggleClass(element, className) {
    if (element.classList) { 
      element.classList.toggle(className);
    } else {
      // For IE9
      var classes = element.className.split(" ");
      var i = classes.indexOf(className);

      if (i >= 0) 
        classes.splice(i, 1);
      else 
        classes.push(className);
        element.className = classes.join(" "); 
    }
}

function asignSelectedSection() {
    var select = document.getElementById('section');
    var selected = select.options[select.selectedIndex];

    var li = document.getElementById('sec-'+selected.value).cloneNode(true);
    li.removeAttribute('id');

    var sortable = document.getElementById('sortable');
    sortable.appendChild(li);

    select.selectedIndex = 0;
    allowAssigning();
}

function remove(remove_span) {
    var li = remove_span.parentNode.parentNode.parentNode;
    li.parentNode.removeChild(li);
}

