const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .copy('resources/css/logs/', 'public/logs/css')
    .copy('packages/pimarinov/table/src/resources/css/table.css', 'public/logs/table/css/table.css')
    .copy('packages/pimarinov/table/src/resources/js/table.js', 'public/logs/table/js/table.js')
    .copy('resources/lt/img/', 'public/lt/img')
    .copy('resources/lt/css/login.css', 'public/lt/css/login.css')
    .copy('resources/lt/js/theme.js', 'public/lt/css/js/theme.js');

if (mix.inProduction()) {
    mix.version();
}
