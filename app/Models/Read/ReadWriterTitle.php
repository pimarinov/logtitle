<?php

namespace App\Models\Read;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadWriterTitle extends Model
{
    use HasFactory;
}
