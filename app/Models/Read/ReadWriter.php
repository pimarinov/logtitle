<?php

namespace App\Models\Read;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadWriter extends Model
{
    use HasFactory;
}
