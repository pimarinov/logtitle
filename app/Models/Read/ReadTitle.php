<?php

namespace App\Models\Read;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadTitle extends Model
{
    use HasFactory;
}
