<?php

namespace App\Models\Read\Log;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadLogTitle extends Model
{
    use HasFactory;

    public const RULES = [
        'user_id' => 'required|integer|exists:users,id',
        'language_id' => 'required|integer|exists:languages,id',
        'heading' => 'required|min:2|max:256', //..|unique:user.id,language.id,heading
        'serie_id' => 'nullable',
        'set_reading' => 'nullable|date_format:H:i:s',
        'end_reading' => 'nullable|date_format:H:i:s',
        'lib_id' => 'nulable|integer',
    ];

    public const MESSAGES = [
        'heading.unique' => 'You already have registered such heading in the selected language.',
        'language_id.integer' => 'Please select a language.',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'language_id',
        'heading',
        'summary',
        'description',
        'serie_id',
        'set_reading',
        'end_reading',
        'lib_id',
    ];
}
