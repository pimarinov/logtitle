<?php

namespace App\Models\Read\Log;

use App\Models\Format\DateFormat;
use App\Models\Language;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadableTitle extends Model
{
    use HasFactory;
    use DateFormat;

    public const MESSAGES = [
        'heading.unique' => 'You already have registered such heading in the selected language.',
        'language_id.integer' => 'Please select a language.',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'language_id',
        'title',
        'subtitle',
        'readable_serie_id',
    ];

    protected $casts = [
    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function language()
    {
        $this->belongsTo(Language::class);
    }

    // Get the created_at_formatted.
    /*public function getSetReadingFormattedAttribute() : string
    {
        return $this->set_reading->format($this->getDateFormat());
    }*/

    // Get the updated_at_formatted.
    /*public function getEndReadingFormattedAttribute() : string
    {
        return $this->end_reading->format($this->getDateFormat());
    }*/
}
