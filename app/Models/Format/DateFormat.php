<?php

namespace App\Models\Format;

trait DateFormat
{
    protected $dateTimeFormat = 'd.m.Y H:i:s';

    /**
     * Get the created_at_formatted.
     */
    public function getCreatedAtFormattedAttribute(): string
    {
        return $this->created_at->format($this->dateTimeFormat);
    }

    /**
     * Get the updated_at_formatted.
     */
    public function getUpdatedAtFormattedAttribute(): string
    {
        return $this->updated_at->format($this->dateTimeFormat);
    }

    /**
     * Get the created_at_diff.
     */
    public function getCreatedAtDiffAttribute(): string
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * Get the updated_at_diff.
     */
    public function getUpdatedAtDiffAttribute(): string
    {
        return $this->updated_at->diffForHumans();
    }
}
