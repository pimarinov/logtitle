<?php

namespace App\Models\Readable;

use App\Models\Readable\ReadableTitle;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReadableSerie extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Get the titles for the serie.
     */
    public function titles()
    {
        return $this->hasMany(ReadableTitle::class);
    }
}
