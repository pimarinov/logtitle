<?php

namespace App\Models\Readable;

use App\Models\Readable\ReadableTitle;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Writer extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The ReadableTitles that belong to the writer.
     */
    public function titles()
    {
        return $this->belongsToMany(ReadableTitle::class);
    }
}
