<?php

namespace App\Models\Readable;

use App\Models\Readable\ReadableSerie;
use App\Models\Readable\Writer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReadableTitle extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Get the serie that owns the title.
     */
    public function serie()
    {
        return $this->belongsTo(ReadableSerie::class);
    }

    /**
     * The writers that belong to the title.
     */
    public function roles()
    {
        return $this->belongsToMany(Writer::class);
    }
}
