<?php

namespace App\Features\Logs;

use App\Features\Logs\Read\Settings\HomeFeature as SettingsHomeFeature;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lucid\Units\Feature;

/**
 * @see https://docs.google.com/document/d/1ePi9GJyym3Dr3JqwuCP-2Qyby3G8DLjBAoIy2WxHwvI/edit?usp=sharing
 */
class LogsFeature extends Feature
{
    protected $controller;
    protected $action;

    public function __construct(string $controller, string $method)
    {
        $this->controller = $controller;
        $this->method = $method;
    }
}
