<?php

namespace App\Features\Logs\Read\Lib;

use App\Domains\Logs\Jobs\ReadLibHomeJob;
use Illuminate\Support\Facades\Request;

class HomeFeature extends ReadLibFeature
{
    public function handle(Request $request)
    {
        return $this->run(new ReadLibHomeJob(
            $this->controller,
            $this->method
        ));
    }
}
