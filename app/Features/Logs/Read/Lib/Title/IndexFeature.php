<?php

namespace App\Features\Logs\Read\Lib\Title;

use App\Domains\Logs\Jobs\ReadLibTitleIndexJob;
//use App\Domains\Logs\Read\Lib\Title\Jobs\NavLibTitleIndexJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Domains\Logs\Tables\Titles as Table;
use App\Features\Logs\Read\Lib\ReadLibFeature;
use Illuminate\Http\Request;

class IndexFeature extends ReadLibFeature
{
    public function handle(Request $request)
    {
        $nav = $this->run(ReadLibTitleIndexJob::class, [
            'controller' => $this->controller,
            'method' => $this->method,
            'request' => $request,
        ]);

        $data = [];
        $data['table'] = Table::class;
        $data['assets'] = Table::init();

        $isJsonRoute = 'read.lib.titles.json' === $request->route()->getName();

        return $isJsonRoute
            ? Table::index($undeletableKeys ?? [])
            : $this->run(RespondWithViewJob::class, [
                'view' => 'logs.read.lib.title.index',
                'nav' => $nav,
                'data' => $data,
            ]);
    }
}
