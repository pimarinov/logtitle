<?php

namespace App\Features\Logs\Read\Lib\Writer;

use App\Domains\Logs\Jobs\ReadLibWriterIndexJob;
//use App\Domains\Logs\Read\Lib\Writer\Jobs\NavLibWriterIndexJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Domains\Logs\Tables\Writers as Table;
use App\Features\Logs\Read\Lib\ReadLibFeature;
use Illuminate\Http\Request;

class IndexFeature extends ReadLibFeature
{
    public function handle(Request $request)
    {
        $nav = $this->run(ReadLibWriterIndexJob::class, [
            'controller' => $this->controller,
            'method' => $this->method,
            'request' => $request,
        ]);

        $data = [];
        $data['table'] = Table::class;
        $data['assets'] = Table::init();

        $isJsonRoute = 'read.lib.writers.json' === $request->route()->getName();
        /*ob_start();
        TitleTable::index($undeletableKeys ?? []);
        $table = ob_get_clean();*/

        /*return $this->request->expectsJson()
            ? Table::index($undeletableKeys ?? [])
            : view('logs.read.log.title.index', compact('nav', 'table', 'assets'));*/

        return $isJsonRoute
            ? Table::index($undeletableKeys ?? [])
            : $this->run(RespondWithViewJob::class, [
                'view' => 'logs.read.log.title.index',
                'nav' => $nav,
                'data' => $data,
            ]);
    }
}
