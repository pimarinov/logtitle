<?php

namespace App\Features\Logs\Read\Settings;

//use App\Domains\Logs\Read\Settings\Home\Jobs\NavSettingsHomeJob;
use App\Domains\Logs\Jobs\ReadSettingsHomeJob;
use Illuminate\Http\Request;

class HomeFeature extends ReadSettingsFeature
{
    public function handle(Request $request)
    {
        return $this->run(new ReadSettingsHomeJob(
            $this->controller,
            $this->method
        ));
    }
}
