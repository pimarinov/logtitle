<?php

namespace App\Features\Logs\Read\Log\Serie;

use App\Domains\Logs\Jobs\ReadLogSerieIndexJob;
use App\Domains\Logs\Jobs\RespondWithJsonJob;
//use Lucid\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Domains\Logs\Jobs\TableReadLogSeries;
use App\Domains\Logs\Tables\Titles as Table;
use App\Features\Logs\Read\Log\ReadLogFeature;
use Illuminate\Http\Request;

class IndexFeature extends ReadLogFeature
{
    public function handle(Request $request)
    {
        $nav = $this->run(ReadLogSerieIndexJob::class, [
            'controller' => $this->controller,
            'method' => $this->method,
            'request' => $request,
        ]);

        $response = $this->run(TableReadLogSeries::class);

        return isset($response['body'])
            ? $this->run(RespondWithJsonJob::class, [
                'data' => $response,
            ])
            : $this->run(RespondWithViewJob::class, [
                'view' => 'logs.read.log.serie.index',
                'nav' => $nav,
                'data' => $response,
            ]);
    }
}
