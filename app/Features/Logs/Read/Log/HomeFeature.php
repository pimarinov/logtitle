<?php

namespace App\Features\Logs\Read\Log;

use App\Domains\Logs\Jobs\ReadLogHomeJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class HomeFeature extends Feature
{
    public function handle(Request $request)
    {
        $discloser = $this->run(ReadLogHomeJob::class);

        return $this->run(RespondWithViewJob::class, [
            'view' => 'logs.read.log.home',
            'nav' => $discloser->nav,
            'data' => [],
        ]);
    }
}
