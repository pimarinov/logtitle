<?php

namespace App\Features\Logs\Read\Log\Title;

use App\Domains\Logs\Jobs\ReadLogTitleStoreJob;
use App\Domains\Logs\Jobs\RespondWithRedirectJob;
use App\Http\Requests\ReadTitleSaveRequest;
use App\Http\Resources\ReadableTitleResource;
use Lucid\Units\Feature;

class StoreFeature extends Feature
{
    public function handle(ReadTitleSaveRequest $request)
    {
        $request->merge(['user_id' => $request->user()->id]);

        $readableTitle = $this->run(ReadLogTitleStoreJob::class);

        $title = new ReadableTitleResource($readableTitle);

        return $this->run(RespondWithRedirectJob::class, [
            'route' => 'read.log.titles.show',
            'routeArgs' => ['title' => $title],
            'status' => 'The title has been created.',
        ]);
    }
}
