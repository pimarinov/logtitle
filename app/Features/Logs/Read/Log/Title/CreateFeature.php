<?php

namespace App\Features\Logs\Read\Log\Title;

use App\Domains\Logs\Jobs\LanguagesGetAllJob;
use App\Domains\Logs\Jobs\ReadLogTitleCreateJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Features\Logs\Read\Log\ReadLogFeature;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class CreateFeature extends Feature
{
    public function handle(Request $request)
    {
        $request->merge([
            'languages' => $this->run(LanguagesGetAllJob::class),
        ]);

        $discloser = $this->run(ReadLogTitleCreateJob::class);

        return $this->run(RespondWithViewJob::class, [
            'view' => $discloser->view,
            'nav' => $discloser->nav,
            'data' => $discloser->data,
        ]);
    }
}
