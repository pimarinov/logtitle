<?php

namespace App\Features\Logs\Read\Log\Title;

use App\Domains\Logs\Jobs\ReadLogTitleIndexJob;
use App\Domains\Logs\Jobs\RespondWithJsonJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
//use Lucid\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Logs\Jobs\TableReadLogTitles;
use App\Features\Logs\Read\Log\ReadLogFeature;
use Illuminate\Http\Request;

class IndexFeature extends ReadLogFeature
{
    public function handle(Request $request)
    {
        $nav = $this->run(ReadLogTitleIndexJob::class, [
            'controller' => $this->controller,
            'method' => $this->method,
            'request' => $request,
        ]);

        $response = $this->run(TableReadLogTitles::class);

        return isset($response['body'])
            ? $this->run(RespondWithJsonJob::class, [
                'data' => $response,
            ])
            : $this->run(RespondWithViewJob::class, [
                'view' => 'logs.read.log.serie.index',
                'nav' => $nav,
                'data' => $response,
            ]);
    }
}
