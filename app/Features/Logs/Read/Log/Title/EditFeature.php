<?php

namespace App\Features\Logs\Read\Log\Title;

use App\Domains\Logs\Jobs\LanguagesGetAllJob;
//use App\Features\Logs\Read\Log\ReadLogFeature;
use App\Domains\Logs\Jobs\ReadLogTitleEditJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Http\Resources\ReadableTitleResource;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class EditFeature extends Feature
{
    public function handle(Request $request)
    {
        /*$request->merge([
            'languages' => $this->run(LanguagesGetAllJob::class)
        ]);*/

        $discloser = $this->run(ReadLogTitleEditJob::class);

        $discloser->data['title'] = new ReadableTitleResource($request->title);
        $discloser->data['languages'] = $this->run(LanguagesGetAllJob::class);

        return $this->run(RespondWithViewJob::class, [
            'view' => $discloser->view,
            'nav' => $discloser->nav,
            'data' => $discloser->data,
        ]);
    }
}
