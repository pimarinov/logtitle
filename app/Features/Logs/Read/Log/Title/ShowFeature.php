<?php

namespace App\Features\Logs\Read\Log\Title;

use App\Domains\Logs\Jobs\LanguagesGetAllJob;
//use App\Features\Logs\Read\Log\ReadLogFeature;
use App\Domains\Logs\Jobs\ReadLogTitleShowJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use App\Models\Readable\ReadableTitle;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class ShowFeature extends Feature
{
    public function handle(Request $request)
    {
        $discloser = $this->run(ReadLogTitleShowJob::class);

        return $this->run(RespondWithViewJob::class, [
            'view' => $discloser->view,
            'nav' => $discloser->nav,
            'data' => $discloser->data,
        ]);

        $nav = $this->run(ReadLogTitleShowJob::class);

        $data = $request->title->toArray() + ['title' => $request->title];
        $data['languages'] = $this->run(LanguagesGetAllJob::class);
        $data['item'] = 'title';
        $data['icon'] = 'title';

        return $this->run(RespondWithViewJob::class, [
            'view' => 'logs.read.log.title.show',
            'nav' => $nav,
            'data' => $data,
        ]);
    }
}
