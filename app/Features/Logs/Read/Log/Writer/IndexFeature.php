<?php

namespace App\Features\Logs\Read\Log\Writer;

use App\Domains\Logs\Jobs\ReadLogWriterIndexJob;
use App\Domains\Logs\Jobs\RespondWithJsonJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
//use Lucid\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Logs\Jobs\TableReadLogWriters;
use App\Domains\Logs\Tables\Writers as Table;
use App\Features\Logs\Read\Log\ReadLogFeature;
use Illuminate\Http\Request;

class IndexFeature extends ReadLogFeature
{
    public function handle(Request $request)
    {
        $nav = $this->run(ReadLogWriterIndexJob::class, [
            'controller' => $this->controller,
            'method' => $this->method,
            'request' => $request,
        ]);

        $response = $this->run(TableReadLogWriters::class);

        return isset($response['body'])
            ? $this->run(RespondWithJsonJob::class, [
                'data' => $response,
            ])
            : $this->run(RespondWithViewJob::class, [
                'view' => 'logs.read.log.writer.index',
                'nav' => $nav,
                'data' => $response,
            ]);
    }
}
