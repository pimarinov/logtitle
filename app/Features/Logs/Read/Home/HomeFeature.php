<?php

namespace App\Features\Logs\Read\Home;

use App\Domains\Logs\Jobs\ReadHomeHomeJob;
use App\Domains\Logs\Jobs\RespondWithViewJob;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class HomeFeature extends Feature
{
    public function handle(Request $request)
    {
        $discloser = $this->run(ReadHomeHomeJob::class);

        return $this->run(RespondWithViewJob::class, [
            'view' => 'logs.read.home.home',
            'nav' => $discloser->nav,
            'data' => [],
        ]);
    }
}
