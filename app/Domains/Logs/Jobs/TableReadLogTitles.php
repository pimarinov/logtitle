<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Tables\Titles;
use Illuminate\Http\Request;
use Lucid\Units\Job;

class TableReadLogTitles extends Job
{
    public const ROUTE_JSON = 'read.log.titles.json';

    /**
     * Execute the job.
     */
    public function handle(Request $request)
    {
        $assets = Titles::init();

        if (self::ROUTE_JSON === $request->route()->getName())
        {
            return Titles::index($undeletableKeys ?? []);
        }

        $data = [];
        $data['table'] = Titles::class;
        $data['assets'] = $assets;

        return $data;
    }
}
