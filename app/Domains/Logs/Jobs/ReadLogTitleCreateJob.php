<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogTitle;
use Illuminate\Http\Request;

class ReadLogTitleCreateJob extends NavLogTitle
{
    public const INDEX_BLADE = 'read.log.titles.index';

    /**
     * Execute the job.
     */
    public function handle(Request $request): \stdClass
    {
        $this->method = $request->route()->getActionMethod();

        $data = [];
        $data['languages'] = $request->languages;

        return (object) [
            'nav' => $this->getNav(),
            'data' => $data,
            'view' => 'logs.read.log.title.create',
        ];
    }
}
