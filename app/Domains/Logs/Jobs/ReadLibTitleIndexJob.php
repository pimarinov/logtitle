<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Lib\NavLibTitle;

class ReadLibTitleIndexJob extends NavLibTitle
{
    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
