<?php

namespace App\Domains\Logs\Jobs;

use App\Http\Requests\ReadTitleSaveRequest;
use App\Models\Read\Log\ReadableTitle;
use Lucid\Units\Job;

class ReadLogTitleStoreJob extends Job
{
    /**
     * Execute the job.
     */
    public function handle(ReadTitleSaveRequest $request): ReadableTitle
    {
        $title = new ReadableTitle();

        $data = $request->only($title->getFillable());

        $data['user_id'] = $request->user()->id;

        $title->fill($data);

        $title->saveOrFail();

        return $title;
    }
}
