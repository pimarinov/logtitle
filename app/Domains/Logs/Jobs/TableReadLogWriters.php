<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Tables\Writers;
use Illuminate\Http\Request;
use Lucid\Units\Job;

class TableReadLogWriters extends Job
{
    public const ROUTE_JSON = 'read.log.writers.json';

    /**
     * Execute the job.
     */
    public function handle(Request $request)
    {
        $assets = Writers::init();

        if (self::ROUTE_JSON === $request->route()->getName())
        {
            return Writers::index($undeletableKeys ?? []);
        }

        $data = [];
        $data['table'] = Writers::class;
        $data['assets'] = $assets;

        return $data;
    }
}
