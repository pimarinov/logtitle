<?php

namespace App\Domains\Logs\Jobs;

use Lucid\Units\Job;

class RespondWithViewJob extends Job
{
    private $view;
    private $nav;
    private $data;

    /**
     * Create a new job instance.
     */
    public function __construct(string $view, \stdClass $nav, array $data = [])
    {
        $this->view = $view;
        $this->nav = $nav;
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        /*if ($this->isJson && isset($this->data['table']))
        {
            $table = $this->data['table'];
            return $table->index($undeletableKeys ?? []);
        }*/

        $nav = $this->nav;
        $assets = $this->data['assets'] ?? null;
        $table = $this->data['table'] ?? null;

        return view($this->view, $this->data + compact('nav', 'assets', 'table'));
    }
}
