<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Home\NavHomeHome;
use Illuminate\Http\Request;

class ReadHomeHomeJob extends NavHomeHome
{
    public function handle(Request $request): \stdClass
    {
        return (object) [
            'nav' => $this->getNav(),
        ];
    }
}
