<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Lib\NavLibSerie;

class ReadLibSerieIndexJob extends NavLibSerie
{
    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
