<?php

namespace App\Domains\Logs\Jobs;

use Lucid\Units\Job;

class RespondWithRedirectJob extends Job
{
    private $route;
    private $routeArgs;
    private $status;

    /**
     * Create a new job instance.
     */
    public function __construct(string $route, array $routeArgs, string $status = null)
    {
        $this->route = $route;
        $this->routeArgs = $routeArgs;
        $this->status = $status;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        return $this->status
            ? redirect()
                ->route($this->route, $this->routeArgs)
                ->with('status', $this->status)
            : redirect()
                ->route($this->route, $this->routeArgs);
    }
}
