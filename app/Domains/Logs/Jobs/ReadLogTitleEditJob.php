<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogTitle;
use App\Http\Requests\ReadTitleEditRequest;

class ReadLogTitleEditJob extends NavLogTitle
{
    /**
     * Execute the job.
     */
    public function handle(ReadTitleEditRequest $request): \stdClass
    {
        return (object) [
            'nav' => $this->getNav(),
            'data' => [],
            'view' => 'logs.read.log.title.edit',
        ];
    }
}
