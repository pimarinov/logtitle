<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Lib\NavLibWriter;

class ReadLibWriterIndexJob extends NavLibWriter
{
    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
