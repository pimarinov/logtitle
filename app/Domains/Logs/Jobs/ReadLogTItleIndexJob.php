<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogTitle;

class ReadLogTitleIndexJob extends NavLogTitle
{
    public const CREATE = [
        'blade' => 'read.log.titles.create',
        'item' => 'title',
    ];

    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
