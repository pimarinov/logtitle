<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogTitle;
use App\Http\Resources\ReadableTitleResource;
use Illuminate\Http\Request;

class ReadLogTitleShowJob extends NavLogTitle
{
    /**
     * Execute the job.
     */
    public function handle(Request $request): \stdClass
    {
        $data = [];
        $data['title'] = new ReadableTitleResource($request->title);

        return (object) [
            'nav' => $this->getNav(),
            'data' => $data,
            'view' => 'logs.read.log.title.show',
        ];
    }
}
