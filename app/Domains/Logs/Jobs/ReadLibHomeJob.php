<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Lib\NavLibHome;
use Illuminate\Http\Request;

class ReadLibHomeJob extends NavLibHome
{
    public function handle(Request $request)
    {
        $nav = $this->getNav();

        $usersCount = 0;

        return view('logs.read.lib.home', compact('nav', 'usersCount'));
    }
}
