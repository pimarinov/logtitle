<?php

namespace App\Domains\Logs\Jobs;

use Lucid\Units\Job;

class RespondWithJsonJob extends Job
{
    private $data;

    /**
     * Create a new job instance.
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        return response()->json($this->data);
    }
}
