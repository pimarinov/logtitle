<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Tables\Series;
use Illuminate\Http\Request;
use Lucid\Units\Job;

class TableReadLogSeries extends Job
{
    public const ROUTE_JSON = 'read.log.series.json';

    /**
     * Execute the job.
     */
    public function handle(Request $request)
    {
        $assets = Series::init();

        if (self::ROUTE_JSON === $request->route()->getName())
        {
            return Series::index($undeletableKeys ?? []);
        }

        $data = [];
        $data['table'] = Series::class;
        $data['assets'] = $assets;

        return $data;
    }
}
