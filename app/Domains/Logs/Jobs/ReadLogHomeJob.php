<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogHome;
use Illuminate\Http\Request;

class ReadLogHomeJob extends NavLogHome
{
    /**
     * Execute the job.
     */
    public function handle(Request $request): \stdClass
    {
        return (object) [
            'nav' => $this->getNav(),
            'data' => null,
            'view' => 'logs.read.log.title.edit',
        ];
    }
}
