<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Settings\NavSettingsAccount;
use Illuminate\Http\Request;

class ReadSettingsAccountJob extends NavSettingsAccount
{
    public function handle(Request $request)
    {
        $nav = $this->getNav();

        return view('logs.read.settings.account', compact('nav'));
    }
}
