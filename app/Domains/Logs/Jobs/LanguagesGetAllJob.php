<?php

namespace App\Domains\Logs\Jobs;

use App\Models\Language;
use Lucid\Units\Job;

class LanguagesGetAllJob extends Job
{
    /**
     * Execute the job.
     */
    public function handle()
    {
        return Language::all();
    }
}
