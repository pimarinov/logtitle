<?php

namespace App\Domains\Logs\Jobs;

use App\Http\Requests\ReadTitleUpdateRequest;
use App\Models\Read\Log\ReadableTitle;
use Lucid\Units\Job;

class ReadLogTitleUpdateJob extends Job
{
    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(ReadTitleUpdateRequest $request): ReadableTitle
    {
        dd($requset->route()->id);
        $title = ReadableTitle::find($requset->route()->id);

        $data = $request->only($title->getFillable());

        $data['user_id'] = $request->user()->id;

        $title->fill($data);

        $title->saveOrFail();

        return $title;
    }
}
