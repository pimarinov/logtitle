<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Settings\NavSettingsHome;
use App\Models\User;
use Illuminate\Http\Request;

class ReadSettingsHomeJob extends NavSettingsHome
{
    public function handle(Request $request)
    {
        $nav = $this->getNav();

        $gravatar = User::find(\Auth::user()->id)->gravatar;

        return view('logs.read.settings.home', compact('nav', 'gravatar'));
    }
}
