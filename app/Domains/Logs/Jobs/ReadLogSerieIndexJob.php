<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogSerie;

class ReadLogSerieIndexJob extends NavLogSerie
{
    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
