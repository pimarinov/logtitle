<?php

namespace App\Domains\Logs\Jobs;

use App\Domains\Logs\Nav\Read\Log\NavLogWriter;

class ReadLogWriterIndexJob extends NavLogWriter
{
    /**
     * Execute the job.
     */
    public function handle(): \stdClass
    {
        return $this->getNav();
    }
}
