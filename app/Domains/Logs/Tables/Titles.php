<?php

namespace App\Domains\Logs\Tables;

use Pimarinov\Table\Table as table;

class Titles extends BaseTable
{
    final public static function index(array $undeletableKeys = [])
    {
        $items = 'titles';
        $item = 'user';

        if (filter_has_var(INPUT_GET, 'time'))
        {
            self::$rustart = getrusage();
        }

        table::create('titles', 'id', 'desc', 50);
        table::$cols[] = ['ID', 'id', ['width' => '50px']];
        table::$cols[] = ['Heading', 'heading'];
        /*table::$cols[] = ["Summary", "summary"];
        table::$cols[] = ["test","test",["sort"=>false]];
        table::$cols[] = ["Created", "created_at_formatted",
                            ["sort"=>"created_at","width"=>"130px"]];*/
        table::$cols[] = ['Updated', 'updated_at_formatted',
            ['sort' => 'updated_at', 'width' => '130px'], ];
        table::$cols[] = ['...', 'links', ['width' => '78px', 'class' => 'links', 'sort' => false]];
        table::$cols[] = ['...', 'link', ['width' => '40px', 'class' => 'link', 'sort' => false]];

        table::execute('SELECT *' . self::qdates() . ' FROM titles ');

        foreach (table::$data as &$cells)
        {
            $cells = get_object_vars($cells); //object -> array
            $id = (int) $cells['id'];
            $cells['link'] = self::icoshow($items, $id, $item);
            $links = in_array($id, $undeletableKeys) ?
                    [self::icoedit($items, $id, $item)] :
                    [self::icodel($items, $id, $item, route('read.home', $id)), self::icoedit($items, $id, $item)];
            $cells['links'] = implode('', $links);
            if (table::$export === false)
            {
                //$cells['heading'] = self::show($items, $id, $cells['heading']);
                //$cells['id'] = self::id($items, $id);
                //$cells['email'] = self::mailto($cells['email']);
            }
        }

        table::$attributes['table']['class'] = 'table-striped table-sm table-hover table-bordered';

        return table::load();
        if (isset(self::$rustart))
        {
            self::time();
        }
    }
}
