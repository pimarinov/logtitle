<?php

namespace App\Domains\Logs\Nav;

use App\Domains\Logs\Nav\Read\Home\NavHomeHome as NavReadHome;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lucid\Units\Job;

class NavLogs extends Job
{
    public const LOG_READ = 'read';
    public const LOG_PLAY = 'play';
    protected $controller;
    protected $method;
    protected $request;

    /**
     * Create a new job instance.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->method = $request->route()->getActionMethod();
        //$this->controller = $request->route()->getController();
    }

    public function getNav(): \stdClass
    {
        return (object) [
            'log' => static::LOG,
            'navSections' => static::NAV_SECTIONS,
            'navSubSections' => static::NAV_SUB_SECTIONS,
            'currentSection' => static::CURRENT_SECTION,
            'currentSubSection' => static::CURRENT_SUB_SECTION,
            'currentMethod' => $this->method,
            'breadcrumbs' => $this->getBreadcrumbs(),
            'headTitle' => $this->getHeadTitle(),
            'create' => defined('static::CREATE') ? static::CREATE : null,
            'websiteName' => env('APP_NAME', 'SomeSite'),
        ];
    }

    private function getHeadTitle()
    {
        $pieces = [];

        $pieces[] = Str::ucfirst(static::LOG);

        $pieces[] = Str::ucfirst(static::CURRENT_SECTION);

        $pieces[] = Str::ucfirst(static::CURRENT_SUB_SECTION);

        return implode(' &#10093; ', $pieces);
    }

    private function getBreadcrumbs()
    {
        $list = [];

        $list[($current = static::LOG)] = [
            'name' => Str::ucfirst($current),
            'url' => route('read.home'),
        ];

        if (NavReadHome::SECTION_HOME === static::CURRENT_SECTION)
        { // Read › Home
            $list[$this->method] = [
                'name' => Str::ucfirst(static::CURRENT_SUB_SECTION),
            ];

            return $list;
        }

        $list[($current = static::CURRENT_SECTION)] = [
            'name' => Str::ucfirst($current),
            'url' => route("read.{$current}.home"),
        ];

        if ('home' === static::CURRENT_SUB_SECTION)
        {
            $list[($current = static::CURRENT_SUB_SECTION)] = [
                'name' => Str::ucfirst($current),
            ];

            return $list;
        }

        /*$list[($current = static::CURRENT_SUB_SECTION)] = [
            'name' => Str::ucfirst($current),
            'url' => route('read.' . static::CURRENT_SECTION . ".{$current}"),
        ];*/

        $list[($current = static::CURRENT_SUB_SECTION)] = [
            'name' => Str::ucfirst($current),
            'url' => route('read.' . static::CURRENT_SECTION . ".{$current}.index"),
        ];

        $method = $this->method === 'index' ? 'list' : $this->method;
        $list[$this->method] = [
            'name' => Str::ucfirst($method),
        ];

        return $list;
    }
}
