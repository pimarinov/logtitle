<?php

namespace App\Domains\Logs\Nav\Read\Log;

class NavLogWriter extends NavLog
{
    public const CURRENT_SUB_SECTION = self::SUB_SECTION_WRITERS;
}
