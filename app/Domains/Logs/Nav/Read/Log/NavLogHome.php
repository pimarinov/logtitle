<?php

namespace App\Domains\Logs\Nav\Read\Log;

class NavLogHome extends NavLog
{
    public const CURRENT_SUB_SECTION = self::SUB_SECTION_HOME;
}
