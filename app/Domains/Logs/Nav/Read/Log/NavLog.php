<?php

namespace App\Domains\Logs\Nav\Read\Log;

use App\Domains\Logs\Nav\Read\NavRead;

class NavLog extends NavRead
{
    public const CURRENT_SECTION = self::SECTION_LOG;

    public const SUB_SECTION_HOME = 'home';
    public const SUB_SECTION_TITLES = 'titles';
    public const SUB_SECTION_WRITERS = 'writers';
    public const SUB_SECTION_SERIES = 'series';

    public const NAV_SUB_SECTIONS = [
        self::SUB_SECTION_HOME,
        self::SUB_SECTION_TITLES,
        self::SUB_SECTION_WRITERS,
        self::SUB_SECTION_SERIES,
    ];
}
