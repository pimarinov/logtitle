<?php

namespace App\Domains\Logs\Nav\Read\Settings;

class NavSettingsAccount extends NavSettings
{
    public const CURRENT_SUB_SECTION = self::SUB_SECTION_ACCOUNT;
}
