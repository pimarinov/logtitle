<?php

namespace App\Domains\Logs\Nav\Read\Settings;

use App\Domains\Logs\Nav\Read\NavRead;

class NavSettings extends NavRead
{
    public const CURRENT_SECTION = self::SECTION_SETTINGS;

    public const SUB_SECTION_HOME = 'home';
    public const SUB_SECTION_ACCOUNT = 'account';

    public const NAV_SUB_SECTIONS = [
        self::SUB_SECTION_HOME,
        self::SUB_SECTION_ACCOUNT,
    ];
}
