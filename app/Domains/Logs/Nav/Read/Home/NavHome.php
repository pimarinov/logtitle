<?php

namespace App\Domains\Logs\Nav\Read\Home;

use App\Domains\Logs\Nav\Read\NavRead;

class NavHome extends NavRead
{
    public const CURRENT_SECTION = self::SECTION_HOME;

    public const SUB_SECTION_HOME = 'home';

    public const NAV_SUB_SECTIONS = [
        self::SUB_SECTION_HOME,
    ];
}
