<?php

namespace App\Domains\Logs\Nav\Read\Home;

class NavHomeHome extends NavHome
{
    public const CURRENT_SUB_SECTION = self::SUB_SECTION_HOME;
}
