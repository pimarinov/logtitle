<?php

namespace App\Domains\Logs\Nav\Read;

use App\Domains\Logs\Nav\NavLogs;

class NavRead extends NavLogs
{
    public const LOG = self::LOG_READ;

    public const SECTION_HOME = 'home';
    public const SECTION_LOG = 'log';
    public const SECTION_LIB = 'lib';
    public const SECTION_SETTINGS = 'settings';

    public const NAV_SECTIONS = [
        self::SECTION_HOME,
        self::SECTION_LOG,
        self::SECTION_LIB,
        self::SECTION_SETTINGS,
    ];

    public const NAV_SECTIONS_ICONS = [
        self::SECTION_HOME => 'home',
        self::SECTION_LOG => 'collections_bookmark',
        self::SECTION_LIB => 'collections',
        self::SECTION_SETTINGS => 'settings',
    ];
}
