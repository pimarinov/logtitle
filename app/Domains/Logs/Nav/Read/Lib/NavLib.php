<?php

namespace App\Domains\Logs\Nav\Read\Lib;

use App\Domains\Logs\Nav\Read\NavRead;

class NavLib extends NavRead
{
    public const CURRENT_SECTION = self::SECTION_LIB;

    public const SUB_SECTION_HOME = 'home';
    public const SUB_SECTION_TITLES = 'titles';
    public const SUB_SECTION_WRITERS = 'writers';
    public const SUB_SECTION_SERIES = 'series';

    public const NAV_SUB_SECTIONS = [
        self::SUB_SECTION_HOME,
        self::SUB_SECTION_TITLES,
        self::SUB_SECTION_WRITERS,
        self::SUB_SECTION_SERIES,
    ];
}
