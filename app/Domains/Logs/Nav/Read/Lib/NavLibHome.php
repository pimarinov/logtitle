<?php

namespace App\Domains\Logs\Nav\Read\Lib;

class NavLibHome extends NavLib
{
    public const CURRENT_SUB_SECTION = self::SUB_SECTION_HOME;
}
