<?php

namespace App\Policies\Readable;

use App\Models\ReadableTitle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReadableTitlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function viewAny(User $user)
    {
    }

    /**
     * Determine whether the user can view the model.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function view(User $user, ReadableTitle $readableTitle)
    {
        return $user->id === $readableTitle->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function create(User $user)
    {
    }

    /**
     * Determine whether the user can update the model.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function update(User $user, ReadableTitle $readableTitle)
    {
        return $user->id === $readableTitle->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function delete(User $user, ReadableTitle $readableTitle)
    {
        return $user->id === $readableTitle->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function restore(User $user, ReadableTitle $readableTitle)
    {
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @return bool|\Illuminate\Auth\Access\Response
     */
    public function forceDelete(User $user, ReadableTitle $readableTitle)
    {
    }
}
