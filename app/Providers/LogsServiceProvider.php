<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LogsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/logs.php');
        //$this->loadViewsFrom(__DIR__.'/../../resources/views/my-log', 'my-log');
        /*$this->publishes([
            __DIR__.'/../Services/Library/resources/css' => public_path('cp/css'),
            __DIR__.'/../Services/Library/resources/js' => public_path('cp/js'),
        ], 'library');*/

        /*
                $this->loadRoutesFrom(__DIR__.'/../routes/cp.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'cp');
        $this->publishes([
            __DIR__.'/../resources/css' => public_path('pimarinov/cp/css'),
            __DIR__.'/../resources/js' => public_path('pimarinov/cp/js'),
        ], 'cp');*/
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        // $this->app->make('Pimarinov\Cp\App\Http\Controllers\CpController');
    }
}
