<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//use App\Http\Controllers\Settings\VariablesController;

class CpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/cp.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views/cp', 'cp');
        /*$this->loadRoutesFrom(__DIR__.'/../Services/Cp/routes/cp.php');
        $this->loadViewsFrom(__DIR__.'/../Services/Cp/resources/views', 'cp1');
        $this->publishes([
            __DIR__.'/../Services/Cp/resources/css' => public_path('cp/css'),
            __DIR__.'/../Services/Cp/resources/js' => public_path('cp/js'),
        ], 'cp1');*/

        /*
                $this->loadRoutesFrom(__DIR__.'/../routes/cp.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'cp');
        $this->publishes([
            __DIR__.'/../resources/css' => public_path('pimarinov/cp/css'),
            __DIR__.'/../resources/js' => public_path('pimarinov/cp/js'),
        ], 'cp');*/
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        // $this->app->make('Pimarinov\Cp\App\Http\Controllers\CpController');
    }
}
