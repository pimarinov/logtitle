<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReadTitleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        dd($this->user_id);

        return $this->user_id === $this->route()->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'user_id' => 'required|int|exists:users,id',
            'title' => 'required|string|min:2:max:255',
            'language_id' => 'required|int|exists:languages,id',
            'readable_serie_id' => 'nullable|int|exists:redable_series,id',
            'summary' => 'nullable|string',
            'description' => 'nullable|string',
        ];
    }
}
