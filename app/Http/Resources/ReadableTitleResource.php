<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReadableTitleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        dd($this->toArray($request));

        return [
            'id' => $this->id,
            'user' => $this->user,
            'language' => $this->language,
            'readable_serie' => $this->readable_serie_id ? $this->readable_serie : null,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_at_formatted' => $this->created_at_formatted,
            'updated_at_formatted' => $this->updated_at_formatted,
        ];
    }
}
