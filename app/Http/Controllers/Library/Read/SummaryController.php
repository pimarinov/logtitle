<?php

namespace App\Http\Controllers\Library\Read;

use Pimarinov\Cp\App\Http\Tables\DummyUsers;
use Pimarinov\Cp\App\Http\Tables\Users;

class SummaryController extends SubController
{
    public const RECENTS_COUNT = 3;
    //use BaseTrait;

    public static $section = 'summary';

    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(/* Request $request */)
    {
        $vars = self::vars(self::$section);

        /*$vars['langs'] = Lang::get(($ids_flag = true));
        $vars['lang_default'] = $vars['langs'][array_keys($vars['langs'])[0]];*/

        $this->panels_vars($vars);

        return view($vars['view'], $vars);
    }

    public function index()
    {
        $vars = [
            'section' => self::$section,
            'action' => self::$section,
            'subNavSection' => 'summary',
        ];

        $vars['users_count'] = Users::count();

        $vars['dummy_users_count'] = sprintf(
            '%s Millions',
            number_format((DummyUsers::count() / 1000000), 2)
        );

        //$this->vars($vars);
        $vars = $this->getVars($vars);

        return view('library::read.summary', $vars);
    }

    /**
     * Cp Logout action.
     *
     * @see return AuthenticatedSessionController::destroy();
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        $this->guard->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('site.home'); //->with('success', $msg);
    }

    private function panels_vars(&$vars)
    {
        // $ld_id = $vars['lang_default'];
        $d = DB::select('SELECT 
                (SELECT COUNT(`id`) FROM `purls`) AS `purls_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `purls` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                            ) AS `purls_r`
                    ) AS `purls_recents`,
                (SELECT COUNT(`id`) FROM `sections`) AS `sections_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `sections` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                            ) AS `sections_r`
                    ) AS `sections_recents`,
                (SELECT COUNT(`id`) FROM `pages`) AS `pages_count`,
                (SELECT GROUP_CONCAT(
                            CONCAT(`id`, "|", `updated_at`)
                            SEPARATOR ","
                        ) 
                        FROM (SELECT `id`, `updated_at` 
                            FROM `pages` 
                            ORDER BY updated_at DESC
                            LIMIT ' . self::RECENTS_COUNT . '
                        ) AS `pages_r`
                    ) AS `pages_recents`,
                (SELECT COUNT(`id`) FROM `users`) AS `users_count`,
                (SELECT COUNT(`id`) FROM `langs`) AS `langs_count`,
                (SELECT COUNT(`id`) FROM `vars`) AS `vars_count`
            ')[0];

        foreach (['purls', 'sections', 'pages'] as $p)
        {
            $name = $p . '_recents';

            $current = [];

            $dd = explode(',', $d->{$name});
            foreach ($dd as $ddd)
            {
                $id_date = explode('|', $ddd);
                $updated = \Carbon\Carbon::createFromDate($id_date[1]);
                $current[] = (object) ['id' => $id_date[0],
                    'updated' => $updated,
                    'datetime' => $id_date[1],
                ];
            }

            $d->{$name} = $current;
        }

        $vars['panels'] = $d;
    }
}
