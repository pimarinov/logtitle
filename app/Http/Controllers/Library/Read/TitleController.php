<?php

namespace App\Http\Controllers\Library\Read;

use App\Http\Controllers\Library\LibraryController;
use App\Models\Read\ReadTitle;
use Illuminate\Http\Request;

class TitleController extends LibraryController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ReadTitle $readTitle
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ReadTitle $readTitle)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ReadTitle $readTitle
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ReadTitle $readTitle)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\ReadTitle $readTitle
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReadTitle $readTitle)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ReadTitle $readTitle
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReadTitle $readTitle)
    {
    }
}
