<?php

namespace App\Http\Controllers\Library\Settings;

use App\Http\Controllers\Library\LibraryController;

class SubController extends LibraryController
{
    public const NAV_SECTION = 'settings';

    public const SUB_NAV = [
        'summary',
        'account',
    ];

    protected function getVars(array $vars = []): array
    {
        $vars['mainNavSection'] = self::NAV_SECTION;
        $vars['subNav'] = self::SUB_NAV;

        return parent::getVars($vars);
    }
}
