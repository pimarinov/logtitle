<?php

namespace App\Http\Controllers\Library\Main;

use App\Http\Controllers\Library\LibraryController;

class SubController extends LibraryController
{
    public const NAV_SECTION = 'all';

    public const SUB_NAV = [
        'summary',
        'titles',
        'authors',
    ];

    protected function getVars(array $vars = []): array
    {
        $vars['mainNavSection'] = self::NAV_SECTION;
        $vars['subNav'] = self::SUB_NAV;

        return parent::getVars($vars);
    }
}
