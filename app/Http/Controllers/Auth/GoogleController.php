<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserAltEmail;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')
            ->stateless()
            ->redirect();
    }

    /**
     * Create a new controller instance.
     */
    public function handleGoogleCallback(Request $request)
    {
        try
        {
            $homeUrl = env('APP_URL') . RouteServiceProvider::HOME;

            $user = Socialite::driver('google')
                ->stateless()
                ->user();

            $findUser = User::where('google_id', $user->id)->first();

            if ($findUser)
            {
                Auth::login($findUser);

                return redirect()->intended($homeUrl);
            }

            $matchingEmailUser = User::where('email', $user->email)->first();

            if ($matchingEmailUser)
            {
                $matchingEmailUser->google_id = $user->id;
                $matchingEmailUser->email_verified_at = $newUser->email_verified_at ?? Carbon::now();
                $matchingEmailUser->saveOrFail();

                Auth::login($matchingEmailUser);

                return redirect()->intended($homeUrl);
            }

            $newUser = new User([
                'name' => $user->name,
                'email' => $user->email,
                'google_id' => $user->id,
                'password' => encrypt('123456dummy'),
            ]);
            $newUser->email_verified_at = Carbon::now();
            $newUser->saveOrFail();

            Auth::login($newUser);

            return redirect()->intended($homeUrl);
        }
        catch (Exception $e)
        {
            dd($e->getMessage());
        }
    }
}
