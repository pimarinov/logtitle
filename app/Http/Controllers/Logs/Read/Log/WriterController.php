<?php

namespace App\Http\Controllers\Logs\Read\Log;

use App\Features\Logs\Read\Log\Writer\IndexFeature;
use App\Http\Controllers\Logs\LogsController;
use App\Models\Read\ReadWriter;
use Illuminate\Http\Request;

class WriterController extends LogsController
{
    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(IndexFeature::class, $args);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ReadWriter $readWriter
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ReadWriter $readWriter)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ReadWriter $readWriter
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ReadWriter $readWriter)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\ReadWriter $readWriter
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReadWriter $readWriter)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ReadWriter $readWriter
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReadWriter $readWriter)
    {
    }
}
