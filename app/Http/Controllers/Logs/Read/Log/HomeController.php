<?php

namespace App\Http\Controllers\Logs\Read\Log;

use App\Features\Logs\Read\Log\HomeFeature;
use App\Http\Controllers\Logs\LogsController;
use Illuminate\Http\Request;

class HomeController extends LogsController
{
    public function index(Request $request)
    {
        return $this->serve(HomeFeature::class);
    }
}
