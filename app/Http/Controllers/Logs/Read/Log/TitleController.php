<?php

namespace App\Http\Controllers\Logs\Read\Log;

use App\Features\Logs\Read\Log\Title\CreateFeature;
use App\Features\Logs\Read\Log\Title\EditFeature;
use App\Features\Logs\Read\Log\Title\IndexFeature;
use App\Features\Logs\Read\Log\Title\ShowFeature;
use App\Features\Logs\Read\Log\Title\StoreFeature;
use App\Features\Logs\Read\Log\Title\UpdateFeature;
use App\Http\Controllers\Logs\LogsController;
use App\Models\Read\Log\ReadableTitle;
use Illuminate\Http\Request;

class TitleController extends LogsController
{
    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->route()->getName() !== 'read.log.titles.json')
        {
            dump(auth()->user()->created_at_diff . ' (' . __METHOD__ . ')');
        }

        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(IndexFeature::class, $args);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->serve(CreateFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(StoreFeature::class, $args);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ReadableTitle $title)
    {
        return $this->serve(ShowFeature::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ReadableTitle $title)
    {
        return $this->serve(EditFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReadableTitle $title)
    {
        return $this->serve(UpdateFeature::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReadableTitle $title)
    {
    }
}
