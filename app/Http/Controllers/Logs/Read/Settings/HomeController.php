<?php

namespace App\Http\Controllers\Logs\Read\Settings;

use App\Features\Logs\Read\Settings\HomeFeature;
use App\Http\Controllers\Logs\LogsController;

class HomeController extends LogsController
{
    public function index()
    {
        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(HomeFeature::class, $args);
    }
}
