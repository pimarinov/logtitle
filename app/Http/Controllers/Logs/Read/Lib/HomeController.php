<?php

namespace App\Http\Controllers\Logs\Read\Lib;

use App\Features\Logs\Read\Lib\HomeFeature;
use App\Http\Controllers\Logs\LogsController;

class HomeController extends LogsController
{
    public function index()
    {
        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(HomeFeature::class, $args);
    }
}
