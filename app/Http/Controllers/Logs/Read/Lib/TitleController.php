<?php

namespace App\Http\Controllers\Logs\Read\Lib;

use App\Features\Logs\Read\Lib\Title\IndexFeature;
use App\Http\Controllers\Logs\LogsController;
use App\Models\Read\Title;
use Illuminate\Http\Request;

class TitleController extends LogsController
{
    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $args = [
            'controller' => self::class,
            'method' => __FUNCTION__,
        ];

        return $this->serve(IndexFeature::class, $args);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Title $title)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Title $title)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Title $title)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Title $title
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Title $title)
    {
    }
}
