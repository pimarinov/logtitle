<?php

namespace App\Http\Controllers\Logs\Play;

use App\Http\Controllers\MyLog\Read\ReadController;

class HomeController extends ReadController
{
    public const RECENTS_COUNT = 3;
    public static $section = 'home';

    public function index()
    {
        dd(__METHOD__);
        $vars = [
            'section' => self::$section,
            'action' => self::$section,
            'sub_nav' => [self::$section],
            'sub' => self::$section,
        ];

        $vars['users_count'] = Users::count();

        $vars['dummy_users_count'] = sprintf(
            '%s Millions',
            number_format((DummyUsers::count() / 1000000), 2)
        );

        //$this->vars($vars);
        $vars = $this->getCpVars($vars);

        return view('my-log.pages.home.home', $vars);
    }
}
