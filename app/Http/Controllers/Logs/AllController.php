<?php

namespace App\Http\Controllers\Logs;

use Illuminate\Http\Request;

class AllController extends LogsController
{
    public static $section = 'all';

    public function index(Request $request)
    {
        dd(__METHOD__);
        // here should be triggered redirect based on "default log" user settings value
        // when setting is empty here should load the choose log screen (read | play etc.)

        return redirect()->route('read.home');
    }
}
