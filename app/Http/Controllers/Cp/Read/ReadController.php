<?php

namespace App\Http\Controllers\Cp\Read;

use App\Http\Controllers\Cp\CpController;

class ReadController extends CpController
{
    public static $log = 'read';
}
