<?php

namespace App\Http\Controllers\Cp\Read;

class HomeController extends ReadController
{
    public const RECENTS_COUNT = 3;
    public static $section = 'home';

    /*public function __construct()
    {
        dd(__METHOD__);
    }*/

    public function index()
    {
        dump(__METHOD__, self::$section);
        $vars = [
            'section' => self::$section,
            'action' => self::$section,
            'sub_nav' => [self::$section],
            'sub' => self::$section,
        ];

        $vars['users_count'] = 0; //Users::count();

        /*$vars['dummy_users_count'] = sprintf('%s Millions'
            , number_format((DummyUsers::count()/1000000), 2)
            );*/
        $vars['dummy_users_count'] = 0;

        //$this->vars($vars);
        $vars = $this->getVars($vars);

        return view('my-log.pages.home.home', $vars);
    }
}
