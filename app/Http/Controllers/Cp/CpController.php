<?php

namespace App\Http\Controllers\Cp;

use App\Http\Controllers\Controller;

class CpController extends Controller
{
    public const NAV = [
        'read' => 'collections_bookmark',
        'all' => 'collections',
        'settings' => 'settings',
    ];

    /**
     * Only guests for "cp" guard are allowed except
     * for logout.
     */
    public function __construct()
    {
        //dd(__METHOD__);
        //$this->middleware('guest:cp');
    }
    //^ see https://fonts.google.com/icons?icon.query=collections_bookmark

    /*public function __construct()
    {
        $this->middleware('auth:cp');
    }*/

    protected function getVars(array $vars): array
    {
        $vars['mainNav'] = static::NAV;

        $class = static::class;

        $pieces = ['library'];

        if (isset($vars['navSection']))
        {
            $pieces[] = $vars['navSection'];
        }

        if (defined($class . '::ITEMS'))
        {
            $pieces[] = $class::ITEMS;
        }

        $pieces[] = $vars['action'];

        $vars['website_name'] = env('APP_NAME', 'SomeSite');
        $vars['content'] = implode('.', $pieces);

        if (in_array(end($pieces), ['index', 'home', 'summary']))
        {
            array_pop($pieces);
        }

        $p = array_map(function ($value) {
            return ucfirst(str_replace(['_', '-'], ' ', $value));
        }, $pieces);

        $vars['head_title'] = implode(' &#10093; ', $p);

        return $vars;
    }
}
