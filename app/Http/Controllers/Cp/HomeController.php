<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

class HomeController extends CpController
{
    public const RECENTS_COUNT = 3;

    /**
     * Only guests for "cp" guard are allowed except
     * for logout.
     */
    /*public function __construct()
    {
        //dd(__METHOD__);
        $this->middleware('auth:admin');
    }*/

    public static $section = 'home';

    public function index(Request $request)
    {
        dd(__METHOD__, $request->route()->uri);
        // here should be triggered redirect based on "default log" user settings value
        // when setting is empty here should load the choose log screen (read | play etc.)

        //return redirect()->route('my-log.read.home');
    }
}
